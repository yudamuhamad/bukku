SELECT b.code, b.title, pp.promo_code, p.title, p.promo_code, normal_price, promo_value, (normal_price - promo_value) as price_after_diskon FROM ms_promoproduct pp 
INNER JOIN ms_product b ON b.code = pp.product_code
INNER JOIN mst_promo p ON p.promo_code = pp.promo_code
WHERE p.type = 'Diskon' AND p.is_active = 1;
