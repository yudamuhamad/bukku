SELECT PU.name, W.full_name, P.title, SUM(TRD.product_order_qty), SUM(TRD.product_total_price), DATE(TR.order_date) 
FROM tr_transaction TR
INNER JOIN tr_transaction_detail TRD ON TRD.order_id = TR.order_id 
INNER JOIN ms_product P ON P.code = TRD.product_code
LEFT JOIN mst_book B ON B.code = P.code
LEFT JOIN mst_bundle BU ON BU.code = P.code
JOIN ms_bookwriter BW ON BW.book_code = B.code
JOIN mst_writer W ON W.code = BW.writer_code
JOIN mst_publisher PU ON PU.code = B.publisher_code
WHERE DATE(TR.)
GROUP BY TRD.product_code, DATE(TR.order_date)