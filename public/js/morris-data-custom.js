$(function() {

    "use strict";

    // Dashboard 1 Morris-chart

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2017',
            whatsapp: 50,
            ipad: 80,
            itouch: 20
        }, {
            period: '2018',
            whatsapp: 130,
            ipad: 100,
            itouch: 80
        }, {
            period: '2019',
            whatsapp: 80,
            ipad: 60,
            itouch: 70
        }, {
            period: '2020',
            whatsapp: 70,
            ipad: 200,
            itouch: 140
        }, {
            period: '2021',
            whatsapp: 180,
            ipad: 150,
            itouch: 140
        }, {
            period: '2022',
            whatsapp: 105,
            ipad: 100,
            itouch: 80
        }, {
            period: '2023',
            whatsapp: 250,
            ipad: 150,
            itouch: 200
        }],
        xkey: 'period',
        ykeys: ['whatsapp', 'ipad', 'itouch'],
        labels: ['whatsapp', 'iPad', 'iPod Touch'],
        pointSize: 3,
        fillOpacity: 0,
        pointStrokeColors: ['#00bbd9', '#ffb136', '#4a23ad'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 1,
        hideHover: 'auto',
        lineColors: ['#00bbd9', '#ffb136', '#4a23ad'],
        resize: true

    });

    Morris.Area({
        element: 'morris-area-chart2',
        data: [{
            period: '2010',
            SiteA: 0,
            SiteB: 0,

        }, {
            period: '2011',
            SiteA: 130,
            SiteB: 100,

        }, {
            period: '2012',
            SiteA: 80,
            SiteB: 60,

        }, {
            period: '2013',
            SiteA: 70,
            SiteB: 200,

        }, {
            period: '2014',
            SiteA: 180,
            SiteB: 150,

        }, {
            period: '2015',
            SiteA: 105,
            SiteB: 90,

        }, {
            period: '2016',
            SiteA: 250,
            SiteB: 150,

        }],
        xkey: 'period',
        ykeys: ['SiteA', 'SiteB'],
        labels: ['Site A', 'Site B'],
        pointSize: 0,
        fillOpacity: 0.4,
        pointStrokeColors: ['#ffb136', '#00bbd9'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 0,
        smooth: false,
        hideHover: 'auto',
        lineColors: ['#ffb136', '#00bbd9'],
        resize: true

    });
});
