package controllers

import (
	"errors"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

type Transaction struct {
	App
	Dashboard
	Master
}

func (c Transaction) NewTransaction() revel.Result {
	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	return c.RenderTemplate("Dashboard/Transaction/new.html")
}

func (c Transaction) NewTransactionHandler(data models.CustomerAddressAPI) revel.Result {
	// Insert to database
	// customer := &models.Customer{0, name, phone, state, }
	customer := &models.CustomerData{
		Name:         data.Name,
		Phone:        data.Phone,
		ModifiedBy:   c.connected().Username,
		ModifiedDate: time.Now(),
		IsActive:     true}

	customerID := GetCustomerID(customer)
	if customerID == 0 {
		err := Dbm.Insert(customer)
		if err != nil {
			panic(err)
		}

		address := &models.AddressData{
			CustomerID:   customer.ID,
			Title:        "Main Address",
			StateID:      1,
			ProvinceID:   9,
			CityID:       115,
			Kelurahan:    data.Kelurahan,
			Kecamatan:    data.Kecamatan,
			Street:       data.Street,
			PostalCode:   "16400",
			ModifiedBy:   c.connected().Username,
			ModifiedDate: time.Now(),
			IsDefault:    true,
			IsActive:     true}
		err = Dbm.Insert(address)
		if err != nil {
			panic(err)
		}
	}
	if customerID == 0 {
		customerID = customer.ID
	}

	// redirect ke user yang sesuai
	return c.Redirect("/dashboard/transaction/cust/%d/new", customerID)
}

func (c Transaction) NewTransactionDetail(uid int) revel.Result {
	customer, err := getCustomerById(uid)
	if err != nil {
		panic(err)
	}
	addressFormated := getFormatedAddress(customer, true)

	uniquecode := getUniqueCode()
	timenow := time.Now().Format("060102") //20060102 => yyyyMMdd

	c.ViewArgs["uniquecode"] = uniquecode
	c.ViewArgs["ordercode"] = timenow + "0" + strconv.Itoa(uniquecode)
	c.ViewArgs["data"] = customer
	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	c.ViewArgs["bundle"] = getAllBundle()
	c.ViewArgs["buku"] = getAllBook()
	c.ViewArgs["addressFormated"] = strings.Replace(addressFormated, "\n", "<br>", -1)
	return c.RenderTemplate("Dashboard/Transaction/new_detail.html")
}

func (c Transaction) NewTransactionDetailHandler(uid int64) revel.Result {
	channel := c.Params.Form.Get("channel")
	courier := c.Params.Form.Get("courier")
	courierService := c.Params.Form.Get("courierType")

	state := c.Params.Form.Get("stateText")
	province := c.Params.Form.Get("provinceText")
	city := c.Params.Form.Get("cityText")
	kecamatan := c.Params.Form.Get("kecamatan")
	kelurahan := c.Params.Form.Get("kelurahan")
	postalCode := c.Params.Form.Get("postalcode")

	shippingCost := c.Params.Form.Get("shippingCost")
	shippingCostInt, _ := strconv.ParseInt(shippingCost, 10, 64)

	uniqueCode := c.Params.Form.Get("uniqueCode")
	uniqueCodeInt, _ := strconv.ParseInt(uniqueCode, 10, 64)

	ucode := c.Params.Form.Get("orderCodeID")
	uCodeInt, _ := strconv.ParseInt(ucode, 10, 64)

	products := c.Params.Form["product[]"]
	qty := c.Params.Form["quantity[]"]
	weight := c.Params.Form.Get("weight")
	weightInt, _ := strconv.ParseInt(weight, 10, 64)
	items := []models.ProductStock{}
	var totalShoppingPrice int64
	totalShoppingPrice = 0

	notes := c.Params.Form.Get("myCopy")

	var err error
	c.Begin()
	defer func() {
		if err != nil {
			c.Rollback()
		}
	}()

	// Check if stock sufficient
	for i, prd := range products {
		stock := models.ProductStock{}
		err = c.Txn.QueryRow(`
			SELECT * FROM 
				(SELECT code, "book" AS type, current_stock FROM mst_book 
					UNION 
					SELECT mst_bundle.code AS code, "bundle" AS type, SUM(current_stock) AS current_stock FROM mst_bundle JOIN mst_book ON mst_bundle.book_code = mst_book.code GROUP BY mst_bundle.code) AS t 
			WHERE code = ?`, prd).Scan(&stock.Code, &stock.Type, &stock.Stock)
		if err != nil {
			panic(err)
		}

		var qtyi int
		qtyi, err = strconv.Atoi(qty[i])
		if err != nil {
			panic(err)
		}
		// Stock not sufficient!
		if qtyi > stock.Stock {
			c.Flash.Error("Stock for " + prd + " is not sufficient")
			err = errors.New("stock not sufficient")
			return c.Redirect("/dashboard/transaction/cust/%d/new", uid)
		} else {
			// Decrease book stock
			if stock.Type == "book" {
				_, err = c.Txn.Exec(`UPDATE mst_book SET current_stock = current_stock - ? WHERE code = ? AND current_stock-?>0`, qtyi, prd, qtyi)
				if err != nil {
					panic(err)
				}
			} else { // decrease all book stock in bundle
				bundle := getBundleFromCode(prd)
				for _, book := range bundle.BookCode {
					_, err = c.Txn.Exec(`UPDATE mst_book SET current_stock = current_stock - ? WHERE code = ? AND current_stock-?>0`, qtyi, book, qtyi)
					if err != nil {
						panic(err)
					}
				}
			}
		}
		items = append(items, models.ProductStock{prd, stock.Type, qtyi})
		obj, err := Dbm.Get(models.ProductData{}, prd)
		if err != nil {
			panic(err)
		}
		product := obj.(*models.ProductData)
		totalShoppingPrice = totalShoppingPrice + (product.NormalPrice * int64(qtyi))
	}

	// Insert transaction
	transaction := &models.TransactionData{}
	transaction.ID = 0 // temporary id
	transaction.CustomerID = uid
	transaction.Date = time.Now()
	transaction.Channel = channel

	transaction.Status = orderWaiting
	if channel == "WEB" {
		transaction.Status = 2 // payment confirmed by customer
	}

	transaction.ShippingState = state
	transaction.ShippingProvince = province
	transaction.ShippingCity = city
	transaction.ShippingKelurahan = kelurahan
	transaction.ShippingKecamatan = kecamatan
	transaction.ShippingPostalCode = postalCode
	transaction.CourierName = courier
	transaction.CourierService = courierService
	transaction.ModifiedBy = c.connected().Username
	transaction.ModifiedDate = time.Now()
	transaction.CourierCost = shippingCostInt
	transaction.TotalWeight = weightInt
	transaction.TotalShoppingPrice = totalShoppingPrice
	transaction.UniqueCode = uCodeInt
	transaction.TotalPrice = totalShoppingPrice + transaction.CourierCost + uniqueCodeInt
	transaction.PromoCode = ""
	transaction.Note = notes

	err = c.Txn.Insert(transaction)
	if err != nil {
		panic(err)
	}

	// Insert transaction detail
	for _, item := range items {
		obj, err := Dbm.Get(models.ProductData{}, item.Code)
		if err != nil {
			panic(err)
		}
		product := obj.(*models.ProductData)

		detail := &models.TransactionDetailData{}
		detail.OrderID = transaction.ID
		detail.ProductCode = item.Code
		detail.Qty = item.Stock
		detail.SinglePrice = product.NormalPrice
		detail.TotalPrice = detail.SinglePrice * int64(detail.Qty)
		err = c.Txn.Insert(detail)
		if err != nil {
			panic(err)
		}
	}

	c.Commit()
	c.Flash.Success("Success creating new order")
	return c.Redirect("/dashboard/transaction/new")
}

func (c Transaction) EditTransaction(id int64) revel.Result {
	obj, err := Dbm.Get(models.TransactionData{}, id)
	if err != nil {
		panic(err)
	}
	if obj == nil {
		return c.RenderHTML("Error! No transaction found. <a href='javascript:history.back()'>Go Back</a>")
	}
	transaction := obj.(*models.TransactionData)
	err = Dbm.SelectOne(&transaction.Customer, "SELECT * FROM mst_customer WHERE id = ?", transaction.CustomerID)
	if err != nil {
		panic(err)
	}
	transaction.Customer.Address, err = getCustomerById(int(transaction.CustomerID))
	if err != nil {
		panic(err)
	}
	_, err = Dbm.Select(&transaction.Detail, "SELECT * FROM tr_transaction_detail WHERE order_id = ?", id)
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = transaction
	c.ViewArgs["books"] = getAllBook()
	c.ViewArgs["bundles"] = getAllBundle()
	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	// return c.RenderJSON(transaction)
	return c.RenderTemplate("Dashboard/Transaction/edit.html")
}

func (c Transaction) Process() revel.Result {
	transactions, err := getAllUnfinishedTransaction()
	if err != nil {
		panic(err)
	}

	c.ViewArgs["book"] = c.getBook()
	c.ViewArgs["banks"] = c.getBank()
	c.ViewArgs["data"] = transactions
	return c.RenderTemplate("Dashboard/Transaction/process.html")
}

func (c Transaction) History() revel.Result {
	transactions, err := getAllFinishedTransaction()
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = transactions
	return c.RenderTemplate("Dashboard/Transaction/history.html")
}

func (c Transaction) Print() revel.Result {
	data, err := getAllReadyToSendTransaction()
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Transaction/print.html")
}

func (c Transaction) PrintExecute() revel.Result {
	data, err := getAllReadyToSendTransaction()
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = data
	c.ViewArgs["size"] = 8
	return c.RenderTemplate("Dashboard/Transaction/print_paper.html")
}

func (c Transaction) ViewTransaction(id int64) revel.Result {
	tx, err := getTransactionById(id)
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = tx
	return c.RenderTemplate("Dashboard/Transaction/view.html")
}

// ----------------- POST route handler for Transaction -----------------------------------------------------------------------------
func (c Transaction) FinishPrintHandler() revel.Result {
	_, err := Dbm.Exec(`UPDATE tr_transaction SET order_status = ? WHERE order_status = ?`, orderShipped, orderPaid)
	if err != nil {
		panic(err)
	}

	return c.Redirect("/dashboard/transaction/print")
}

func (c Transaction) EditTransactionHandler() revel.Result {
	return c.RenderText("EditTransactionHandler")
}

func (c Transaction) DeleteTransaction(id int64) revel.Result {
	_, err := Dbm.Exec(`UPDATE tr_transaction SET order_status = 0 WHERE order_id = ?`, id)
	if err != nil {
		panic(err)
	}

	return c.Redirect("/dashboard/transaction/process")
}

func (c Transaction) ConfirmTransactionHandler(id int64) revel.Result {
	_, err := Dbm.Exec(`UPDATE tr_transaction SET order_status = ? WHERE order_id = ?`, orderConfirmed, id)
	if err != nil {
		panic(err)
	}

	return c.Redirect("/dashboard/transaction/process")
}

func (c Transaction) ConfirmPaidTransactionHandler(id int64) revel.Result {

	bankbukku := c.Params.Form.Get("BankBukku")
	bankfrom := c.Params.Form.Get("BankFrom")
	accountfrom := c.Params.Form.Get("AccountName")

	//panic(bankbukku)

	po, err := Dbm.SelectStr(`SELECT category FROM tr_transaction_detail trd
		JOIN tr_transaction tr ON tr.order_id = trd.order_id
		JOIN ms_product ON product_code = code WHERE tr.order_id = ? AND category LIKE 'Pre %' AND order_status = ?`, id, orderWaiting)

	if po == ("Pre Order") {
		_, err = Dbm.Exec(`UPDATE tr_transaction 
			SET 
				confirmation_date = now(), 
				confirmation_bank = ?,
				confirmation_bank_owner = ?,
				bank_bukku = ?,
				order_status = ? 
			WHERE 
				order_id = ?`,
			bankfrom, accountfrom, bankbukku, preOrderPaid, id)
		if err != nil {
			panic(err)
		}
	} else {
		_, err = Dbm.Exec(`UPDATE tr_transaction 
			SET 
				confirmation_date = now(), 
				confirmation_bank = ?,
				confirmation_bank_owner = ?,
				bank_bukku = ?,
				order_status = ? 
			WHERE 
				order_id = ?`,
			bankfrom, accountfrom, bankbukku, orderPaid, id)
		if err != nil {
			panic(err)
		}
	}

	return c.Redirect("/dashboard/transaction/process")
}

func (c Transaction) PackTransactionHandler(id int64) revel.Result {
	_, err := Dbm.Exec(`UPDATE tr_transaction SET order_status = ? WHERE order_id = ?`, orderPaid, id)
	if err != nil {
		panic(err)
	}

	return c.Redirect("/dashboard/transaction/process")
}

func (c Transaction) SendTransactionHandler(id int64) revel.Result {
	_, err := Dbm.Exec(`UPDATE tr_transaction SET order_status = ? WHERE order_id = ?`, orderShipped, id)
	if err != nil {
		panic(err)
	}

	return c.Redirect("/dashboard/transaction/process")
}

func (c Transaction) ChangeStatusHandler() revel.Result {
	bookCode := c.Params.Get("BookCode")
	statusAwal := c.Params.Get("StatusAwal")
	statusTujuan := c.Params.Get("StatusTujuan")

	_, err := Dbm.Exec(`UPDATE tr_transaction tr 
		INNER JOIN tr_transaction_detail d ON tr.order_id = d.order_id 
		SET order_status = ? 
		WHERE order_status = ? AND d.product_code = ?`, statusTujuan, statusAwal, bookCode)
	if err != nil {
		panic(err)
	}

	//status, _ := strconv.ParseInt(statusAkhir, 10, 64)
	//title, desc, color := getStateText(status)

	c.Flash.Success("Berhasil mengubah status")
	return c.Redirect("/dashboard/transaction/process")
}

func (c Transaction) InputResiHandler(id int64) revel.Result {
	resi := c.Params.Get("resi")

	_, err := Dbm.Exec(`UPDATE tr_transaction SET courier_resi = ? WHERE order_id = ?`, resi, id)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("No Resi : " + resi)

	return c.Redirect("/dashboard/transaction/history")
}

// ----------------- Helper Function for Transaction -----------------------------------------------------------------------------
func getUniqueCode() int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}

	date := time.Now().Format("2006-01-02")
	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE DATE(order_date) = ?`, date).Scan(&result)
	if err != nil {
		return 1
	}

	return result + 1
}

func getTransactionById(id int64) (*models.TransactionData, error) {
	obj, err := Dbm.Get(models.TransactionData{}, id)
	if err != nil {
		return nil, err
	}
	data := obj.(*models.TransactionData)

	_, err = Dbm.Select(&data.Detail, `
			SELECT * FROM tr_transaction_detail WHERE order_id = ?
		`, data.ID)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func getStateText(status int) (text string, desc string, color string) {
	switch status {
	case orderCanceled:
		return "Canceled", "Pesanan dibatalkan, dapat diakibatkan karena pembayaran tidak dikonfirmasi terlalu lama", "danger"
	case orderShipped:
		return "Sent to customer", "Pesanan sudah dikemas dan dikirim ke pembeli", "info"
	case orderFinished:
		return "Received by customer", "Pesanan sudah diterima pembeli", "success"
	case orderWaiting:
		return "Waiting for payment", "Menunggu pembayaran dari pembeli", "warning"
	case orderConfirmed:
		return "Confirmed by customer", "Pembeli telah melakukan pemabyaran, silahkan konfirmasi", "info"
	case orderPaid:
		return "Ready to print label", "Label siap di print", "primary"
	case preOrderPaid:
		return "Payment Completed", "Pembayaran PreOrder telah selesai", "primary"
	// case orderPacked:
	// 	return "Ready to print", "Alamat pengiriman dapat dicetak", "success"
	default:
		return "N/A", "Kesalahan sistem", "default"
	}
}

// Unfinsihed : Wait for payment, Confirmed By Customer, Ready to Send
func getAllUnfinishedTransaction() ([]models.TransactionView, error) {
	data := []models.TransactionView{}

	_, err := Dbm.Select(&data, `SELECT tr.order_id, sum(d.product_order_qty) as total_order_qty, unique_code, channel, order_date, order_status, full_name, phone_1, total_price 
		FROM tr_transaction tr
		JOIN tr_transaction_detail d ON d.order_id = tr.order_id
		JOIN mst_customer ON ID = customer_id
		WHERE order_status IN (?,?,?) GROUP BY tr.order_id`, orderWaiting, orderConfirmed, preOrderPaid) //, orderPaid, orderPacked)
	if err != nil {
		return nil, err
	}

	loc, _ := time.LoadLocation("Asia/Jakarta")
	for i, item := range data {
		_, err = Dbm.Select(&data[i].ProductName, "SELECT CONCAT( title, ' (', product_order_qty, ')' ) FROM tr_transaction_detail JOIN ms_product ON product_code = code WHERE order_id = ?", item.ID)
		if err != nil {
			return nil, err
		}

		po, err := Dbm.SelectStr(`SELECT category FROM tr_transaction_detail trd
			JOIN tr_transaction tr ON tr.order_id = trd.order_id
			JOIN ms_product ON product_code = code WHERE tr.order_id = ? AND category LIKE 'Pre %'`, item.ID)

		if po == ("Pre Order") {
			data[i].ProductCategory = po
		} else {
			reg, err := Dbm.SelectStr(`SELECT category FROM tr_transaction_detail trd
				JOIN tr_transaction tr ON tr.order_id = trd.order_id
				JOIN ms_product ON product_code = code WHERE tr.order_id = ? `, item.ID)
			if err != nil {
				return nil, err
			}
			data[i].ProductCategory = reg
		}
		if err != nil {
			return nil, err
		}
		// _, err = Dbm.Select(&data[i].ProductCategory, "SELECT category FROM tr_transaction_detail JOIN ms_product ON product_code = code WHERE order_id = ?", item.ID)
		// if err != nil {
		// 	return nil, err
		// }

		data[i].StatusText, data[i].StatusDesc, data[i].StatusColor = getStateText(item.Status)
		data[i].DateText = item.Date.In(loc).Format("02 Jan 2006")
		data[i].TotalPriceText = NumberToString(item.TotalPrice, '.')
	}

	return data, nil
}

// Finished : Sent, Received, Canceled
func getAllFinishedTransaction() ([]models.TransactionView, error) {
	data := []models.TransactionView{}

	_, err := Dbm.Select(&data, `SELECT order_id, unique_code, customer_id, channel, order_date, order_status, full_name, phone_1, total_price, courier_resi 
		FROM tr_transaction 
		JOIN mst_customer ON ID = customer_id 
		WHERE order_status IN (?,?,?)`, orderCanceled, orderShipped, orderFinished)
	if err != nil {
		return nil, err
	}

	loc, _ := time.LoadLocation("Asia/Jakarta")
	for i, item := range data {
		_, err = Dbm.Select(&data[i].ProductName, "SELECT title FROM tr_transaction_detail JOIN ms_product ON product_code = code WHERE order_id = ?", item.ID)
		if err != nil {
			return nil, err
		}

		data[i].StatusText, data[i].StatusDesc, data[i].StatusColor = getStateText(item.Status)
		data[i].DateText = item.Date.In(loc).Format("02 Jan 2006")
		data[i].TotalPriceText = NumberToString(item.TotalPrice, '.')
		// customer, err := getCustomerById(item.CustomerID)
		// if err != nil {
		// 	return nil, err
		// }
		// address := getFormatedAddress(customer, false)
		// data[i].CustomerAddress = strings.Replace(address, "\n", "<br>", -1)
		// if item.Resi == "" {
		// 	data[i].Resi = "N/A"
		// }

		data[i].CustomerAddress = GetShippingAddress(item.ID)
	}

	return data, nil
}

func getAllReadyToSendTransaction() ([]models.TransactionView, error) {
	data := []models.TransactionView{}

	_, err := Dbm.Select(&data, `SELECT 
			order_id, 
			unique_code, 
			customer_id, 
			channel, 
			order_date, 
			order_status, 
			full_name, 
			phone_1, 
			total_price, 
			courier_resi, 
			courier_name, 
			courier_type 
		FROM tr_transaction 
		JOIN mst_customer ON ID = customer_id 
		WHERE order_status = ?`, orderPaid)
	if err != nil {
		return nil, err
	}

	for i, item := range data {
		_, err = Dbm.Select(&data[i].ProductName, "SELECT CONCAT( title, ' (', product_order_qty, ')' ) FROM tr_transaction_detail JOIN ms_product ON product_code = code WHERE order_id = ?", item.ID)
		if err != nil {
			return nil, err
		}

		_, err = Dbm.Select(&data[i].ProductCode, "SELECT code FROM tr_transaction_detail JOIN ms_product ON product_code = code WHERE order_id = ?", item.ID)
		if err != nil {
			return nil, err
		}

		// customer, err := getCustomerById(item.CustomerID)
		// if err != nil {
		// 	return nil, err
		// }
		// address := getFormatedAddress(customer, false)
		data[i].CustomerAddress = GetShippingAddress(item.ID) //strings.Replace(address, "\n", "<br>", -1)
	}

	return data, nil
}

func GetShippingAddress(id int64) string {
	tx, err := Dbm.Begin()
	if err != nil {
		return ""
	}
	var result = ""
	err = tx.QueryRow(`
		select CONCAT(street, ", ", kelurahan, ", ", kecamatan, ", ", city, " ", postal_code, " ", state) as address 
		from tr_transaction 
		where order_id = ?`, id).Scan(&result)
	if err != nil {
		return ""
	}

	return result
}

func NumberToString(n int64, sep rune) string {

	s := strconv.FormatInt(n, 10)

	startOffset := 0
	if n < 0 {
		startOffset = 1
	}

	const groupLen = 3
	groups := (len(s) - startOffset - 1) / groupLen

	if groups == 0 {
		return s
	}

	sepLen := utf8.RuneLen(sep)
	sepBytes := make([]byte, sepLen)
	_ = utf8.EncodeRune(sepBytes, sep)

	buf := make([]byte, groups*(groupLen+sepLen)+len(s)-(groups*groupLen))

	startOffset += groupLen
	p := len(s)
	q := len(buf)
	for p > startOffset {
		p -= groupLen
		q -= groupLen
		copy(buf[q:q+groupLen], s[p:])
		q -= sepLen
		copy(buf[q:], sepBytes)
	}
	if q > 0 {
		copy(buf[:q], s)
	}
	return string(buf)
}
