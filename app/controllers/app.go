package controllers

import (
	"crypto/sha1"
	"fmt"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

type App struct {
	GorpController
}

// This Action function renders the .../view/App/index.html
func (c App) Index() revel.Result {
	if user := c.connected(); user != nil {

		return c.Redirect("/dashboard")
	}
	return c.Render()
}

func (c App) AddUser() revel.Result {
	if user := c.connected(); user != nil {
		c.ViewArgs["user"] = user

		c.ViewArgs["countProcess"] = getCountProcessOrder()
		c.ViewArgs["countPrint"] = getCountPrint()
	}
	return nil
}

func (c App) connected() *models.User {
	if c.ViewArgs["user"] != nil {
		return c.ViewArgs["user"].(*models.User)
	}
	if c.ViewArgs["username"] != nil {
		return c.ViewArgs["username"].(*models.User)
	}
	if username, ok := c.Session["username"]; ok {
		return c.getUser(username)
	}
	return nil
}

// This Action function handles the login of an user
func (c App) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		//c.Flash.Error("Please log in first")
		return c.Redirect("/")
	}
	return nil
}

func (c App) getUser(username string) *models.User {
	users, err := Dbm.Select(models.User{},
		`select * from mst_user where Username = ? and is_active = 1 and role_code != 'CUS'`, username)
	if err != nil {
		//	panic(err)
	}
	if len(users) == 0 {
		return nil
	}

	return users[0].(*models.User)
}

func (c App) encryptString(text string) string {

	var sha = sha1.New()
	sha.Write([]byte(text))
	var encrypted = sha.Sum(nil)
	var encryptedString = fmt.Sprintf("%x", encrypted)

	return encryptedString
}
