package controllers

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"

	"github.com/jlaffaye/ftp"
)

const FTP_SERVER = "bukku.id:21"
const FTP_USER = "admin"
const FTP_PASSWORD = "Buk4nrembul4n!"
const FTP_ADDRESS = "bukku.id"

func postFTP(filename string) error {
	ftp, err := ftp.Dial("ftp://bukku.co.id")
	if err != nil {
		return err
	}

	if err := ftp.Login("admin", "Buk4nrembul4n!"); err != nil {
		return err
	}
	return nil
}

func postFile(filename string, targetUrl string) error {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	// this step is very important
	fileWriter, err := bodyWriter.CreateFormFile("uploadfile", filename)
	if err != nil {
		fmt.Println("error writing to buffer")
		return err
	}

	// open file handle
	fh, err := os.Open(filename)
	if err != nil {
		fmt.Println("error opening file")
		return err
	}
	defer fh.Close()

	//iocopy
	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(targetUrl, contentType, bodyBuf)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	resp_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(resp.Status)
	fmt.Println(string(resp_body))
	return nil
}

func uploadImage(category string, imageName string, image []byte) error {
	var err error

	err = nil

	// Preparing FTP
	client, err := ftp.Dial(FTP_SERVER)
	if err != nil {
		return err
	}
	defer client.Quit()

	if err = client.Login(FTP_USER, FTP_PASSWORD); err != nil {
		return err
	}

	file := bytes.NewReader(image)

	path := fmt.Sprintf("img/%s/%s", category, imageName)
	err = client.Stor(path, file)

	return err
}
