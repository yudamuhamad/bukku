package controllers

const orderCanceled = 0  // Transaksi melewati batas waktu atau dibatalkan
const orderWaiting = 1   // Menunggu pembayaran dari pembeli
const orderConfirmed = 2 // Pembeli sudah membayar, menunggu konfirmasi sistem
const orderPaid = 3      // Pembayaran terkonfirmasi, siap cetak alamat
const preOrderPaid = 4   // Pembayaran preorder terkonfirmasi, siap cetak alamat
//const orderPacked = 4    // Pesanan siap dikirim, proses pencetakan alamat
const orderShipped = 5  // Alamat tercetak, pesanan sudah dikirim
const orderFinished = 6 // Pesanan sampai ke pelanggan

// Label size in mm
const labelWidth = 100
const labelHeight = 50
const labelPaddingHorizontal = 10
const labelPaddingVertical = 10
