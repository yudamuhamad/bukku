package controllers

import (
	"bukku.co.id/app/routes"
	"github.com/revel/revel"
)

// Logout : handler to delete all session and make user loged out from system
func (c App) Logout() revel.Result {
	for k := range c.Session {
		delete(c.Session, k)
	}
	return c.Redirect(routes.App.Index())
}
