package controllers

import (
	"github.com/revel/revel"
)

type Courier struct {
	App
	Ongkir
}

func (c Courier) CekOngkir() revel.Result {
	return c.RenderTemplate("Dashboard/Courier/cekongkir.html")
}

func (c Courier) CekResi() revel.Result {
	return c.RenderTemplate("Dashboard/Courier/cekresi.html")
}
