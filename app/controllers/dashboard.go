package controllers

import (
	"strings"
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

type Dashboard struct {
	App
}

func (c Dashboard) Index() revel.Result {
	today := time.Now()
	yestreday := today.AddDate(0, 0, -1)
	trxYesterday := c.getDashboardTransaction(yestreday.Format("2006-01-02"))
	trxToday := c.getDashboardTransaction(today.Format("2006-01-02"))

	c.ViewArgs["today"] = today.Format("02 Jan 2006")
	c.ViewArgs["month"] = today.Format("January")
	c.ViewArgs["year"] = today.Format("2006")
	c.ViewArgs["ordertoday"] = getCountOrderToday()
	c.ViewArgs["orderonprocess"] = getCountProcessOrder()
	c.ViewArgs["orderpayment"] = getCountOrderStatus(1)
	c.ViewArgs["ordermonth"] = getCountOrderMonth(today.Format("01"), today.Format("2006"))
	c.ViewArgs["ordersuccess"] = getCountOrderSuccess(today.Format("2006"))

	c.ViewArgs["yesterdayTransaction"] = trxYesterday
	c.ViewArgs["todayTransaction"] = trxToday
	return c.Render()
}

func getCountProcessOrder() int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}
	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE order_status IN (1,2,4)`).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func getCountPrint() int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}
	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE order_status = 3`).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func getCountOrderToday() int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}

	date := time.Now().Format("2006-01-02")
	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE DATE(order_date) = ?`, date).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func getCountOrderStatus(status int) int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}

	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE order_status = ?`, status).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func getCountOrderMonth(month string, year string) int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}

	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE MONTH(order_date) = ? AND YEAR(order_date) = ?`, month, year).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func getCountOrderSuccess(year string) int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}

	var result = 0
	err = tx.QueryRow(`
			SELECT COUNT(*) AS COUNT FROM tr_transaction WHERE order_status = 5 AND YEAR(order_date) = ?`, year).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func (c Dashboard) getDateTime(datetime string) time.Time {

	layout := "01/02/2006" //"2006-01-02 15:04:05" //time.LoadLocation("Asia/Jakarta") //
	str := datetime
	t, err := time.Parse(layout, str)

	if err != nil {
		panic(err)
	}
	return t
}

func (c Dashboard) getBookImageName(originalName string, writerCode string, plainTitle string, imgNo string) string {
	title := strings.Replace(plainTitle, " ", "", -1)
	title = strings.ToLower(title)
	writerCode = strings.ToLower(writerCode)

	originalSlice := strings.Split(originalName, ".")
	ext := ""
	if len(originalSlice) > 1 {
		ext = originalSlice[len(originalSlice)-1]
	}

	if imgNo == "1" || imgNo == "0" {
		imgNo = ""
	}

	return writerCode + "0" + title + imgNo + "." + ext
}

func (c Dashboard) getDashboardTransaction(date string) []models.DashboarTransaction {
	trxs := []models.DashboarTransaction{}
	_, err := Dbm.Select(&trxs,
		`SELECT 
			TR.order_id, 
			PU.name as publisher, 
			W.full_name as writer, 
			P.title, 
			SUM(TRD.product_order_qty) as qty, 
			SUM(TRD.product_total_price) as price, 
			DATE(TR.order_date) as date 
		FROM tr_transaction TR
		INNER JOIN tr_transaction_detail TRD ON TRD.order_id = TR.order_id 
		INNER JOIN ms_product P ON P.code = TRD.product_code
		LEFT JOIN mst_book B ON B.code = P.code
		LEFT JOIN mst_bundle BU ON BU.code = P.code
		JOIN ms_bookwriter BW ON BW.book_code = B.code
		JOIN mst_writer W ON W.code = BW.writer_code
		JOIN mst_publisher PU ON PU.code = B.publisher_code 
		WHERE DATE(TR.order_date) = DATE(?)
		GROUP BY 
			TRD.product_code, DATE(TR.order_date)`, date)
	if err != nil {
		panic(err)
	}

	for i, trx := range trxs {
		trxs[i].PriceFormat = decimalToCurrency("Rp", trx.Price)
	}

	return trxs
}

func (c Dashboard) getConfig(enum string) []models.Config {
	var configs []models.Config
	_, err := Dbm.Select(&configs,
		`SELECT * FROM mst_config WHERE ConfigEnum = ?`, enum)
	if err != nil {
		panic(err)
	}
	return configs
}

func (c Dashboard) getBank() []models.Bank {
	var banks []models.Bank
	_, err := Dbm.Select(&banks,
		`SELECT bank_code, bank_name, account_number, account_name FROM mst_bank WHERE account_number != ''`)
	if err != nil {
		panic(err)
	}
	return banks
}
