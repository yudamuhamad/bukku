package controllers

import (
	"strings"
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Promo GET Handler ====================================
func (c Master) Promo() revel.Result {
	data, err := c.getAllPromo()
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/promo.html")
}

func (c Master) NewPromo() revel.Result {
	return c.RenderTemplate("Dashboard/Master/promo_new.html")
}

func (c Master) EditPromo(id string) revel.Result {
	data := c.getPromo(id)
	c.ViewArgs["p"] = data
	return c.RenderTemplate("Dashboard/Master/promo_edit.html")
}

// ==================================== Promo POST Handler ====================================
func (c Master) NewPromoHandler(p models.PromoData) revel.Result {
	p.ModifiedBy = c.connected().Username
	p.ModifiedDate = time.Now()

	p.Type = c.Params.Form.Get("Type")
	p.PromoSatuan = c.Params.Form.Get("PromoSatuan")
	startDate := c.Params.Form.Get("PeriodStart")
	endDate := c.Params.Form.Get("PeriodEnd")

	p.PeriodStart = c.getDateTime(startDate)
	p.PeriodEnd = c.getDateTime(endDate)

	_, err := Dbm.Exec(`INSERT INTO mst_promo (
		promo_code,
		title,
		type,
		min_product,
		min_purchase,
		promo_satuan,
		promo_value,
		max_reduction,
		max_count,
		used_count,
		max_budget,
		used_budget,
		start_date,
		end_date,
		modified_by,
		modified_date,
		is_active)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?, 0, ?, ?, ?, now(), 1)`,
		strings.ToUpper(p.PromoCode),
		p.Title,
		p.Type,
		p.MinProduct,
		p.MinPembayaran,
		p.PromoSatuan,
		p.PromoValue,
		p.MaxReduction,
		p.MaxCount,
		p.MaxBudget,
		p.PeriodStart,
		p.PeriodEnd,
		p.ModifiedBy)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/promo/new")
	}

	c.Flash.Success("New promo data is added")
	return c.Redirect("/dashboard/master/promo")
}
func (c Master) EditPromoHandler(id string, p models.PromoData) revel.Result {
	return c.Redirect("/dashboard/master/promo")
}

func (c Master) DeletePromo(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_promo
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 0
		WHERE promo_code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/dashboard/master/promo")
	}

	c.Flash.Success("Data is deleted")
	return c.Redirect("/dashboard/master/promo")
}

func (c Master) ReactivePromo(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_promo
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE promo_code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactive data! " + err.Error())
		return c.Redirect("/dashboard/master/promo")
	}

	c.Flash.Success("Data is actived")
	return c.Redirect("/dashboard/master/promo")
}

// ==================================== Promo Helper Function ====================================
func (c Master) getAllPromo() ([]models.PromoData, error) {
	data := []models.PromoData{}
	_, err := Dbm.Select(&data,
		`SELECT * FROM mst_promo`)
	if err != nil {
		panic(err)
	}

	loc, _ := time.LoadLocation("Asia/Jakarta")
	for i, item := range data {
		data[i].StartDateText = item.PeriodStart.In(loc).Format("02 Jan 2006")
		data[i].EndDateText = item.PeriodEnd.In(loc).Format("02 Jan 2006")
	}

	return data, nil
}

func (c Master) getPromos() []models.PromoData {
	promos := []models.PromoData{}
	_, err := Dbm.Select(&promos,
		`SELECT * FROM mst_promo WHERE is_active = 1`)
	if err != nil {
		panic(err)
	}
	return promos
}

func (c Master) getPromo(code string) *models.PromoData {
	result, err := Dbm.Select(models.PromoData{},
		`SELECT * FROM mst_promo WHERE promo_code = ?`, code)
	if err != nil {
		panic(err)
	}

	return result[0].(*models.PromoData)
}
