package controllers

import "github.com/revel/revel"

// ==================================== Restock GET Handler ====================================
func (c Master) NewRestock() revel.Result {

	return c.RenderTemplate("Dashboard/Restock/restock.html")
}

func (c Master) HistoryRestock() revel.Result {

	return c.RenderTemplate("Dashboard/Restock/restock_history.html")
}
