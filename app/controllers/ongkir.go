package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

type Ongkir struct {
	*revel.Controller
}

const APIkey = "3419a0a6d37d157a06888d0c907fe818"

func getJson(target interface{}) error {
	url := "https://pro.rajaongkir.com/api/province"
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("key", APIkey)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	return json.NewDecoder(res.Body).Decode(target)
}

// func (c Ongkir) Province() models.Province{} {

// 	url := "https://api.rajaongkir.com/starter/province"
// 	req, err := http.NewRequest("GET", url, nil)

// 	req.Header.Add("key", APIkey)

// 	res, err := http.DefaultClient.Do(req)
// 	if err != nil {
// 		panic(err)
// 	}

// 	defer res.Body.Close()
// 	body, _ := ioutil.ReadAll(res.Body)

// 	var data := models.Province
// 	err = json.Unmarshal(body, &data)
// 	if err != nil {
// 		panic(err)
// 	}

// 	return data
// }

func City(provinceID int) {
	url := "https://pro.rajaongkir.com/api/city?province=%d"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("key", APIkey)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}

func Cost(dari, ke, berat int, kurir string) {

	url := "https://pro.rajaongkir.com/api/cost"

	payload := strings.NewReader("origin=%d&destination=%d&weight=%d&courier=%s")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("key", APIkey)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}

func GetBranch() []models.Kota {
	kota := []models.Kota{}
	_, err := Dbm.Select(&kota,
		`SELECT 
			id, type, name
		FROM 
			ongkir_city WHERE id IN (115, 501)`)
	if err != nil {
		panic(err)
	}

	return kota
}

func GetKota() []models.Kota {
	kota := []models.Kota{}
	_, err := Dbm.Select(&kota,
		`SELECT 
			id, type, name
		FROM 
			ongkir_city WHERE type = 'Kota'`)
	if err != nil {
		panic(err)
	}

	return kota
}
