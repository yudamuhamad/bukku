package controllers

import (
	"regexp"

	"github.com/revel/revel"
)

// Register : handler for register get request
func (c App) Register() revel.Result {
	if user := c.connected(); user != nil {
		return c.Redirect(Dashboard.Index)
	}
	return c.Render()
}

// RegisterHandler : handler for register post request
// Do some validation before registering user in database
func (c App) RegisterHandler(name, username, password, repassword string) revel.Result {
	if user := c.connected(); user != nil {
		return c.Redirect(Dashboard.Index)
	}

	// Basic validation proccess
	c.Flash.Out["name"] = name
	c.Flash.Out["username"] = username
	c.Validation.Required(name).Message("Name is required!")
	c.Validation.Required(username).Message("Username is required!")
	c.Validation.Required(password).Message("Password is required!")
	c.Validation.Required(repassword).Message("Password confirmation is required!")
	var repasswordRgx, err = regexp.Compile(repassword)
	if err != nil {
		c.Flash.Error("Password confirmation is error: " + err.Error())
		return c.Redirect(App.Register)
	}
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Register)
	}

	// Minimum size validation
	c.Validation.MinSize(name, 5).Message("Name require at least 5 characters")
	c.Validation.MinSize(username, 5).Message("Username require at least 5 characters")
	c.Validation.MinSize(password, 6).Message("Password require at least 6 characters")
	c.Validation.Match(password, repasswordRgx).Message("Password confirmation is not match!")
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Register)
	}

	// Check if user already exist
	// user, _ := c.getUserData(username)
	// if user != nil {
	// 	c.Flash.Error("User already exist! please use other username")
	// 	return c.Redirect(App.Register)
	// }

	// Register user
	var query, errq = c.Txn.Prepare(`
			INSERT INTO mst_user (Username, password, full_name, role_code, modified_by, modified_date, is_active) 
			VALUES (?, ?, ?, 'ADM', 'admin', now(), 1)
		`)
	if errq != nil {
		c.Flash.Error("Registration error: " + errq.Error())
		return c.Redirect(App.Register)
	}
	var _, errx = query.Exec(username, password, name)
	if errx != nil {
		c.Flash.Error("Registration error: " + errx.Error())
		return c.Redirect(App.Register)
	}

	// Registration is success
	c.Flash.Success("Welcome, " + username)
	c.Session["username"] = username
	return c.Redirect(App.Index)
}
