package controllers

import (
	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Customer GET Handler ====================================
func (c Master) Customer() revel.Result {
	data, err := getAllCustomer()
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/customer.html")
}

func (c Master) ViewCustomer(id int) revel.Result {
	data, err := getCustomerById(id)
	if err != nil {
		panic(err)
	}

	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/customer_view.html")
}

// ==================================== Customer POST Handler ====================================

// ==================================== Customer Helper Function ====================================
func getAllCustomer() ([]models.CustomerData, error) {
	data := []models.CustomerData{}

	_, err := Dbm.Select(&data, "SELECT * FROM mst_customer WHERE is_active = 1")
	if err != nil {
		return nil, err
	}

	// for i, item := range data {
	// 	customer, err := getCustomerById(item.ID)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	address := getFormatedAddress(customer, false)
	// 	data[i].FormatedAddress = strings.Replace(address, "\n", "<br>", -1)
	// }

	return data, nil
}

func GetCustomerID(model *models.CustomerData) int {
	tx, err := Dbm.Begin()
	if err != nil {
		return 0
	}
	var result = 0
	err = tx.QueryRow(`
		SELECT id
		FROM mst_customer 
		WHERE full_name = ? AND phone_1 = ?`, model.Name, model.Phone).Scan(&result)
	if err != nil {
		return 0
	}

	return result
}

func getCustomerById(id int) (*models.CustomerAddressAPI, error) {
	tx, err := Dbm.Begin()
	if err != nil {
		return nil, err
	}

	var result models.CustomerAddressAPI
	err = tx.QueryRow(`
		SELECT 
			mst_customer.id AS id,
			username,
			full_name,
			phone_1,
			sex,
			photo_url,
			email,
			facebook,
			twitter,
			instagram,
			google,
			line,
			birth_date,
			birth_place,
			created_date,
			state_id,
			province_id,
			city_id,
			kecamatan,
			kelurahan,
			street,
			postal_code
		FROM mst_customer JOIN mst_address 
			ON mst_customer.id = mst_address.customer_id
		WHERE mst_customer.id = ? AND mst_address.is_default = 1`, id).
		Scan(&result.ID, &result.Username, &result.Name, &result.Phone, &result.Sex,
			&result.PhotoUrl,
			&result.Email,
			&result.Facebook,
			&result.Twitter,
			&result.Instagram,
			&result.Google,
			&result.Line,
			&result.BirthDate,
			&result.BirthPlace,
			&result.CreatedDate,
			&result.StateID, &result.ProvinceID, &result.CityID, &result.Kecamatan, &result.Kelurahan, &result.Street, &result.PostalCode)
	if err != nil {
		panic(err)
	}
	result.FormatedAddress = getFormatedAddress(&result, true)

	return &result, nil
}

func getAllCustomerAddress() ([]models.CustomerAddressAPI, error) {
	data := []models.CustomerAddressAPI{}
	_, err := Dbm.Select(&data, `
		SELECT 
				mst_customer.id AS id, 
				full_name, 
				phone_1, 
				state_id, 
				province_id, 
				city_id, 
				kecamatan, 
				kelurahan, 
				street, 
				postal_code
			FROM 
				mst_customer JOIN 
				mst_address ON mst_customer.id = mst_address.customer_id
			WHERE mst_customer.is_active = 1
		`)
	if err != nil {
		return nil, err
	}

	for i, item := range data {
		data[i].FormatedAddress = getFormatedAddress(&item, false)
	}

	return data, nil
}

func getFormatedAddress(customer *models.CustomerAddressAPI, withPhone bool) string {
	addressFormated := customer.Street + "\n"
	if customer.Kelurahan != "" {
		addressFormated = addressFormated + customer.Kelurahan
	}
	// if customer.Kelurahan != "" {
	// 	addressFormated = addressFormated + ", " + customer.Kelurahan
	// }
	if customer.Kecamatan != "" {
		addressFormated = addressFormated + ", " + customer.Kecamatan
	}
	if customer.CityID != 0 {
		for _, city := range Static.DataCity {
			if city.ID == customer.CityID {
				addressFormated = addressFormated + ", " + city.Name
				break
			}
		}
	}
	if customer.ProvinceID != 0 {
		for _, province := range Static.DataProvince {
			if province.ID == customer.ProvinceID {
				addressFormated = addressFormated + ", " + province.Name
				break
			}
		}
	}
	if customer.StateID != 0 {
		for _, state := range Static.DataState {
			if state.ID == customer.StateID {
				addressFormated = addressFormated + ", " + state.Name
				break
			}
		}
	}
	addressFormated = addressFormated + " (" + customer.PostalCode + ") "
	if withPhone {
		addressFormated = addressFormated + "Ph : " + customer.Phone
	}

	return addressFormated
}
