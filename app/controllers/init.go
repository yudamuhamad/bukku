package controllers

import (
	"strings"

	"github.com/revel/revel"
)

func init() {
	revel.OnAppStart(InitDB)
	revel.InterceptMethod((*GorpController).Begin, revel.BEFORE)
	revel.InterceptMethod(App.AddUser, revel.BEFORE)
	revel.InterceptFunc(checkAuth, revel.BEFORE, &Dashboard{})
	revel.InterceptMethod((*GorpController).Commit, revel.AFTER)
	revel.InterceptMethod((*GorpController).Rollback, revel.FINALLY)

}

func checkAuth(c *revel.Controller) revel.Result {
	if username, ok := c.Session["username"]; ok && strings.Trim(username, " ") != "" {
		return nil
	}
	for k := range c.Session {
		delete(c.Session, k)
	}
	c.Flash.Error("Please log in first")
	return c.Redirect("/")
}
