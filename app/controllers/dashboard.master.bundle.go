package controllers

import (
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Bundle GET Handler ====================================
func (c Master) Bundle() revel.Result {
	data := c.getAllBundle()
	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/bundle.html")
}

func (c Master) NewBundle() revel.Result {
	books := c.getBook()
	c.ViewArgs["books"] = books
	return c.RenderTemplate("Dashboard/Master/bundle_new.html")
}

func (c Master) EditBundle(id string) revel.Result {
	data := c.getBundleFromCode(id)
	books := c.getBook()
	c.ViewArgs["data"] = data
	c.ViewArgs["books"] = books
	return c.RenderTemplate("Dashboard/Master/bundle_edit.html")
}

// ==================================== Bundle POST Handler ====================================
func (c Master) DeleteBundle(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE ms_product
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 0
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/dashboard/master/bundle")
	}

	c.Flash.Success("Data is deleted")
	return c.Redirect("/dashboard/master/bundle")
}

func (c Master) ReactiveBundle(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE ms_product
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactive data! " + err.Error())
		return c.Redirect("/dashboard/master/bundle")
	}

	c.Flash.Success("Data is actived")
	return c.Redirect("/dashboard/master/bundle")
}

func (c Master) NewBundleHandler(data models.BundleInput, mainImage []byte, additionalImage1 []byte, additionalImage2 []byte) revel.Result {
	data.BookCode = c.Params.Form["BookCode"]
	data.ModifiedBy = c.connected().Username

	data.Price = data.NormalPrice - (data.NormalPrice * data.Discount / 100)

	timenow := time.Now().Format("060102150405")
	var err error

	// Uploading file
	var filename string
	if len(mainImage) > 0 {
		filename := timenow + c.getBookImageName(c.Params.Files["mainImage"][0].Filename, "bundle", data.Title, "0")
		err = uploadImage("product", filename, mainImage)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
		data.MainImage = "http://img.bukku.id/img/product/" + filename
	}
	if len(additionalImage1) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["additionalImage1"][0].Filename, "bundle", data.Title, "0")
		err = uploadImage("product", filename, additionalImage1)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
		data.AdditionalImage1 = "http://img.bukku.id/img/product/" + filename
	}
	if len(additionalImage2) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["additionalImage2"][0].Filename, "bundle", data.Title, "0")
		err = uploadImage("product", filename, additionalImage2)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
		data.AdditionalImage2 = "http://img.bukku.id/img/product/" + filename
	}

	// Begin database transaction
	tx, err := Dbm.Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	// Insert into product
	stmt1, err := tx.Prepare(`
			INSERT INTO ms_product (
				code,
				title,
				type,
				purchase_price,
				percentage_price,
				normal_price,
				sell_price,
				image1,
				image2,
				image3,
				notes,
				status,
				modified_by,
				modified_date,
				is_active
			)
			VALUES (?,?,"bundle",?,?,?,?,?,?,?,?,?,?,now(),1)
		`)
	if err != nil {
		c.Flash.Error("Error saving product data! " + err.Error())
		c.FlashParams()
		return c.Redirect(Master.NewBundle)
	}
	// Insert into bundle
	stmt3, err := tx.Prepare(`
			INSERT INTO mst_bundle (
				code,
				book_code
			)
			VALUES (?,?)
		`)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect(Master.NewBundle)
	}

	// Execute Query
	_, err = stmt1.Exec(data.Code, data.Title, data.PurchasePrice, data.Discount, data.NormalPrice, data.Price,
		data.MainImage, data.AdditionalImage1, data.AdditionalImage2, data.Notes, data.Status, data.ModifiedBy)
	for _, code := range data.BookCode {
		_, err = stmt3.Exec(data.Code, code)
		if err != nil {
			c.Flash.Error("Error saving data! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
	}

	c.Flash.Success("New bundle data is added")
	return c.Redirect("/dashboard/master/bundle")
}

func (c Master) EditBundleHandler(id string, data models.BundleInput, mainImage []byte, additionalImage1 []byte, additionalImage2 []byte) revel.Result {
	var err error
	oldValue := c.getBundleFromCode(id)
	data.Code = id
	data.Price = data.NormalPrice - (data.NormalPrice * data.Discount / 100)
	data.MainImage = oldValue.MainImage
	data.AdditionalImage1 = oldValue.AdditionalImage1.String
	data.AdditionalImage2 = oldValue.AdditionalImage2.String
	data.BookCode = c.Params.Form["BookCode"]
	data.ModifiedBy = c.connected().Username

	timenow := time.Now().Format("060102150405")
	// Uploading file
	var filename string
	if len(mainImage) > 0 {
		filename := timenow + c.getBookImageName(c.Params.Files["mainImage"][0].Filename, "bundle", data.Title, "0")
		err = uploadImage("product", filename, mainImage)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
		data.MainImage = "http://img.bukku.id/img/product/" + filename
	} else {
		data.MainImage = oldValue.MainImage
	}
	if len(additionalImage1) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["additionalImage1"][0].Filename, "bundle", data.Title, "0")
		err = uploadImage("product", filename, additionalImage1)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
		data.AdditionalImage1 = "http://img.bukku.id/img/product/" + filename
	} else {
		data.AdditionalImage1 = oldValue.AdditionalImage1.String
	}
	if len(additionalImage2) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["additionalImage2"][0].Filename, "bundle", data.Title, "0")
		err = uploadImage("product", filename, additionalImage2)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect(Master.NewBundle)
		}
		data.AdditionalImage2 = "http://img.bukku.id/img/product/" + filename
	} else {
		data.AdditionalImage2 = oldValue.AdditionalImage2.String
	}

	// Begin database transaction
	tx, err := Dbm.Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	// Edit product detail
	stmt1, err := tx.Prepare(`
		UPDATE ms_product SET
			title = ?,
			purchase_price = ? ,
			percentage_price = ? ,
			normal_price = ? ,
			sell_price = ? ,
			image1 = ?,
			image2 = ?,
			image3 = ?,
			notes = ?,
			status = ?,
			modified_by = ?,
			modified_date = now()
		WHERE code = ?
	`)
	// Delete all book from bundle (reset)
	stmt2, err := tx.Prepare(`
		DELETE FROM mst_bundle WHERE code = ?
	`)
	// Insert into bundle
	stmt3, err := tx.Prepare(`
		INSERT INTO mst_bundle (
			code,
			book_code
		)
		VALUES (?,?)
	`)

	_, err = stmt1.Exec(data.Title, data.PurchasePrice, data.Discount, data.NormalPrice, data.Price,
		data.MainImage, data.AdditionalImage1, data.AdditionalImage2, data.Notes, data.Status, data.ModifiedBy, data.Code)
	_, err = stmt2.Exec(id)
	for _, code := range data.BookCode {
		_, err = stmt3.Exec(data.Code, code)
		if err != nil {
			c.Flash.Error("Error saving data! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/bundle/%s/edit", id)
		}
	}

	c.Flash.Success("Bundle data edited!")
	return c.Redirect("/dashboard/master/bundle")
	//return c.Redirect("/dashboard/master/bundle/%s/edit", id)
}

// ==================================== Bundle Helper Function ====================================
func (c Master) getAllBundle() []models.BundleView {
	bundles := []models.BundleView{}
	_, err := Dbm.Select(&bundles,
		`SELECT 
			code, 
			title, 
			sell_price, 
			image1 AS image, 
			status,
			is_active
		FROM 
			ms_product
		WHERE type="bundle"`)
	if err != nil {
		panic(err)
	}

	// Find book titles and count stock
	for i, bundle := range bundles {
		bookData := []struct {
			Title string
			Stock int
		}{}
		_, err := Dbm.Select(&bookData,
			`SELECT title, current_stock AS stock FROM mst_bundle JOIN ms_product ON mst_bundle.book_code = ms_product.code JOIN mst_book ON ms_product.code = mst_book.code WHERE mst_bundle.code = ?`, bundle.Code)
		if err != nil {
			panic(err)
		}

		bundles[i].Stock = -1
		for _, data := range bookData {
			bundles[i].BookTitles = append(bundles[i].BookTitles, data.Title)
			if (data.Stock < bundles[i].Stock && bundles[i].Stock != -1) || (bundles[i].Stock == -1) {
				bundles[i].Stock = data.Stock
			}
		}

		bundles[i].PriceFormat = decimalToCurrency("Rp", bundle.Price)
		bundles[i].StatusColor = "success"
		bundles[i].StatusFormat = "Published"
		if bundle.Status == 0 {
			bundles[i].StatusColor = "warning"
			bundles[i].StatusFormat = "Draft"
		}

	}

	return bundles
}

func getAllBundle() []models.BundleView {
	bundles := []models.BundleView{}
	tx, err := Dbm.Begin()
	if err != nil {
		panic(err)
	}

	_, err = tx.Select(&bundles,
		`SELECT 
			code, 
			title, 
			sell_price, 
			image1 AS image, 
			status
		FROM 
			ms_product
		WHERE is_active = 1 AND type="bundle"`)
	if err != nil {
		panic(err)
	}

	// Find book titles and count stock & weight
	for i, bundle := range bundles {
		bookData := []struct {
			Title  string
			Stock  int
			Weight int
		}{}
		_, err := tx.Select(&bookData,
			`SELECT title, current_stock AS stock, weight FROM mst_bundle JOIN ms_product ON mst_bundle.book_code = ms_product.code JOIN mst_book ON ms_product.code = mst_book.code WHERE mst_bundle.code = ?`, bundle.Code)
		if err != nil {
			panic(err)
		}

		bundles[i].Weight = 0
		bundles[i].Stock = -1
		for _, data := range bookData {
			bundles[i].Weight = bundles[i].Weight + data.Weight
			bundles[i].BookTitles = append(bundles[i].BookTitles, data.Title)
			if (data.Stock < bundles[i].Stock && bundles[i].Stock != -1) || (bundles[i].Stock == -1) {
				bundles[i].Stock = data.Stock
			}
		}

		bundles[i].PriceFormat = decimalToCurrency("Rp", bundle.Price)
		bundles[i].StatusColor = "success"
		bundles[i].StatusFormat = "Published"
		if bundle.Status == 0 {
			bundles[i].StatusColor = "warning"
			bundles[i].StatusFormat = "Draft"
		}

	}

	return bundles
}

func (c Master) getBundleFromCode(code string) *models.Bundle {
	var err error
	c.Begin()
	defer func() {
		if err != nil {
			c.Rollback()
		}
	}()
	result, err := Dbm.Select(models.Bundle{},
		`SELECT 
			code, title, purchase_price, percentage_price, normal_price, sell_price, image1, image2, image3,  notes, status
		FROM 
			ms_product 
		WHERE is_active = 1 AND type = "bundle" AND code = ?`, code)
	if err != nil {
		panic(err)
	}

	bookCode := []string{}
	_, err = Dbm.Select(&bookCode,
		`SELECT 
			book_code
		FROM 
			mst_bundle 
		WHERE code = ?`, code)
	if err != nil {
		panic(err)
	}

	data := result[0].(*models.Bundle)
	data.BookCode = bookCode

	c.Commit()
	return data
}

func getBundleFromCode(code string) *models.Bundle {
	tx, err := Dbm.Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()
	if err != nil {
		panic(err)
	}

	result, err := tx.Select(models.Bundle{},
		`SELECT 
			code, title, percentage_price, normal_price, sell_price, 
			image1, image2, image3, purchase_price, percentage_price, normal_price, sell_price, notes, status
		FROM 
			ms_product 
		WHERE is_active = 1 AND type = "bundle" AND code = ?`, code)
	if err != nil {
		panic(err)
	}

	bookCode := []string{}
	_, err = tx.Select(&bookCode,
		`SELECT 
			book_code
		FROM 
			mst_bundle 
		WHERE code = ?`, code)
	if err != nil {
		panic(err)
	}

	data := result[0].(*models.Bundle)
	data.BookCode = bookCode

	tx.Commit()
	return data
}
