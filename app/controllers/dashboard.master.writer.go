package controllers

import (
	"strconv"
	"strings"
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Writer GET Handler ====================================
func (c Master) Writer() revel.Result {
	data := c.getAllWriter()

	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/writer.html")
}

func (c Master) NewWriter() revel.Result {
	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	return c.RenderTemplate("Dashboard/Master/writer_new.html")
}

func (c Master) EditWriter(id string) revel.Result {
	data := c.getWriter(id)
	c.ViewArgs["p"] = data

	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	return c.RenderTemplate("Dashboard/Master/writer_edit.html")
}

// ==================================== Writer POST Handler ====================================
func (c Master) NewWriterHandler(p models.Writer) revel.Result {
	p.ModifiedBy = c.connected().Username
	p.ModifiedDate = time.Now()

	kodepos := c.Params.Form.Get("kodepos")
	postalCode, _ := strconv.ParseInt(kodepos, 10, 64)
	p.PostalCode = postalCode

	_, err := Dbm.Exec(`
		INSERT INTO mst_writer (
			code, 
			full_name, 
			phone_1, 
			phone_2, 
			email, 
			facebook, 
			twitter, 
			instagram,	
			state_id, 
			province_id, 
			city_id, 
			kecamatan, 
			kelurahan, 
			street, 
			postal_code,
			modified_by,
			modified_date,
			is_active )
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), 1)`,
		strings.ToUpper(p.Code),
		p.Name,
		p.Phone1,
		p.Phone2,
		p.Email,
		p.Facebook,
		p.Twitter,
		p.Instagram,
		p.State,
		p.Province,
		p.City,
		p.Kecamatan,
		p.Kelurahan,
		p.Street,
		p.PostalCode,
		p.ModifiedBy)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/writer/new")
	}

	c.Flash.Success("New writer data is added")
	return c.Redirect("/dashboard/master/writer")
}

func (c Master) EditWriterHandler(id string, p models.Writer) revel.Result {
	p.ModifiedBy = c.connected().Username
	p.ModifiedDate = time.Now()

	_, err := Dbm.Exec(`UPDATE mst_writer
		SET 
			full_name = ? ,
			phone_1 = ? ,
			phone_2 = ? ,
			email = ? ,
			facebook = ? ,
			twitter = ? ,
			instagram = ? ,
			state_id = ? ,
			province_id = ? ,
			city_id = ? ,
			kecamatan = ? ,
			kelurahan = ? ,
			street = ? ,
			postal_code = ? ,
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE code = ?`,
		p.Name,
		p.Phone1,
		p.Phone2,
		p.Email,
		p.Facebook,
		p.Twitter,
		p.Instagram,
		p.State,
		p.Province,
		p.City,
		p.Kecamatan,
		p.Kelurahan,
		p.Street,
		p.PostalCode,
		p.ModifiedBy,
		id)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		return c.Redirect("/dashboard/master/writer/%s/edit", id)
	}

	c.Flash.Success("Data is edited")
	return c.Redirect("/dashboard/master/writer")
}

func (c Master) DeleteWriter(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_writer
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 0
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/dashboard/master/writer")
	}

	c.Flash.Success("Data is deleted")
	return c.Redirect("/dashboard/master/writer")
}

func (c Master) ReactiveWriter(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_writer
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactive data! " + err.Error())
		return c.Redirect("/dashboard/master/writer")
	}

	c.Flash.Success("Data is actived")
	return c.Redirect("/dashboard/master/writer")
}

// ==================================== Helper function ====================================
func (c Master) getAllWriter() []models.Writer {
	var writers []models.Writer
	_, err := Dbm.Select(&writers,
		`SELECT * FROM mst_writer`)
	if err != nil {
		panic(err)
	}
	return writers
}

func (c Master) getWriters() []models.Writer {
	var writers []models.Writer
	_, err := Dbm.Select(&writers,
		`SELECT * FROM mst_writer WHERE is_active = 1`)
	if err != nil {
		panic(err)
	}
	return writers
}

func (c Master) getWriter(code string) *models.Writer {
	result, err := Dbm.Select(models.Writer{},
		`SELECT * FROM mst_writer WHERE is_active = 1 AND code = ?`, code)
	if err != nil {
		panic(err)
	}
	return result[0].(*models.Writer)
}
