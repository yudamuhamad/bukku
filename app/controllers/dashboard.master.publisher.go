package controllers

import (
	"strconv"
	"strings"
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Publisher GET Handler ====================================
func (c Master) Publisher() revel.Result {
	data := c.getAllPublisher()

	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/publisher.html")
}

func (c Master) NewPublisher() revel.Result {
	officialtype := c.getConfig("OfficialType")

	c.ViewArgs["officialtype"] = officialtype
	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	return c.RenderTemplate("Dashboard/Master/publisher_new.html")
}

func (c Master) EditPublisher(id string) revel.Result {
	data := c.getPublisherData(id)
	officialtype := c.getConfig("OfficialType")

	c.ViewArgs["ID"] = id
	c.ViewArgs["p"] = data
	c.ViewArgs["officialtype"] = officialtype
	c.ViewArgs["states"] = Static.DataState
	c.ViewArgs["provinces"] = Static.DataProvince
	c.ViewArgs["cities"] = Static.DataCity
	return c.RenderTemplate("Dashboard/Master/publisher_edit.html")
}

// ==================================== Publisher POST Handler ====================================
func (c Master) NewPublisherHandler(p models.Publisher) revel.Result {
	p.ModifiedBy = c.connected().Username
	p.ModifiedDate = time.Now()

	p.OfficialType = c.Params.Form.Get("OfficialType")
	kodepos := c.Params.Form.Get("kodepos")
	postalCode, _ := strconv.ParseInt(kodepos, 10, 64)
	p.PostalCode = postalCode

	_, err := Dbm.Exec(`INSERT INTO mst_publisher (
			code, 
			name, 
			official_type, 
			phone_1, 
			phone_2, 
			email, 
			facebook, 
			twitter, 
			instagram, 
			state_id, 
			province_id, 
			city_id, 
			kecamatan, 
			kelurahan, 
			street, 
			postal_code,
			modified_by, 
			modified_date)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?,	?, ?, ?, ?, ?, ?, ?, ?, now(), ?)`,
		strings.ToUpper(p.Code),
		p.Name,
		p.OfficialType,
		p.Phone1,
		p.Phone2,
		p.Email,
		p.Facebook,
		p.Twitter,
		p.Instagram,
		p.State,
		p.Province,
		p.City,
		p.Kecamatan,
		p.Kelurahan,
		p.Street,
		p.PostalCode,
		p.ModifiedBy)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/publisher/new")
	}

	c.Flash.Success("New publisher data is added")
	return c.Redirect("/dashboard/master/publisher")
}

func (c Master) EditPublisherHandler(id string, p models.Publisher) revel.Result {
	p.ModifiedBy = c.connected().Username
	p.ModifiedDate = time.Now()
	p.OfficialType = c.Params.Form.Get("OfficialType")

	_, err := Dbm.Exec(`UPDATE mst_publisher
		SET 
			name = ? ,
			official_type = ? ,
			phone_1 = ? ,
			phone_2 = ? ,
			email = ? ,
			facebook = ? ,
			twitter = ? ,
			instagram = ? ,
			state_id = ? ,
			province_id = ? ,
			city_id = ? ,
			kecamatan = ? ,
			kelurahan = ? ,
			street = ? ,
			postal_code = ? ,
			modified_by = ? ,
			modified_date = now()
		WHERE code = ?`,
		p.Name,
		p.OfficialType,
		p.Phone1,
		p.Phone2,
		p.Email,
		p.Facebook,
		p.Twitter,
		p.Instagram,
		p.State,
		p.Province,
		p.City,
		p.Kecamatan,
		p.Kelurahan,
		p.Street,
		p.PostalCode,
		p.ModifiedBy, id)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		return c.Redirect("/dashboard/master/publisher/%s/edit", id)
	}

	c.Flash.Success("Data is edited")
	return c.Redirect("/dashboard/master/publisher")
}

func (c Master) DeletePublisher(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_publisher
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 0
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/dashboard/master/publisher")
	}

	c.Flash.Success("Data is deleted")
	return c.Redirect("/dashboard/master/publisher")
}

func (c Master) ReactivePublisher(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_publisher
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactive data! " + err.Error())
		return c.Redirect("/dashboard/master/publisher")
	}

	c.Flash.Success("Data is actived")
	return c.Redirect("/dashboard/master/publisher")
}

// ==================================== Helper function ====================================
func (c Master) getAllPublisher() []models.Publisher {
	publishers := []models.Publisher{}
	_, err := Dbm.Select(&publishers,
		`SELECT * FROM mst_publisher`)
	if err != nil {
		panic(err)
	}
	return publishers
}
func (c Master) getPublisher() []models.Publisher {
	publishers := []models.Publisher{}
	_, err := Dbm.Select(&publishers,
		`SELECT * FROM mst_publisher WHERE is_active = 1`)
	if err != nil {
		panic(err)
	}
	return publishers
}

func (c Master) getPublisherData(id string) *models.Publisher {
	result, err := Dbm.Select(models.Publisher{},
		`SELECT * FROM mst_publisher WHERE is_active = 1 AND code = ?`, id)
	if err != nil {
		panic(err)
	}
	return result[0].(*models.Publisher)
}
