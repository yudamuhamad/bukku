package controllers

import (
	"database/sql"
	"fmt"
	"strings"

	m "bukku.co.id/app/models"
	"github.com/go-gorp/gorp"
	r "github.com/revel/revel"
)

var (
	Dbm *gorp.DbMap
)

// InitDB : create sql connection and prepare database map using Gorp
func InitDB() {
	connectionString := getConnectionString()
	r.AppLog.Info("BUKKU >> Init database connection on " + connectionString)
	if db, err := sql.Open("mysql", connectionString); err != nil {
		r.ERROR.Fatal(err)
	} else {
		Dbm = &gorp.DbMap{
			Db:      db,
			Dialect: gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8"}}
	}
	initDBScheme()
}

func initDBScheme() {
	r.AppLog.Info("BUKKU >> Configuring database scheme")

	// Adding table
	Dbm.AddTableWithName(m.StateData{}, "ongkir_state").SetKeys(false, "ID")
	Dbm.AddTableWithName(m.ProvinceData{}, "ongkir_province").SetKeys(false, "ID")
	Dbm.AddTableWithName(m.CityData{}, "ongkir_city").SetKeys(false, "ID")
	Dbm.AddTableWithName(m.CustomerData{}, "mst_customer").SetKeys(true, "ID")
	Dbm.AddTableWithName(m.AddressData{}, "mst_address").SetKeys(true, "ID")
	Dbm.AddTableWithName(m.ProductData{}, "ms_product").SetKeys(false, "Code")
	Dbm.AddTableWithName(m.TransactionData{}, "tr_transaction").SetKeys(true, "ID")
	Dbm.AddTableWithName(m.TransactionDetailData{}, "tr_transaction_detail")
	err := Dbm.CreateTablesIfNotExists()
	if err != nil {
		panic(err)
	}

	// Add static state data
	r.AppLog.Info("BUKKU >> Bulk insert ongkir data")
	bulkInsertStaticState()
	bulkInsertProvince()
	bulkInsertCity()
}

// MySQL Connectionstring
func getParamString(param string, defaultValue string) string {
	p, found := r.Config.String(param)
	if !found {
		if defaultValue == "" {
			r.ERROR.Fatal("Cound not find parameter: " + param)
		} else {
			return defaultValue
		}
	}
	return p
}

func getConnectionString() string {
	host := getParamString("db.host", "127.0.0.1")
	port := getParamString("db.port", "3306")
	user := getParamString("db.user", "root")
	pass := getParamString("db.password", "")
	dbname := getParamString("db.name", "bukku")
	protocol := getParamString("db.protocol", "tcp")
	dbargs := getParamString("dbargs", " ")

	if strings.Trim(dbargs, " ") != "" {
		dbargs = "?" + dbargs
	} else {
		dbargs = ""
	}
	return fmt.Sprintf("%s:%s@%s([%s]:%s)/%s%s?parseTime=true",
		user, pass, protocol, host, port, dbname, dbargs)
}

type GorpController struct {
	*r.Controller
	Txn *gorp.Transaction
}

func (c *GorpController) Begin() r.Result {
	defer recover()
	txn, err := Dbm.Begin()
	if err != nil {
		//panic(err)
		fmt.Printf("Error:", err)
	}

	c.Txn = txn

	return nil
}
func catch() {
	if r := recover(); r != nil {
		fmt.Println("Error occured", r)
	} else {
		fmt.Println("Application running perfectly")
	}
}

func (c *GorpController) Commit() r.Result {
	if c.Txn == nil {
		return nil
	}
	if err := c.Txn.Commit(); err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Txn = nil
	return nil
}

func (c *GorpController) Rollback() r.Result {
	if c.Txn == nil {
		return nil
	}
	if err := c.Txn.Rollback(); err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Txn = nil
	return nil
}
