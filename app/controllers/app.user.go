package controllers

import (
	"errors"
	"strconv"
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

type User struct {
	App
	Dashboard
}

// ==================================== User GET Handler ====================================
func (c User) User() revel.Result {
	data := c.getAllUser()
	roles := c.getRole()
	kota := GetBranch()

	c.ViewArgs["data"] = data
	c.ViewArgs["roles"] = roles
	c.ViewArgs["kota"] = kota
	return c.RenderTemplate("Dashboard/User/user.html")
}

func (c User) NewUser() revel.Result {
	roles := c.getRole()

	c.ViewArgs["roles"] = roles
	return c.RenderTemplate("Dashboard/User/user_new.html")
}

func (c User) EditUser(id string) revel.Result {
	data := c.getUserFromID(id)
	roles := c.getRole()
	kota := GetBranch()

	c.ViewArgs["u"] = data
	c.ViewArgs["roles"] = roles
	c.ViewArgs["kota"] = kota
	return c.RenderTemplate("Dashboard/User/user_edit.html")
}

// ==================================== User POST Handler ====================================
func (c User) NewUserHandler(data models.User) revel.Result {
	// Check if user already exist
	user, _ := c.getExistUser(data.Username)
	if user != nil {
		c.Flash.Error("User already exist! please use other username")
		return c.Redirect("/user")
	}

	encryptPassword := c.encryptString(data.Password)
	data.Role = c.Params.Form.Get("RoleCode")

	branchCode := c.Params.Form.Get("BranchCode")
	branchCodeInt, _ := strconv.ParseInt(branchCode, 10, 64)
	data.Branch = branchCodeInt

	_, err := Dbm.Exec(`INSERT INTO mst_user (
		username, 
		password, 
		full_name, 
		role_code, 
		branch_code,
		modified_by, 
		modified_date, 
		is_active) 
		VALUES (?, ?, ?, ?, ?, ?, now(), 1)`,
		data.Username,
		encryptPassword,
		data.FullName,
		data.Role,
		data.Branch,
		c.connected().Username)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/user")
	}
	c.Flash.Success("New user is added")
	return c.Redirect("/user")
}

func (c User) EditUserHandler(id string, u models.User, profileImage []byte) revel.Result {
	var err error
	timenow := time.Now().Format("060102150405")
	// Uploading file
	var filename string
	if len(profileImage) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["profileImage"][0].Filename, u.Username, "", "")
		err = uploadImage("profile", filename, profileImage)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/user")
		}
		u.URLProfile = "http://img.bukku.id/img/profile/" + filename
	}

	u.Role = c.Params.Form.Get("RoleCode")
	_, err = Dbm.Exec(`UPDATE mst_user 
		SET  
			full_name = ?, 
			role_code = ?, 
			url_profile = ?,
			modified_by = ?, 
			modified_date = now() 
		WHERE id = ?`,
		u.FullName,
		u.Role,
		u.URLProfile,
		c.connected().Username,
		id)

	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/user")
	}
	c.Flash.Success("user is updated" + id)
	return c.Redirect("/user")
}

func (c User) DeleteUser(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_user
			SET 
				modified_by = ? ,
				modified_date = now() ,
				is_active = 0
			WHERE id = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/user")
	}

	c.Flash.Success("Data is deleted")
	return c.Redirect("/user")
}

func (c User) ReactiveUser(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_user
			SET 
				modified_by = ? ,
				modified_date = now() ,
				is_active = 1
			WHERE id = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactiving data! " + err.Error())
		return c.Redirect("/user")
	}

	c.Flash.Success("Data is reactived")
	return c.Redirect("/user")
}

func (c User) getAllUser() []models.UserView {
	users := []models.UserView{}
	_, err := Dbm.Select(&users,
		`SELECT 
			U.id,
			username,
			password,
			full_name,
			U.role_code,
			R.role_name,
			B.name as branch_name,
			U.is_active
		FROM 
			mst_user U
			INNER JOIN mst_role R ON R.role_code = U.role_code 
			LEFT JOIN ongkir_city B ON B.id = U.branch_code`)
	if err != nil {
		panic(err)
	}

	return users
}

func (c User) getUserFromID(id string) *models.User {
	result, err := Dbm.Select(models.User{},
		`SELECT 
			id, username, password, full_name, role_code, url_profile, is_active
	   	FROM mst_user WHERE id = ?`, id)
	if err != nil {
		panic(err)
	}

	data := result[0].(*models.User)
	return data
}

func (c User) getExistUser(username string) (*models.User, error) {
	users, err := Dbm.Select(models.User{},
		`select * from mst_user where username = ?`, username)
	if err != nil {
		return nil, err
	}

	if len(users) == 0 {
		return nil, errors.New("User not found")
	}

	return users[0].(*models.User), nil
}
