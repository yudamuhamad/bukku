package controllers

import (
	"strconv"
	"time"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Book GET Handler ====================================
func (c Master) Book() revel.Result {
	data := c.getAllBook()
	c.ViewArgs["data"] = data
	return c.RenderTemplate("Dashboard/Master/book.html")
}

func (c Master) NewBook() revel.Result {
	// Getting writer and publisher data
	writers := c.getWriters()
	publishers := c.getPublisher()
	bookcategory := c.getBookCategory()
	productcategory := c.getConfig("ProductCategory")

	c.ViewArgs["writers"] = writers
	c.ViewArgs["publishers"] = publishers
	c.ViewArgs["bookcategory"] = bookcategory
	c.ViewArgs["productcategory"] = productcategory
	return c.RenderTemplate("Dashboard/Master/book_new.html")
}

func (c Master) EditBook(id string) revel.Result {
	data := c.getBookFromCode(id)
	writers := c.getWriters()
	publishers := c.getPublisher()
	bookcategory := c.getBookCategory()
	productcategory := c.getConfig("ProductCategory")

	c.ViewArgs["data"] = data
	c.ViewArgs["writers"] = writers
	c.ViewArgs["publishers"] = publishers
	c.ViewArgs["bookcategory"] = bookcategory
	c.ViewArgs["productcategory"] = productcategory
	return c.RenderTemplate("Dashboard/Master/book_edit.html")
}

// ==================================== Book POST Handler ====================================
func (c Master) NewBookHandler(data models.BookInput, mainImage []byte, additionalImage1 []byte, additionalImage2 []byte) revel.Result {
	data.WriterCode = c.Params.Form["WriterCode"]
	data.PublisherCode = c.Params.Form.Get("PublisherCode")
	data.ModifiedBy = c.connected().Username
	data.Category = c.Params.Form.Get("ProductCategory")
	data.BookCategory = c.Params.Form.Get("BookCategory")

	data.Price = data.NormalPrice - (data.NormalPrice * data.Discount / 100)
	data.Cover = c.Params.Form.Get("Cover")
	timenow := time.Now().Format("060102150405")
	var err error

	// Uploading file
	if len(mainImage) > 0 {
		filename := timenow + c.getBookImageName(c.Params.Files["mainImage"][0].Filename, data.WriterCode[0], data.Title, "0")
		err = uploadImage("product", filename, mainImage)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/new")
		}
		data.MainImage = "http://img.bukku.id/img/product/" + filename
	}
	if len(additionalImage1) > 0 {
		filename := timenow + c.getBookImageName(c.Params.Files["additionalImage1"][0].Filename, data.WriterCode[0], data.Title, "2")
		err = uploadImage("product", filename, additionalImage1)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/new")
		}
		data.AdditionalImage1 = "http://img.bukku.id/img/product/" + filename
	}
	if len(additionalImage2) > 0 {
		filename := timenow + c.getBookImageName(c.Params.Files["additionalImage2"][0].Filename, data.WriterCode[0], data.Title, "3")
		err = uploadImage("product", filename, additionalImage2)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/new")
		}
		data.AdditionalImage2 = "http://img.bukku.id/img/product/" + filename
	}

	// Begin database transaction
	tx, err := Dbm.Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	// Insert into product
	stmt1, err := tx.Prepare(`
			INSERT INTO ms_product (
				code,
				title,
				type,
				category,
				purchase_price,
				percentage_price,
				normal_price,
				sell_price,
				image1,
				image2,
				image3,
				notes,
				status,
				modified_by,
				modified_date
			)
			VALUES (?,?,"book",?,?,?,?,?,?,?,?,?,?,?,now())
		`)
	if err != nil {
		c.Flash.Error("Error saving product data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}

	// Insert into book
	stmt2, err := tx.Prepare(`
			INSERT INTO mst_book (
				code,
				book_category,
				publisher_code,
				genre,
				weight,
				length,
				total_page,
				bahan_isi,
				book_cover,
				year,
				ISBN,
				bahasa,
				synopsis,
				current_stock
			)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
		`)
	if err != nil {
		c.Flash.Error("Error saving book data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}
	// Insert into book_writer
	stmt3, err := tx.Prepare(`
			INSERT INTO ms_bookwriter (
				book_code,
				writer_code
			)
			VALUES (?,?)
		`)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}
	// Insert into stock
	stmt4, err := tx.Prepare(`
			INSERT INTO tr_stock (
				book_code, 
				stock_type,
				last_stock,
				re_stock,
				restock_date,
				total_stock,
				modified_by,
				modified_date) 
				VALUES (?, ?, ?, ?, now(), ?, ?, now())`)

	// Execute Query
	_, err = stmt1.Exec(data.Code, data.Title, data.Category, data.PurchasePrice, data.Discount, data.NormalPrice,
		data.Price, data.MainImage, data.AdditionalImage1, data.AdditionalImage2, data.Notes, data.Status, data.ModifiedBy)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}
	_, err = stmt2.Exec(data.Code, data.BookCategory, data.PublisherCode, data.Genre, data.Weight, data.Length,
		data.PageNumber, data.Paper, data.Cover, data.Year, data.ISBN, data.Bahasa, data.Synopsis, data.Stock)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}
	for _, code := range data.WriterCode {
		_, err = stmt3.Exec(data.Code, code)
		if err != nil {
			c.Flash.Error("Error saving data! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/new")
		}
	}
	_, err = stmt4.Exec(data.Code, data.Category, 0, data.Stock, data.Stock, data.ModifiedBy)
	c.Flash.Success("New book data is added")
	return c.Redirect("/dashboard/master/book")
}

func (c Master) EditBookHandler(id string, data models.Book, mainImage []byte, additionalImage1 []byte, additionalImage2 []byte) revel.Result {
	var err error
	oldValue := c.getBookFromCode(id)
	data.ModifiedBy = c.connected().Username
	data.WriterCode = c.Params.Form["WriterCode"]
	data.PublisherCode = c.Params.Form.Get("PublisherCode")
	data.Category = c.Params.Form.Get("ProductCategory")
	data.BookCategory = c.Params.Form.Get("BookCategory")
	data.Price = data.NormalPrice - (data.NormalPrice * data.Discount / 100)
	data.MainImage = oldValue.MainImage
	data.AdditionalImage1 = oldValue.AdditionalImage1
	data.AdditionalImage2 = oldValue.AdditionalImage2

	data.Cover = c.Params.Form.Get("Cover")

	timenow := time.Now().Format("060102150405")
	// Uploading file
	var filename string
	if len(mainImage) > 0 {
		filename := timenow + c.getBookImageName(c.Params.Files["mainImage"][0].Filename, data.WriterCode[0], data.Title, "0")
		err = uploadImage("product", filename, mainImage)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/%s/edit")
		}
		data.MainImage = "http://img.bukku.id/img/product/" + filename
	}
	if len(additionalImage1) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["additionalImage1"][0].Filename, data.WriterCode[0], data.Title, "0")
		err = uploadImage("product", filename, additionalImage1)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/%s/edit")
		}
		data.AdditionalImage1 = "http://img.bukku.id/img/product/" + filename
	} else {
		data.AdditionalImage1 = oldValue.AdditionalImage1
	}
	if len(additionalImage2) > 0 {
		filename = timenow + c.getBookImageName(c.Params.Files["additionalImage2"][0].Filename, data.WriterCode[0], data.Title, "0")
		err = uploadImage("product", filename, additionalImage2)
		if err != nil {
			c.Flash.Error("Upload error! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/%s/edit")
		}
		data.AdditionalImage2 = "http://img.bukku.id/img/product/" + filename
	} else {
		data.AdditionalImage2 = oldValue.AdditionalImage2
	}

	// Begin database transaction
	tx, err := Dbm.Begin()
	if err != nil {
		c.Flash.Error("Error preparing product database! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/%s/edit", id)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		tx.Commit()
	}()

	// Update into product
	stmt1, err := tx.Prepare(`
		UPDATE ms_product SET
			title = ? ,
			category = ? ,
			purchase_price = ? ,
			percentage_price = ? ,
			normal_price = ? ,
			sell_price = ? ,
			image1 = ? ,
			image2 = ? ,
			image3 = ? ,
			notes = ? ,
			status = ? ,
			modified_by = ? ,
			modified_date = now()
		WHERE code = ?
	`)
	if err != nil {
		c.Flash.Error("Error preparing product database! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/%s/edit", id)
	}

	// Insert into book
	stmt2, err := tx.Prepare(`
			UPDATE mst_book SET
				book_category = ?,
				publisher_code = ?,
				genre = ?,
				weight = ?,
				length = ?,
				total_page = ?,
				bahan_isi = ?,
				book_cover = ?,
				year = ?,
				bahasa = ?,
				ISBN = ?,
				synopsis = ?,
				current_stock = ?
			WHERE code = ?
		`)
	if err != nil {
		c.Flash.Error("Error preparing book database! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/%s/edit", id)
	}

	// Delete all writers relation
	stmt3, err := tx.Prepare(`
		DELETE FROM ms_bookwriter WHERE book_code = ?
	`)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}

	// Insert into book_writer
	stmt4, err := tx.Prepare(`
		INSERT INTO ms_bookwriter (
			book_code,
			writer_code
		)
		VALUES (?,?)
	`)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/new")
	}

	// Executing query
	_, err = stmt1.Exec(data.Title, data.Category, data.PurchasePrice, data.Discount, data.NormalPrice, data.Price,
		data.MainImage, data.AdditionalImage1, data.AdditionalImage2, data.Notes, data.Status, data.ModifiedBy, data.Code)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/%s/edit", id)
	}
	_, err = stmt2.Exec(data.BookCategory, data.PublisherCode, data.Genre, data.Weight, data.Length, data.PageNumber,
		data.Paper, data.Cover, data.Year, data.Bahasa, data.ISBN, data.Synopsis, data.Stock, data.Code)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/%s/edit", id)
	}
	_, err = stmt3.Exec(data.Code)
	if err != nil {
		c.Flash.Error("Error saving data! " + err.Error())
		c.FlashParams()
		return c.Redirect("/dashboard/master/book/%s/edit", id)
	}
	for _, writercode := range data.WriterCode {
		_, err = stmt4.Exec(data.Code, writercode)
		if err != nil {
			c.Flash.Error("Error saving data! " + err.Error())
			c.FlashParams()
			return c.Redirect("/dashboard/master/book/%s/edit", id)
		}
	}

	c.Flash.Success("Book data edited!")
	return c.Redirect("/dashboard/master/book")
}

func (c Master) getAllBook() []models.BookView {
	books := []models.BookView{}
	_, err := Dbm.Select(&books,
		`SELECT 
			ms_product.code AS code, 
			title, 
			sell_price, 
			image1,
			image2, 
			image3,
			weight,
			ms_product.status AS status, 
			mst_publisher.name AS publisher_name, 
			current_stock AS stock, 
			ms_product.is_active
		FROM 
			ms_product 
			JOIN mst_book USING (code) 
			LEFT JOIN mst_publisher 
				ON mst_book.publisher_code = mst_publisher.code`)
	if err != nil {
		panic(err)
	}

	for i, book := range books {
		books[i].PriceFormat = decimalToCurrency("Rp", book.Price)
		books[i].StatusColor = "success"
		books[i].StatusFormat = "Published"
		if book.Status == 0 {
			books[i].StatusColor = "warning"
			books[i].StatusFormat = "Draft"
		}
		if !book.MainImage.Valid {
			books[i].MainImage.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		if !book.AdditionalImage1.Valid {
			books[i].AdditionalImage1.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		if !book.AdditionalImage2.Valid {
			books[i].AdditionalImage2.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		// Get book writers
		writers := []string{}
		_, err := Dbm.Select(&writers,
			`SELECT 
				full_name 
			FROM 
				ms_bookwriter 
					LEFT JOIN 
						mst_writer 
					ON ms_bookwriter.writer_code = mst_writer.code 
			WHERE book_code = ?`, book.Code)
		if err != nil {
			c.Flash.Error("Error getting writer: ", err.Error())
		}

		books[i].WriterName = writers
	}

	return books
}

func getAllBook() []models.BookView {
	books := []models.BookView{}
	tx, err := Dbm.Begin()
	if err != nil {
		panic(err)
	}

	_, err = tx.Select(&books,
		`SELECT 
			ms_product.code AS code, 
			title, 
			sell_price, 
			image1,
			image2,
			image3, 
			weight,
			ms_product.status AS status, 
			mst_publisher.name AS publisher_name, 
			current_stock AS stock, 
			ms_product.is_active
		FROM 
			ms_product 
			JOIN mst_book USING (code) 
			LEFT JOIN mst_publisher 
				ON mst_book.publisher_code = mst_publisher.code`)
	if err != nil {
		panic(err)
	}

	for i, book := range books {
		books[i].PriceFormat = decimalToCurrency("Rp", book.Price)
		books[i].StatusColor = "success"
		books[i].StatusFormat = "Published"
		if book.Status == 0 {
			books[i].StatusColor = "warning"
			books[i].StatusFormat = "Draft"
		}
		if !book.MainImage.Valid {
			books[i].MainImage.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		if !book.AdditionalImage1.Valid {
			books[i].AdditionalImage1.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		if !book.AdditionalImage2.Valid {
			books[i].AdditionalImage2.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		// Get book writers
		writers := []string{}
		_, err := tx.Select(&writers,
			`SELECT 
				full_name 
			FROM 
				ms_bookwriter 
					LEFT JOIN 
						mst_writer 
					ON ms_bookwriter.writer_code = mst_writer.code 
			WHERE book_code = ?`, book.Code)
		if err != nil {
			panic(err)
		}

		books[i].WriterName = writers
	}

	return books
}

func (c Master) getBook() []models.BookView {
	books := []models.BookView{}
	_, err := Dbm.Select(&books,
		`SELECT 
			ms_product.code AS code, 
			title, 
			sell_price, 
			image1, 
			weight,
			ms_product.status AS status, 
			mst_publisher.name AS publisher_name, 
			current_stock AS stock, 
			ms_product.is_active
		FROM 
			ms_product 
			JOIN mst_book USING (code) 
			LEFT JOIN mst_publisher 
				ON mst_book.publisher_code = mst_publisher.code
		WHERE
			ms_product.is_active = 1
		`)
	if err != nil {
		panic(err)
	}

	for i, book := range books {
		books[i].PriceFormat = decimalToCurrency("Rp", book.Price)
		books[i].StatusColor = "success"
		books[i].StatusFormat = "Published"
		if book.Status == 0 {
			books[i].StatusColor = "warning"
			books[i].StatusFormat = "Draft"
		}
		if !book.MainImage.Valid {
			books[i].MainImage.String = "https://st.depositphotos.com/2239311/3456/v/450/depositphotos_34566475-stock-illustration-blank-book-template-with-soft.jpg"
		}
		// Get book writers
		writers := []string{}
		_, err := Dbm.Select(&writers,
			`SELECT 
				full_name 
			FROM 
				ms_bookwriter 
					LEFT JOIN 
						mst_writer 
					ON ms_bookwriter.writer_code = mst_writer.code 
			WHERE book_code = ?`, book.Code)
		if err != nil {
			c.Flash.Error("Error getting writer: ", err.Error())
		}

		books[i].WriterName = writers
	}

	return books
}

func (c Master) DeleteBook(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE ms_product
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 0
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/dashboard/master/book")
	}

	c.Flash.Success("Data is deleted")
	return c.Redirect("/dashboard/master/book")
}

func (c Master) ReactiveBook(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE ms_product
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactive data! " + err.Error())
		return c.Redirect("/dashboard/master/book")
	}

	c.Flash.Success("Data is actived")
	return c.Redirect("/dashboard/master/book")
}

func decimalToCurrency(currency string, nominal int64) string {
	var str string
	str = currency + " " + str

	strNominal := strconv.FormatInt(nominal, 10)
	strLength := len(strNominal)
	i := strLength - 3
	for i > 0 {
		strNominal = strNominal[:i] + "." + strNominal[i:]
		i = i - 3
	}
	str = str + strNominal

	return str
}

func (c Master) getBookFromCode(code string) *models.Book {
	result, err := Dbm.Select(models.Book{},
		`SELECT 
			code, title, category, book_category, purchase_price, percentage_price, normal_price, sell_price, image1, image2, image3, notes, status, publisher_code, genre, 
			weight, length, total_page, bahan_isi, year, ISBN, synopsis, bahasa, current_stock, modified_by, modified_date
	   	FROM ms_product JOIN mst_book USING(code) WHERE code = ?`, code)
	if err != nil {
		panic(err)
	}

	writerCode := []string{}
	_, err = Dbm.Select(&writerCode,
		`SELECT 
			writer_code
		FROM 
			ms_bookwriter 
		WHERE book_code = ?`, code)
	if err != nil {
		panic(err)
	}

	data := result[0].(*models.Book)
	data.WriterCode = writerCode

	return data
}

func (c Master) getBookCategory() []models.BookCategory {
	var categories []models.BookCategory
	_, err := Dbm.Select(&categories,
		`SELECT code, name FROM mst_category WHERE is_active = 1`)
	if err != nil {
		panic(err)
	}
	return categories
}
