package controllers

import (
	"database/sql"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

type Search struct {
	App
}

const DepokID = "115"

func (c Search) Product(code string) revel.Result {
	data, err := getProductByCode(code)
	if err == sql.ErrNoRows {
		return c.RenderJSON("not found")
	}
	if err != nil {
		panic(err)
	}

	return c.RenderJSON(data)
}

func (c Search) Customer(query string) revel.Result {
	// cleaning query string (sql injection prevention)
	if s := escapeString(query); s != query {
		return c.Redirect("/search/customer/%s", s)
	}

	data, err := getCustomerAddress(query)
	if err != nil {
		panic(err)
	}
	str, _ := json.Marshal(data)
	return c.RenderText(string(str))
}

func (c Search) Shipping(courier string) revel.Result {
	if courier != "jne" && courier != "jnt" && courier != "pos" {
		panic(errors.New("courier is wrong"))
	}
	destination := c.Params.Query.Get("destination")
	weight := c.Params.Query.Get("weight")
	weightInt, err := strconv.Atoi(weight)
	if err != nil {
		panic(err)
	}
	if weightInt < 1000 {
		weightInt = 1000
	}

	data, err := getShippingCost(courier, DepokID, destination, weightInt)
	if err != nil {
		panic(err)
	}

	return c.RenderJSON(data)
}

func (c Search) CityByProvince(provinceID int) revel.Result {
	result := []models.CityAPI{}
	for _, city := range Static.DataCity {
		if city.ProvinceID == provinceID {
			id := strconv.Itoa(city.ID)
			provID := strconv.Itoa(provinceID)
			result = append(result, models.CityAPI{id, provID, city.ProvinceName, city.Type, city.Name, city.PostalCode})
		}
	}
	return c.RenderJSON(result)
}

func escapeString(s string) string {
	s = strings.Replace(s, ";", "", -1)
	s = strings.Replace(s, "=", "", -1)
	s = strings.Replace(s, ")", "", -1)
	s = strings.Replace(s, "(", "", -1)
	s = strings.Replace(s, ":", "", -1)
	s = strings.Replace(s, "{", "", -1)
	s = strings.Replace(s, "}", "", -1)
	s = strings.Replace(s, "[", "", -1)
	s = strings.Replace(s, "]", "", -1)
	s = strings.Replace(s, "|", "", -1)
	s = strings.Replace(s, "<", "", -1)
	s = strings.Replace(s, ">", "", -1)
	s = strings.Replace(s, "?", "", -1)
	s = strings.Replace(s, "*", "", -1)
	return s
}

func getCustomerAddress(query string) ([]models.CustomerAddressAPI, error) {
	data := []models.CustomerAddressAPI{}

	// Begin database transaction
	tx, err := Dbm.Begin()
	if err != nil {
		return nil, err
	}

	q := `
		SELECT  
			mst_customer.id AS id, 
			full_name, 
			phone_1, 
			state_id, 
			province_id, 
			city_id, 
			kecamatan, 
			kelurahan, 
			street, 
			postal_code
		FROM 
			mst_customer LEFT JOIN 
			mst_address ON mst_customer.id = mst_address.customer_id
		WHERE full_name LIKE '%` + query + `%' AND mst_customer.is_active = 1
		LIMIT 25`

	if _, err := strconv.Atoi(query); err == nil {
		q = `
			SELECT 
				mst_customer.id AS id, 
				full_name, 
				phone_1, 
				state_id, 
				province_id, 
				city_id, 
				kecamatan, 
				kelurahan, 
				street, 
				postal_code
			FROM 
				mst_customer JOIN 
				mst_address ON mst_customer.id = mst_address.customer_id
			WHERE phone_1 LIKE '%` + query + `%' AND mst_customer.is_active = 1
			LIMIT 25`
	}

	// Get customer
	_, err = tx.Select(&data, q)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func getProductByCode(code string) (*models.ProductData, error) {
	data := models.ProductData{}

	// Begin database transaction
	tx, err := Dbm.Begin()
	if err != nil {
		return nil, err
	}

	err = tx.QueryRow(`
			SELECT 
				code,
				title,
				type,
				normal_price,
				image1,
				notes,
				status,
				modified_by,
				modified_date
			FROM ms_product 
			WHERE code = ? AND is_active = 1
		`, code).Scan(&data.Code, &data.Title, &data.Type, &data.NormalPrice, &data.MainImage, &data.Notes,
		&data.Status, &data.ModifiedBy, &data.ModifiedDate)
	if err != nil {
		return nil, err
	}

	return &data, nil
}
