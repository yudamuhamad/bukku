package controllers

import (
	"bukku.co.id/app/routes"
	"github.com/revel/revel"
)

// Hello : handler for POST request in login
func (c App) Hello(username string, password string) revel.Result {
	if user := c.connected(); user != nil {
		return c.Redirect(Dashboard.Index)
	}

	// Validation process
	c.Flash.Out["username"] = username
	c.Validation.Required(username).Message("Username is required!")
	c.Validation.Required(password).Message("Password is required!")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect("/")
	}

	user := c.getUser(username)
	if user == nil {
		c.Flash.Error("Login failed")
		return c.Redirect("/")
	}

	if user.Password != c.encryptString(password) {
		c.Flash.Error("Username/password is wrong")
		return c.Redirect("/")
	}

	//ip := strings.Split(c.Request.RemoteAddr, ":")[0]
	_, err := Dbm.Exec(`UPDATE mst_user 
		SET  
			last_login = now()
		WHERE username = ?`,
		username)

	if err != nil {
		return c.Redirect("/")
	}

	c.Session["username"] = user.Username
	return c.Redirect(routes.Dashboard.Index())
}
