package controllers

import (
	"bukku.co.id/app/models"
	"github.com/revel/revel"
)

// ==================================== Role GET Handler ====================================
func (c User) Role() revel.Result {
	data := c.getAllRole()
	c.ViewArgs["data"] = data

	return c.RenderTemplate("Dashboard/User/role.html")
}

func (c User) NewRole() revel.Result {
	return c.RenderTemplate("Dashboard/User/role_new.html")
}

func (c User) EditRole(id string) revel.Result {
	data := c.getRoleFromID(id)

	c.ViewArgs["r"] = data
	return c.RenderTemplate("Dashboard/User/role_edit.html")
}

// ==================================== Role POST Handler ====================================
func (c User) NewRoleHandler(data models.User) revel.Result {
	return c.Redirect("/role")
}

func (c User) EditRoleHandler(id string, u models.User) revel.Result {
	return c.Redirect("/role")
}

func (c User) DeleteRole(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_role
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 0
		WHERE role_code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error deleting data! " + err.Error())
		return c.Redirect("/user")
	}

	c.Flash.Success("Data is deleted")

	return c.Redirect("/role")
}

func (c User) ReactiveRole(id string) revel.Result {
	_, err := Dbm.Exec(`UPDATE mst_role
		SET 
			modified_by = ? ,
			modified_date = now() ,
			is_active = 1
		WHERE role_code = ?`, c.connected().Username, id)

	if err != nil {
		c.Flash.Error("Error reactiving data! " + err.Error())
		return c.Redirect("/user")
	}

	c.Flash.Success("Data is reactived")
	return c.Redirect("/role")
}

func (c User) getAllRole() []models.RoleView {
	roles := []models.RoleView{}
	_, err := Dbm.Select(&roles,
		`SELECT 
			role_code,
			role_name,
			modified_by,
			modified_date,
			is_active
		FROM 
			mst_role`)
	if err != nil {
		panic(err)
	}

	return roles
}

func (c User) getRole() []models.RoleView {
	roles := []models.RoleView{}
	_, err := Dbm.Select(&roles,
		`SELECT 
			role_code,
			role_name,
			modified_by,
			modified_date,
			is_active
		FROM 
			mst_role
		WHERE is_active = 1`)
	if err != nil {
		panic(err)
	}

	return roles
}

func (c User) getRoleFromID(id string) *models.RoleView {
	result, err := Dbm.Select(models.RoleView{},
		`SELECT 
			role_code,
			role_name,
			modified_by,
			modified_date,
			is_active
	   	FROM mst_role WHERE id = ?`, id)
	if err != nil {
		panic(err)
	}

	data := result[0].(*models.RoleView)
	return data
}
