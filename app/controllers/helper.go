package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	m "bukku.co.id/app/models"
)

type ShippingCostDetailAPI struct {
	Value int64  `json:"value"`
	Etd   string `json:"etd"`
	Note  string `json:"note"`
}

type ShippingCostAPI struct {
	Service     string                  `json:"service"`
	Description string                  `json:"description"`
	Cost        []ShippingCostDetailAPI `json:"cost"`
}

type ShippingAPI struct {
	Code  string            `json:"code"`
	Name  string            `json:"name"`
	Costs []ShippingCostAPI `json:"costs"`
}

type ShippingCost struct {
	Courier string
	Service string
	Cost    int64
}

const rajaOngkirKey = "3419a0a6d37d157a06888d0c907fe818"
const rajaOngkirEndpoint = "https://pro.rajaongkir.com/api/"

// Static save data to be access faster
var Static struct {
	DataState    []m.StateData
	DataProvince []m.Province
	DataCity     []m.City
}

// ==============================================================

func bulkInsertStaticState() {
	states := []m.StateData{
		{1, "Indonesia"},
		{2, "Malaysia"},
		{3, "Singapore"},
	}

	for _, state := range states {
		temp := &m.StateData{state.ID, state.Name}
		if res, err := Dbm.Get(m.StateData{}, state.ID); res == nil {
			if err != nil {
				panic(err)
			}
			err = Dbm.Insert(temp)
			if err != nil {
				panic(err)
			}
		}
	}

	Static.DataState = states
}

func getShippingCost(courier string, origin string, destination string, weight int) ([]ShippingCost, error) {
	client := &http.Client{}
	form := url.Values{
		"origin":          {origin},
		"destination":     {destination},
		"originType":      {"city"},
		"destinationType": {"city"},
		"courier":         {courier},
		"weight":          {strconv.Itoa(weight)}}
	req, err := http.NewRequest("POST", rajaOngkirEndpoint+"cost", strings.NewReader(form.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Add("key", rajaOngkirKey)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	result := struct {
		RajaOngkir struct {
			Results []ShippingAPI `json:"results"`
		} `json:"rajaongkir"`
	}{}

	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, err
	}

	ret := []ShippingCost{}
	for _, data := range result.RajaOngkir.Results {
		for _, x := range data.Costs {
			ret = append(ret, ShippingCost{courier, x.Service, x.Cost[0].Value})
		}
	}

	return ret, err
}

func getAllProvince() ([]m.Province, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", rajaOngkirEndpoint+"province", nil)
	if err != nil {
		return getAllProvinceFromDB()
	}

	req.Header.Add("key", rajaOngkirKey)
	resp, err := client.Do(req)
	if err != nil {
		return getAllProvinceFromDB()
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return getAllProvinceFromDB()
	}

	result := struct {
		RajaOngkir struct {
			Results []m.ProvinceAPI `json:"results"`
		} `json:"rajaongkir"`
	}{}
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return getAllProvinceFromDB()
	}

	provinces := []m.Province{}
	for _, data := range result.RajaOngkir.Results {
		id, err := strconv.Atoi(data.ID)
		if err != nil {
			panic(err)
		}
		provinces = append(provinces, m.Province{id, data.Name})
	}

	Static.DataProvince = provinces
	return provinces, nil
}

func getAllProvinceFromDB() ([]m.Province, error) {
	var result []m.ProvinceData
	_, err := Dbm.Select(&result, "SELECT * FROM ongkir_province ORDER BY id")
	if err != nil {
		return nil, err
	}

	province := []m.Province{}
	for _, data := range result {
		province = append(province, m.Province{data.ID, data.Name})
	}

	Static.DataProvince = province
	return province, nil
}

func getAllCity() ([]m.City, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", rajaOngkirEndpoint+"city", nil)
	if err != nil {
		return getAllCityFromDB()
	}

	req.Header.Add("key", rajaOngkirKey)
	resp, err := client.Do(req)
	if err != nil {
		return getAllCityFromDB()
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return getAllCityFromDB()
	}

	result := struct {
		RajaOngkir struct {
			Results []m.CityAPI `json:"results"`
		} `json:"rajaongkir"`
	}{}
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return getAllCityFromDB()
	}

	cities := []m.City{}
	for _, city := range result.RajaOngkir.Results {
		var id, provinceID int
		if id, err = strconv.Atoi(city.ID); err != nil {
			panic(err)
		}
		if provinceID, err = strconv.Atoi(city.ProvinceID); err != nil {
			panic(err)
		}
		cities = append(cities, m.City{id, provinceID, city.Type, city.Name, city.ProvinceName, city.PostalCode})
	}

	Static.DataCity = cities
	return cities, nil
}

func getAllCityFromDB() ([]m.City, error) {
	var result []m.City
	_, err := Dbm.Select(&result, `
		SELECT 
			ongkir_city.id as id,  
			ongkir_province.id as province_id, 
			type, 
			ongkir_city.name as name,
			ongkir_province.name as province_name, 
			postal_code 
		FROM ongkir_city JOIN ongkir_province 
			ON ongkir_city.province_id = ongkir_province.id
		ORDER BY ongkir_city.id`)
	if err != nil {
		return nil, err
	}

	Static.DataCity = result
	return result, nil
}

func getCityByProvince(provinceId int) ([]m.City, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", rajaOngkirEndpoint+"city", nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("key", rajaOngkirKey)
	provinceIdString := strconv.Itoa(provinceId)
	req.Header.Add("province", provinceIdString)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	result := struct {
		RajaOngkir struct {
			Results []m.City `json:"results"`
		} `json:"rajaongkir"`
	}{}
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, err
	}

	data := result.RajaOngkir.Results
	return data, nil
}

func bulkInsertProvince() {
	// Change Province to ProvinceData
	province, err := getAllProvince() //getAllProvinceFromDB()
	if err != nil {
		panic(err)
	}
	for _, data := range province {
		temp := &m.ProvinceData{data.ID, 1, data.Name}
		if res, err := Dbm.Get(m.ProvinceData{}, data.ID); res == nil {
			if err != nil {
				panic(err)
			}
			err = Dbm.Insert(temp)
			if err != nil {
				panic(err)
			}
		}
	}
}

func bulkInsertCity() {
	city, err := getAllCity() // getAllCityFromDB() //
	if err != nil {
		panic(err)
	}

	for _, data := range city {
		temp := &m.CityData{data.ID, data.ProvinceID, data.Type, data.Name, data.PostalCode}
		if res, err := Dbm.Get(m.CityData{}, data.ID); res == nil {
			if err != nil {
				panic(err)
			}
			err = Dbm.Insert(temp)
			if err != nil {
				panic(err)
			}
		}
	}

}
