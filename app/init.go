package app

import (
	"bukku.co.id/app/controllers"
	"github.com/revel/revel"
)

var (
	// AppVersion revel app version (ldflags)
	AppVersion string

	// BuildTime revel app build-time (ldflags)
	BuildTime string
)

func init() {
	// Filters is the default set of global filters.
	revel.Filters = []revel.Filter{
		revel.BeforeAfterFilter,
		revel.PanicFilter,             // Recover from panics and display an error page instead.
		revel.RouterFilter,            // Use the routing table to select the right Action
		revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
		revel.ParamsFilter,            // Parse parameters into Controller.Params.
		revel.SessionFilter,           // Restore and write the session cookie.
		revel.FlashFilter,             // Restore and write the flash cookie.
		revel.ValidationFilter,        // Restore kept validation errors and save new ones from cookie.
		revel.I18nFilter,              // Resolve the requested language
		HeaderFilter,                  // Add some security based headers
		revel.InterceptorFilter,       // Run interceptors around the action.
		revel.CompressFilter,          // Compress the result.
		revel.ActionInvoker,           // Invoke the action.
	}

	revel.TemplateFuncs["member_string"] = func(e string, s []string) bool {
		for _, a := range s {
			if a == e {
				return true
			}
		}
		return false
	}
	revel.TemplateFuncs["eq"] = func(a interface{}, b interface{}) bool {
		return a == b
	}
	revel.TemplateFuncs["neq"] = func(a interface{}, b interface{}) bool {
		return a != b
	}
	revel.TemplateFuncs["is_mul"] = func(a int, b int) bool {
		return b%a == 0
	}

	//revel.InterceptFunc(userAuth, revel.BEFORE, controllers.App{})
	//revel.InterceptFunc(doNothing, revel.AFTER, controllers.App{})
	revel.InterceptFunc(userAuth, revel.BEFORE, controllers.Dashboard{})
	revel.InterceptFunc(userAuth, revel.BEFORE, controllers.Master{})
	revel.InterceptFunc(userAuth, revel.BEFORE, controllers.Transaction{})

}

func doNothing(c *revel.Controller) revel.Result { return nil }

// HeaderFilter adds common security headers
// TODO turn this into revel.HeaderFilter
// should probably also have a filter for CSRF
// not sure if it can go in the same filter or not
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {
	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

// Intercept for page that need auth before user
// can access
func userAuth(c *revel.Controller) revel.Result {
	if _, ok := c.Session["username"]; ok {
		return nil
	}
	c.Flash.Error("Please log in first")

	return c.Redirect("/")
}
