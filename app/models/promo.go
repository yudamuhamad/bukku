package models

import (
	"database/sql"
	"time"
)

type PromoData struct {
	PromoCode     string          `db:"promo_code"`
	Title         string          `db:"title"`
	Type          string          `db:"type"`
	MinProduct    sql.NullInt64   `db:"min_product"`
	MinPembayaran sql.NullFloat64 `db:"min_purchase"`
	PromoValue    float64         `db:"promo_value"`
	PromoSatuan   string          `db:"promo_satuan"`
	MaxReduction  int64           `db:"max_reduction"`
	MaxCount      int64           `db:"max_count"`
	MaxBudget     float64         `db:"max_budget"`

	UsedCount     int64     `db:"used_count"`
	UsedBudget    float64   `db:"used_budget"`
	StartDateText string    `db:"-"`
	PeriodStart   time.Time `db:"start_date"`
	EndDateText   string    `db:"-"`
	PeriodEnd     time.Time `db:"end_date"`
	ModifiedBy    string    `db:"modified_by"`
	ModifiedDate  time.Time `db:"modified_date"`
	IsActive      int16     `db:"is_active"`
}
