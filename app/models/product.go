package models

import (
	"time"
)

type ProductData struct {
	Code         string    `db:"code, primarykey" json:"code"`
	Title        string    `db:"title" json:"title"`
	Type         string    `db:"type" json:"type"`
	NormalPrice  int64     `db:"normal_price" json:"price"`
	MainImage    string    `db:"image1" json:"image"`
	Notes        string    `db:"notes" json:"notes"`
	Status       bool      `db:"status" json:"status"`
	ModifiedBy   string    `db:"modified_by" json:"modified_by"`
	ModifiedDate time.Time `db:"modified_date" json:"modified_date"`
}

type ProductStock struct {
	Code  string `db:"code"`
	Type  string `db:"type"`
	Stock int    `db:"current_stock"`
}
