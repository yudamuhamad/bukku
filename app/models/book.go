package models

import (
	"database/sql"
	"time"
)

type BookInput struct {
	Code             string
	WriterCode       []string
	PublisherCode    string
	Title            string
	BookCategory     string
	Category         string
	Genre            string
	Discount         int64
	PurchasePrice    int64
	NormalPrice      int64
	Price            int64
	Weight           float32
	Length           string
	PageNumber       int
	Paper            string
	Cover            string
	MainImage        string
	AdditionalImage1 string
	AdditionalImage2 string
	Year             int
	ISBN             string
	Stock            int
	Bahasa           string
	Synopsis         string
	Notes            string
	Status           int
	ModifiedBy       string
	ModifiedDate     time.Time
	IsActive         bool
}

type Book struct {
	Code             string `db:"code"`
	WriterCode       []string
	PublisherCode    string    `db:"publisher_code"`
	Title            string    `db:"title"`
	BookCategory     string    `db:"book_category"`
	Category         string    `db:"category"`
	PurchasePrice    int64     `db:"purchase_price"`
	Discount         int64     `db:"percentage_price"`
	NormalPrice      int64     `db:"normal_price"`
	Price            int64     `db:"sell_price"`
	Genre            string    `db:"genre"`
	Weight           float32   `db:"weight"`
	Length           string    `db:"length"`
	PageNumber       int       `db:"total_page"`
	Paper            string    `db:"bahan_isi"`
	Cover            string    `db:"book_cover"`
	MainImage        string    `db:"image1"`
	AdditionalImage1 string    `db:"image2"`
	AdditionalImage2 string    `db:"image3"`
	Year             int       `db:"year"`
	ISBN             string    `db:"ISBN"`
	Stock            int       `db:"current_stock"`
	Bahasa           string    `db:"bahasa"`
	Synopsis         string    `db:"synopsis"`
	Notes            string    `db:"notes"`
	Status           bool      `db:"status"`
	ModifiedBy       string    `db:"modified_by"`
	ModifiedDate     time.Time `db:"modified_date"`
}

type BookCategory struct {
	Code string `db:"code"`
	Name string `db:"name"`
}

type BookView struct {
	Code             string         `db:"code"`
	MainImage        sql.NullString `db:"image1"`
	AdditionalImage1 sql.NullString `db:"image2"`
	AdditionalImage2 sql.NullString `db:"image3"`
	Title            string         `db:"title"`
	WriterName       []string
	PublisherName    string `db:"publisher_name"`
	Stock            int    `db:"stock"`
	Price            int64  `db:"sell_price"`
	Weight           int    `db:"weight"`
	PriceFormat      string
	Status           int `db:"status"`
	StatusColor      string
	StatusFormat     string
	IsActive         int `db:"is_active"`
}

type Bundle struct {
	Code             string `db:"code"`
	Title            string `db:"title"`
	BookCode         []string
	PurchasePrice    int64          `db:"purchase_price"`
	Discount         int64          `db:"percentage_price"`
	NormalPrice      int64          `db:"normal_price"`
	Price            int64          `db:"sell_price"`
	Notes            string         `db:"notes"`
	MainImage        string         `db:"image1"`
	AdditionalImage1 sql.NullString `db:"image2"`
	AdditionalImage2 sql.NullString `db:"image3"`
	Status           int16          `db:"status"`
}

type BundleView struct {
	Code         string
	Image        string
	Title        string
	BookTitles   []string
	Price        int64 `db:"sell_price"`
	Stock        int   `db:"current_stock"`
	Weight       int
	PriceFormat  string
	Status       int `db:"status"`
	StatusColor  string
	StatusFormat string
	IsActive     int `db:"is_active"`
}

type BundleInput struct {
	Code             string
	Title            string
	BookCode         []string
	Discount         int64
	PurchasePrice    int64
	NormalPrice      int64
	Price            int64
	Notes            string
	MainImage        string
	AdditionalImage1 string
	AdditionalImage2 string
	Status           string
	ModifiedBy       string
}
