package models

type Config struct {
	ID    int    `db:ID, primarykey, autoincrement`
	Enum  string `db:"ConfigEnum"`
	Value string `db:"ConfigValue"`
	Text  string `db:"ConfigText"`
}

type Bank struct {
	BankCode      int    `db:"bank_code"`
	BankName      string `db:"bank_name"`
	AccountNumber string `db:"account_number"`
	AccountName   string `db:"account_name"`
}
