package models

import (
	"time"
)

type TransactionData struct {
	ID                 int64                   `db:"order_id, primarykey, autoincrement"`
	UniqueCode         int64                   `db:"unique_code"`
	Date               time.Time               `db:"order_date"`
	CustomerID         int64                   `db:"customer_id"`
	Customer           CustomerData            `db:"-"`
	Channel            string                  `db:"channel"`
	TotalShoppingPrice int64                   `db:"shopping_price"`
	ShippingState      string                  `db:"state"`
	ShippingProvince   string                  `db:"province"`
	ShippingCity       string                  `db:"city"`
	ShippingKecamatan  string                  `db:"kecamatan"`
	ShippingKelurahan  string                  `db:"kelurahan"`
	ShippingStreet     string                  `db:"street"`
	ShippingPostalCode string                  `db:"postal_code"`
	CourierName        string                  `db:"courier_name"`
	CourierService     string                  `db:"courier_type"`
	CourierCost        int64                   `db:"courier_cost"`
	TotalWeight        int64                   `db:"-"`
	Resi               string                  `db:"courier_resi"`
	TotalPrice         int64                   `db:"total_price"`
	ConfirmationDate   time.Time               `db:"confirmation_date"`
	ConfirmationBank   string                  `db:"confirmation_bank"`
	ConfirmationImage  string                  `db:"confirmation_image"`
	CourierDate        string                  `db:"courier_date"`
	PromoCode          string                  `db:"promo_code"`
	ReductionPrice     int64                   `db:"reduction_price"`
	IsRefund           bool                    `db:"is_refund"`
	Status             int                     `db:"order_status"`
	Note               string                  `db:"note"`
	ModifiedBy         string                  `db:"modified_by"`
	ModifiedDate       time.Time               `db:"modified_date"`
	Detail             []TransactionDetailData `db:"-"`
}

type TransactionDetailData struct {
	OrderID     int64  `db:"order_id"`
	ProductCode string `db:"product_code"`
	Qty         int    `db:"product_order_qty"`
	SinglePrice int64  `db:"product_price"`
	TotalPrice  int64  `db:"product_total_price"`
}

type TransactionView struct {
	ID              int64     `db:"order_id"`
	UniqueCode      int64     `db:"unique_code"`
	Channel         string    `db:"channel"`
	CustomerID      int       `db:"customer_id"`
	CustomerPhone   string    `db:"phone_1"`
	CustomerName    string    `db:"full_name"`
	CustomerAddress string    `db:"-"`
	CourierName     string    `db:"courier_name"`
	CourierService  string    `db:"courier_type"`
	Date            time.Time `db:"order_date"`
	DateText        string    `db:"-"`
	ProductName     []string  `db:"-"`
	ProductCode     []string  `db:""`
	ProductQty      []int64   `db:""`
	ProductCategory string    `db:"-"`
	TotalPrice      int64     `db:"total_price"`
	TotalPriceText  string    `db:"-"`
	TotalProduct    int64     `db:"total_order_qty"`
	Resi            string    `db:"courier_resi"`
	Status          int       `db:"order_status"`
	Note            string    `db:"note"`
	StatusText      string    `db:"-"`
	StatusDesc      string    `db:"-"`
	StatusColor     string    `db:"-"`
	BankList        []Bank    `db:"-"`
}

type DashboarTransaction struct {
	ID          int64  `db:"order_id"`
	Publisher   string `db:"publisher"`
	Writer      string `db:"writer"`
	Title       string `db:"title"`
	Qty         int    `db:"qty"`
	Price       int64  `db:"price"`
	PriceFormat string
	Date        time.Time `db:"date"`
}
