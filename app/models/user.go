package models

import (
	"database/sql"
	"time"

	"github.com/revel/revel"
)

type User struct {
	ID           int       `db:"ID, primarykey, autoincrement"`
	Username     string    `db:"username"`
	Password     string    `db:"password"`
	FullName     string    `db:"full_name"`
	Role         string    `db:"role_code"`
	Branch       int64     `db:"branch_code"`
	URLProfile   string    `db:"url_profile"`
	URLCover     string    `db:"url_cover"`
	LastLogin    time.Time `db:"last_login"`
	IPAddress    string    `db:"ip_address"`
	ModifiedBy   string    `db:"modified_by"`
	ModifiedDate time.Time `db:"modified_date"`
	IsActive     int16     `db:"is_active"`
}

func (user User) Validate(v *revel.Validation) {
	v.Required(user.Username).Message("a Username is Requaried")
	v.MinSize(user.Username, 4).Message("The Username must be at least 4 characters long!")

	v.Required(user.Password).Message("a Password is Requaried")
	v.MinSize(user.Password, 6).Message("The Password must be at least 6 characters long!")

	v.Required(user.FullName).Message("a Full Name is Requaried")
	v.Required(user.Role)
}

type UserView struct {
	ID         int            `db:"ID, primarykey, autoincrement"`
	Username   string         `db:"username"`
	Password   string         `db:"password"`
	FullName   string         `db:"full_name"`
	RoleCode   string         `db:"role_code"`
	RoleName   string         `db:"role_name"`
	BranchName sql.NullString `db:"branch_name"`
	IsActive   int16          `db:"is_active"`
}

type RoleView struct {
	RoleCode     string    `db:"role_code"`
	RoleName     string    `db:"role_name"`
	ModifiedBy   string    `db:"modified_by"`
	ModifiedDate time.Time `db:"modified_date"`
	IsActive     int16     `db:"is_active"`
}
