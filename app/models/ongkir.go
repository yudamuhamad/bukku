package models

import (
	"time"
)

type AddressData struct {
	ID           int       `db:"id, primarykey, autoincrement"`
	CustomerID   int       `db:"customer_id"`
	Title        string    `db:"address_title"`
	StateID      int       `db:"state_id"`
	ProvinceID   int       `db:"province_id"`
	CityID       int       `db:"city_id"`
	Kecamatan    string    `db:"kecamatan"`
	Kelurahan    string    `db:"kelurahan"`
	Street       string    `db:"street"`
	PostalCode   string    `db:"postal_code"`
	ModifiedBy   string    `db:"modified_by"`
	ModifiedDate time.Time `db:"modified_date"`
	IsDefault    bool      `db:"is_default"`
	IsActive     bool      `db:"is_active"`
}

type Costs struct {
	origin      string
	destination string
	weight      int
	code        string
	service     string
	description string

	Cost *Cost
}

type Cost struct {
	value string
	etd   string
	note  string
}

type StateData struct {
	ID   int    `db:"id, primarykey"`
	Name string `db:"name"`
}

type Province struct {
	ID   int    `json:"province_id"`
	Name string `json:"province"`
}

type ProvinceAPI struct {
	ID   string `json:"province_id"`
	Name string `json:"province"`
}

type ProvinceData struct {
	ID      int    `db:"id, primarykey"`
	StateID int    `db:"state_id"`
	Name    string `db:"name"`
}

type City struct {
	ID           int    `db:"id, primarykey"`
	ProvinceID   int    `db:"province_id"`
	Type         string `db:"type"`
	Name         string `db:"name"`
	ProvinceName string `db:"province_name"`
	PostalCode   string `db:"postal_code"`
}

type CityAPI struct {
	ID           string `json:"city_id"`
	ProvinceID   string `json:"province_id"`
	ProvinceName string `json:"province"`
	Type         string `json:"type"`
	Name         string `json:"city_name"`
	PostalCode   string `json:"postal_code"`
}

type CityData struct {
	ID         int    `db:"id, primarykey"`
	ProvinceID int    `db:"province_id"`
	Type       string `db:"type"`
	Name       string `db:"name"`
	PostalCode string `db:"postal_code"`
}

type Kota struct {
	ID   int    `db:"id, primarykey"`
	Type string `db:"type"`
	Name string `db:"name"`
}
