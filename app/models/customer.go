package models

import (
	"database/sql"
	"time"
)

type Customer struct {
	ID         int    `db:"id" json:"id"`
	Name       string `db:"full_name" json:"name"`
	Sex        int    `db:"sex"`
	Phone      string `db:"phone_1" json:"phone"`
	State      string `db:"state" json:"state"`
	Province   string `db:"province" json:"province"`
	City       string `db:"city" json:"city"`
	Kecamatan  string `db:"kecamatan" json:"kecamatan"`
	Kelurahan  string `db:"kelurahan" json:"kelurahan"`
	Street     string `db:"street" json:"street"`
	PostalCode string `db:"postal_code" json:"postal_code"`
}

type CustomerAddressAPI struct {
	ID              int       `db:"id" json:"id"`
	Username        string    `db:"username"`
	Name            string    `db:"full_name" json:"name"`
	Phone           string    `db:"phone_1" json:"phone"`
	Sex             int       `db:"sex"`
	PhotoUrl        string    `db:"photo_url"`
	Email           string    `db:"email"`
	Facebook        string    `db:"facebook"`
	Twitter         string    `db:"twitter"`
	Instagram       string    `db:"instagram"`
	Google          string    `db:"google"`
	Line            string    `db:"line"`
	BirthDate       time.Time `db:"birth_date"`
	BirthPlace      string    `db:"birth_place"`
	CreatedDate     time.Time `db:"created_date"`
	StateID         int       `db:"state_id" json:"state"`
	ProvinceID      int       `db:"province_id" json:"province"`
	CityID          int       `db:"city_id" json:"city"`
	Kecamatan       string    `db:"kecamatan" json:"kecamatan"`
	Kelurahan       string    `db:"kelurahan" json:"kelurahan"`
	Street          string    `db:"street" json:"street"`
	PostalCode      string    `db:"postal_code" json:"postal_code"`
	FormatedAddress string    `db:"-" json",omitempty"`
}

type CustomerData struct {
	ID              int                 `db:"id, primarykey, autoincrement"`
	UID             string              `db:"uid`
	Username        string              `db:"username"`
	Name            string              `db:"full_name"`
	Sex             int                 `db:"sex"`
	PhotoUrl        string              `db:"photo_url"`
	Phone           string              `db:"phone_1"`
	AdditionalPhone sql.NullString      `db:"phone_2"`
	Email           string              `db:"email"`
	Address         *CustomerAddressAPI `db:"-"`
	Facebook        string              `db:"facebook"`
	Twitter         string              `db:"twitter"`
	Instagram       string              `db:"instagram"`
	Google          string              `db:"google"`
	Line            string              `db:"line"`
	BirthDate       time.Time           `db:"birth_date"`
	BirthPlace      string              `db:"birth_place"`
	CreatedDate     time.Time           `db:"created_date"`
	ModifiedBy      string              `db:"modified_by"`
	ModifiedDate    time.Time           `db:"modified_date"`
	IsActive        bool                `db:"is_active"`
	FormatedAddress string              `db:"-"`
}
