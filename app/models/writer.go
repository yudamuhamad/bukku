package models

import (
	"time"
)

//Penulis
type Writer struct {
	Code         string    `db:"code"`
	UserID       string    `db:"username"`
	Name         string    `db:"full_name"`
	Photo        string    `db:"profile_picture"`
	Phone1       string    `db:"phone_1"`
	Phone2       string    `db:"phone_2"`
	Email        string    `db:"email"`
	Facebook     string    `db:"facebook"`
	Twitter      string    `db:"twitter"`
	Instagram    string    `db:"instagram"`
	Line         string    `db:"line"`
	State        int       `db:"state_id"`
	Province     int       `db:"province_id"`
	City         int       `db:"city_id"`
	Kecamatan    string    `db:"kecamatan"`
	Kelurahan    string    `db:"kelurahan"`
	Street       string    `db:"street"`
	PostalCode   int64     `db:"postal_code"`
	ModifiedBy   string    `db:"modified_by"`
	ModifiedDate time.Time `db:"modified_date"`
	IsActive     int16     `db:"is_active"`
}

//Penerbit
type Publisher struct {
	Code         string    `db:"code"`
	Username     string    `db:"username"`
	Name         string    `db:"name"`
	OfficialType string    `db:"official_type"`
	Photo        string    `db:"profile_picture"`
	Phone1       string    `db:"phone_1"`
	Phone2       string    `db:"phone_2"`
	Email        string    `db:"email"`
	Facebook     string    `db:"facebook"`
	Twitter      string    `db:"twitter"`
	Instagram    string    `db:"instagram"`
	Line         string    `db:"line"`
	State        int       `db:"state_id"`
	Province     int       `db:"province_id"`
	City         int       `db:"city_id"`
	Kecamatan    string    `db:"kecamatan"`
	Kelurahan    string    `db:"kelurahan"`
	Street       string    `db:"street"`
	PostalCode   int64     `db:"postal_code"`
	ModifiedBy   string    `db:"modified_by"`
	ModifiedDate time.Time `db:"modified_date"`

	IsActive int16 `db:"is_active"`
}
