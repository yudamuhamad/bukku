-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bukku
CREATE DATABASE IF NOT EXISTS `bukku` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bukku`;

-- Dumping structure for table bukku.log_login
CREATE TABLE IF NOT EXISTS `log_login` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `login_date` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `operating_system` varchar(50) NOT NULL,
  `browser_name` varchar(50) NOT NULL,
  `browser_version` varchar(50) NOT NULL,
  `paltform` varchar(50) NOT NULL COMMENT 'mobile/desktop',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.log_login: ~0 rows (approximately)
DELETE FROM `log_login`;
/*!40000 ALTER TABLE `log_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_login` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_address
CREATE TABLE IF NOT EXISTS `mst_address` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `address_title` varchar(50) NOT NULL,
  `receiver_name` varchar(50) NOT NULL,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kelurahan` varchar(255) DEFAULT NULL,
  `street` varchar(255) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'hanya 1  yang default, selain itu value 0',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table bukku.mst_address: ~3 rows (approximately)
DELETE FROM `mst_address`;
/*!40000 ALTER TABLE `mst_address` DISABLE KEYS */;
INSERT INTO `mst_address` (`ID`, `customer_id`, `address_title`, `receiver_name`, `state_id`, `province_id`, `city_id`, `kecamatan`, `kelurahan`, `street`, `postal_code`, `modified_by`, `modified_date`, `is_default`, `is_active`) VALUES
	(1, 8, 'Main Address', '', 1, 9, 115, 'Mekarjaya', 'Sukmajaya', 'Jalan Sesama', 16400, 'admin', '2018-02-03 07:50:01', 1, 1),
	(2, 9, 'Metland Cileungsi', 'Regie Pahlewi', 1, 9, 22, 'Cileungsi', 'Cipenjo', 'Sektor 6 Blok F008/12 Ravenia', 16820, 'regie', '2018-02-04 14:04:59', 1, 1),
	(3, 9, 'Metland Cileungsi', 'Regie Pahlewi', 1, 9, 22, 'Cileungsi', 'Cipenjo', 'Sektor 6 Blok F008/12 Ravenia', 16820, 'regie', '2018-02-09 13:24:43', 0, 1),
	(4, 10, 'Main Address', '', 1, 12, 365, '', '', 'Hai', 76612, 'admin', '2018-02-16 02:48:53', 1, 1);
/*!40000 ALTER TABLE `mst_address` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_bank
CREATE TABLE IF NOT EXISTS `mst_bank` (
  `bank_code` int(3) unsigned zerofill NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `account_branch` varchar(50) NOT NULL,
  PRIMARY KEY (`bank_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_bank: ~8 rows (approximately)
DELETE FROM `mst_bank`;
/*!40000 ALTER TABLE `mst_bank` DISABLE KEYS */;
INSERT INTO `mst_bank` (`bank_code`, `bank_name`, `account_number`, `account_name`, `account_branch`) VALUES
	(002, 'BANK BRI', '580801007728509', 'Agusta Reivo KS', 'Depok'),
	(008, 'BANK MANDIRI', '1140002708926', 'Agusta Reivo KS', 'Depok'),
	(009, 'BNI', '0194092035', 'Maulana Yusuf', 'Bandung'),
	(011, 'BANK DANAMON', '', '', ''),
	(013, 'PERMATA BANK', '', '', ''),
	(014, 'BANK BCA', '6610719790', 'Agusta Reivo KS', 'Depok'),
	(019, 'BANK PANIN', '', '', ''),
	(110, 'BANK BJB', '', '', '');
/*!40000 ALTER TABLE `mst_bank` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_book
CREATE TABLE IF NOT EXISTS `mst_book` (
  `code` varchar(50) NOT NULL,
  `book_category` varchar(10) NOT NULL,
  `publisher_code` varchar(50) NOT NULL,
  `genre` varchar(50) DEFAULT NULL,
  `weight` double NOT NULL,
  `length` varchar(50) DEFAULT NULL,
  `total_page` int(11) DEFAULT NULL,
  `bahan_isi` varchar(50) DEFAULT NULL,
  `book_cover` enum('Soft Cover','Hard Cover','') DEFAULT NULL,
  `year` year(4) DEFAULT '2018' COMMENT 'tahun terbit',
  `ISBN` varchar(50) DEFAULT NULL,
  `bahasa` varchar(50) DEFAULT NULL,
  `synopsis` varchar(255) DEFAULT NULL,
  `current_stock` int(11) NOT NULL DEFAULT '0',
  `status_stock` int(11) NOT NULL COMMENT 'mst_config = StatusStock',
  PRIMARY KEY (`code`),
  KEY `publisher_code` (`publisher_code`),
  KEY `book_category` (`book_category`),
  CONSTRAINT `book_fk` FOREIGN KEY (`code`) REFERENCES `ms_product` (`code`) ON UPDATE CASCADE,
  CONSTRAINT `book_fk_category` FOREIGN KEY (`book_category`) REFERENCES `mst_category` (`code`) ON UPDATE CASCADE,
  CONSTRAINT `book_fk_publiisher` FOREIGN KEY (`publisher_code`) REFERENCES `mst_publisher` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_book: ~5 rows (approximately)
DELETE FROM `mst_book`;
/*!40000 ALTER TABLE `mst_book` DISABLE KEYS */;
INSERT INTO `mst_book` (`code`, `book_category`, `publisher_code`, `genre`, `weight`, `length`, `total_page`, `bahan_isi`, `book_cover`, `year`, `ISBN`, `bahasa`, `synopsis`, `current_stock`, `status_stock`) VALUES
	('DST001', 'COM', 'HKR', 'Novel', 600, '13 x 19 cm', 440, 'Bahan Isi', 'Soft Cover', '2018', '914523444', 'Indonesia', '', 88, 0),
	('MT16007', 'NOV', 'MDK', 'Novel', 340, '13 x 19 cm', 188, '', 'Hard Cover', '2017', '914523451', 'Indonesia', '', 94, 0),
	('MT16008', 'FIK', 'MDK', 'Novel,Drama', 200, '13 x 19 cm', 180, 'Bahan Isi', 'Soft Cover', '2017', '914523423', 'Indonesia', '', 200, 0),
	('MT16009', 'NOV', 'MDK', 'Novel', 200, '13 x 19 cm', 188, 'Bahan Isi', 'Soft Cover', '2017', '914523423', 'Indonesia', '', 199, 0),
	('MT17021', 'NOV', 'MDK', 'Novel', 311, '13 x 19 cm', 200, 'Bahan Isi', 'Soft Cover', '2017', '914523444', 'Indonesia', '', 99, 0);
/*!40000 ALTER TABLE `mst_book` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_bundle
CREATE TABLE IF NOT EXISTS `mst_bundle` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `book_code` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `book_code` (`book_code`),
  KEY `code` (`code`),
  CONSTRAINT `bundle_fk_book` FOREIGN KEY (`book_code`) REFERENCES `mst_book` (`code`) ON UPDATE CASCADE,
  CONSTRAINT `bundle_fk_product` FOREIGN KEY (`code`) REFERENCES `ms_product` (`code`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_bundle: ~3 rows (approximately)
DELETE FROM `mst_bundle`;
/*!40000 ALTER TABLE `mst_bundle` DISABLE KEYS */;
INSERT INTO `mst_bundle` (`ID`, `code`, `book_code`) VALUES
	(1, 'BFIERSA', 'MT16008'),
	(2, 'BFIERSA', 'MT16009'),
	(3, 'BFIERSA', 'MT17021');
/*!40000 ALTER TABLE `mst_bundle` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_category
CREATE TABLE IF NOT EXISTS `mst_category` (
  `code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_category: ~5 rows (approximately)
DELETE FROM `mst_category`;
/*!40000 ALTER TABLE `mst_category` DISABLE KEYS */;
INSERT INTO `mst_category` (`code`, `name`, `modified_by`, `modified_date`, `is_active`) VALUES
	('ANK', 'Buku Anak', 'admin', '2018-01-22 10:52:59', 1),
	('COM', 'Buku Komik', 'admin', '2018-01-22 10:52:59', 1),
	('DWS', 'Buku Dewasa', 'admin', '2018-01-22 10:52:59', 1),
	('FIK', 'Buku Fiksi', 'admin', '2018-01-17 00:00:00', 1),
	('NOV', 'Buku Novel', 'admin', '2018-01-22 10:52:59', 1);
/*!40000 ALTER TABLE `mst_category` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_config
CREATE TABLE IF NOT EXISTS `mst_config` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ConfigEnum` varchar(255) NOT NULL,
  `ConfigValue` varchar(255) NOT NULL,
  `ConfigText` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_config: ~32 rows (approximately)
DELETE FROM `mst_config`;
/*!40000 ALTER TABLE `mst_config` DISABLE KEYS */;
INSERT INTO `mst_config` (`ID`, `ConfigEnum`, `ConfigValue`, `ConfigText`) VALUES
	(1, 'Month', '1', 'Januari'),
	(2, 'Month', '2', 'Februari'),
	(3, 'Month', '3', 'Maret'),
	(4, 'Month', '4', 'April'),
	(5, 'Month', '5', 'Mei'),
	(6, 'Month', '6', 'Juni'),
	(7, 'Month', '7', 'Juli'),
	(8, 'Month', '8', 'Agustus'),
	(9, 'Month', '9', 'September'),
	(10, 'Month', '10', 'Oktober'),
	(11, 'Month', '11', 'November'),
	(12, 'Month', '12', 'Desember'),
	(13, 'ProductCategory', 'Regular', 'Regular'),
	(14, 'ProductCategory', 'Pre Order', 'Pre Order'),
	(15, 'ProductCategory', 'Flash Sale', 'Flash Sale'),
	(16, 'PromoType', 'Kupon', 'Kupon'),
	(17, 'PromoType', 'Diskon', 'Diskon'),
	(18, 'BookCover', 'Soft Cover', 'Soft Cover'),
	(19, 'BookCover', 'Hard Cover', 'Hard Cover'),
	(20, 'StatusStock', '1', 'Stok Tersedia'),
	(21, 'StatusStock', '2', 'Stok Terbatas'),
	(22, 'StatusStock', '3', 'Produk ini dikirim dari distributor'),
	(23, 'ChannelType', 'WA', 'WhatsApp'),
	(24, 'ChannelType', 'LINE', 'LINE'),
	(25, 'ChannelType', 'WEB', 'bukku.id'),
	(26, 'ContentType', 'HIGHLIGHT', 'Slider for Highlight'),
	(27, 'ContentType', 'WOTM', 'Writer of The Month'),
	(28, 'ContentType', 'BOTM', 'Book of The Month'),
	(29, 'ContentType', 'PROMO1', 'Headline Promo'),
	(30, 'ContentType', 'PROMO1', 'Headline Promo'),
	(31, 'OfficialType', 'Regular', 'Regular'),
	(32, 'OfficialType', 'Official', 'Official');
/*!40000 ALTER TABLE `mst_config` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_customer
CREATE TABLE IF NOT EXISTS `mst_customer` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `sex` tinyint(4) NOT NULL COMMENT '1 Laki-laki, 0 Wanita',
  `phone_1` varchar(50) NOT NULL,
  `phone_2` varchar(50) DEFAULT NULL,
  `photo_url` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `google` varchar(100) NOT NULL,
  `line` varchar(100) NOT NULL COMMENT 'id Line chat',
  `birth_date` datetime NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table bukku.mst_customer: ~2 rows (approximately)
DELETE FROM `mst_customer`;
/*!40000 ALTER TABLE `mst_customer` DISABLE KEYS */;
INSERT INTO `mst_customer` (`ID`, `uid`, `username`, `full_name`, `sex`, `phone_1`, `phone_2`, `photo_url`, `email`, `facebook`, `twitter`, `instagram`, `google`, `line`, `birth_date`, `birth_place`, `modified_by`, `modified_date`, `is_active`) VALUES
	(8, '', '', 'Fizal Aselole', 0, '+628214523515', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', 'admin', '2018-02-03 07:50:01', 1),
	(9, 'vvvBa85wmmU4V8c5nhoQMXMurDT2', '', 'Regie Pahlewi', 0, '+628561231232', '', 'https://lh4.googleusercontent.com/-Zp_SGk3JCEs/AAAAAAAAAAI/AAAAAAAAFPM/5goz5G_cByg/photo.jpg', 'regie@bambunesia.com', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', 1),
	(10, '', '', 'Uchup', 0, '+6282211357352', NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', 'admin', '2018-02-16 02:48:53', 1);
/*!40000 ALTER TABLE `mst_customer` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_menu
CREATE TABLE IF NOT EXISTS `mst_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_menu: ~0 rows (approximately)
DELETE FROM `mst_menu`;
/*!40000 ALTER TABLE `mst_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_menu` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_menu_parent
CREATE TABLE IF NOT EXISTS `mst_menu_parent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) DEFAULT NULL COMMENT 'null = parent menu',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_menu_parent: ~0 rows (approximately)
DELETE FROM `mst_menu_parent`;
/*!40000 ALTER TABLE `mst_menu_parent` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_menu_parent` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_promo
CREATE TABLE IF NOT EXISTS `mst_promo` (
  `promo_code` varchar(50) NOT NULL COMMENT 'code / kupon code',
  `title` varchar(255) DEFAULT NULL,
  `type` enum('Diskon','Kupon','') NOT NULL,
  `min_product` int(11) DEFAULT NULL,
  `min_purchase` decimal(10,0) DEFAULT NULL,
  `promo_satuan` varchar(11) NOT NULL,
  `promo_value` decimal(10,0) NOT NULL,
  `max_reduction` int(11) DEFAULT NULL,
  `max_count` int(11) DEFAULT NULL,
  `used_count` int(11) NOT NULL,
  `max_budget` decimal(10,0) DEFAULT NULL,
  `used_budget` decimal(10,0) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`promo_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_promo: ~2 rows (approximately)
DELETE FROM `mst_promo`;
/*!40000 ALTER TABLE `mst_promo` DISABLE KEYS */;
INSERT INTO `mst_promo` (`promo_code`, `title`, `type`, `min_product`, `min_purchase`, `promo_satuan`, `promo_value`, `max_reduction`, `max_count`, `used_count`, `max_budget`, `used_budget`, `start_date`, `end_date`, `modified_by`, `modified_date`, `is_active`) VALUES
	('ENDYEAR', 'Promo Akhir Tahun', 'Kupon', 1, NULL, '%', 10, 10000, 100, 0, 1000000, 0, '0000-00-00 00:00:00', '2018-12-31 00:00:00', 'admin', '2018-01-01 00:00:00', 1),
	('MIDYEAR', 'Promo Tengah Tahun', 'Kupon', 1, NULL, '%', 10, 10000, 100, 0, 1000000, 0, '0000-00-00 00:00:00', '2018-07-01 00:00:00', 'admin', '2018-01-01 00:00:00', 1);
/*!40000 ALTER TABLE `mst_promo` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_publisher
CREATE TABLE IF NOT EXISTS `mst_publisher` (
  `code` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `official_type` varchar(50) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `phone_1` varchar(50) NOT NULL,
  `phone_2` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `line` varchar(50) NOT NULL COMMENT 'id Line chat',
  `state_id` int(11) NOT NULL DEFAULT '0',
  `province_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `kecamatan` varchar(255) NOT NULL,
  `kelurahan` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table bukku.mst_publisher: ~13 rows (approximately)
DELETE FROM `mst_publisher`;
/*!40000 ALTER TABLE `mst_publisher` DISABLE KEYS */;
INSERT INTO `mst_publisher` (`code`, `username`, `name`, `official_type`, `profile_picture`, `phone_1`, `phone_2`, `email`, `facebook`, `twitter`, `instagram`, `line`, `state_id`, `province_id`, `city_id`, `kecamatan`, `kelurahan`, `street`, `postal_code`, `modified_by`, `modified_date`, `is_active`) VALUES
	('AKPS', '', 'Aksara Plus', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:19:38', 1),
	('BTPTK', '', 'Bentang Pustaka', 'Regular', '', '', '', '', '', '', '', '', 0, 5, 501, '', '', '', 0, 'admin', '2018-01-31 08:27:25', 1),
	('CCNTBK', '', 'Coconut Books', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:21:14', 1),
	('GGSMD', '', 'Gagas Media', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:28:11', 1),
	('HKR', '', 'Hikaru Publishing', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 0, '', '', '', 0, 'admin', '2018-01-31 08:24:54', 1),
	('HRU', '', 'Haru', 'Regular', '', '', '', '', '', '', '', '', 0, 3, 456, '', '', '', 0, 'admin', '2018-01-31 08:29:41', 1),
	('LD01', '', 'Lampu Djalan', 'Regular', '', '', '', '', '', '', '', '', 0, 9, 115, 'Sukmajaya', 'Mekarjaya', 'Merdeka Barat BM No.23', 141611, 'admin', '2018-01-31 08:08:46', 1),
	('LVBL', '', 'Loveable', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:25:43', 1),
	('MDK', 'media.kita', 'Media Kita', 'Official', ' ', '', '', '', ' ', ' ', ' ', ' ', 0, 6, 153, ' ', ' ', ' ', 10000, 'admin', '2018-02-10 11:16:30', 1),
	('MZN', '', 'Mizan', 'Regular', '', '', '', '', '', '', '', '', 0, 9, 23, '', '', '', 0, 'admin', '2018-01-31 08:26:25', 1),
	('NMNBK', '', 'Namina Books', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:20:27', 1),
	('RDM', '', 'RDM Publishers', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:23:02', 1),
	('RMCS', '', 'Romancious', 'Regular', '', '', '', '', '', '', '', '', 0, 6, 153, '', '', '', 0, 'admin', '2018-01-31 08:18:25', 1);
/*!40000 ALTER TABLE `mst_publisher` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_role
CREATE TABLE IF NOT EXISTS `mst_role` (
  `role_code` varchar(50) NOT NULL DEFAULT '0',
  `role_name` varchar(50) DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_role: ~3 rows (approximately)
DELETE FROM `mst_role`;
/*!40000 ALTER TABLE `mst_role` DISABLE KEYS */;
INSERT INTO `mst_role` (`role_code`, `role_name`, `modified_by`, `modified_date`, `is_active`) VALUES
	('ADM', 'Administrator', 'admin', '2018-01-22 09:51:05', 1),
	('PUB', 'Publisher', 'admin', '2018-01-19 00:00:00', 1),
	('WRT', 'Writer', 'admin', '2018-01-19 00:00:00', 1);
/*!40000 ALTER TABLE `mst_role` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_user
CREATE TABLE IF NOT EXISTS `mst_user` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1' COMMENT '1 Aktif, 0 Tidak Aktif',
  PRIMARY KEY (`ID`),
  KEY `role_code` (`role_code`),
  CONSTRAINT `user_fk_role` FOREIGN KEY (`role_code`) REFERENCES `mst_role` (`role_code`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_user: ~0 rows (approximately)
DELETE FROM `mst_user`;
/*!40000 ALTER TABLE `mst_user` DISABLE KEYS */;
INSERT INTO `mst_user` (`ID`, `username`, `password`, `full_name`, `role_code`, `modified_by`, `modified_date`, `is_active`) VALUES
	(1, 'admin', 'admin123', 'Administrator', 'ADM', 'SYSTEM', '2018-01-27 11:32:10', 1);
/*!40000 ALTER TABLE `mst_user` ENABLE KEYS */;

-- Dumping structure for table bukku.mst_writer
CREATE TABLE IF NOT EXISTS `mst_writer` (
  `code` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `phone_1` varchar(50) NOT NULL,
  `phone_2` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `line` varchar(50) NOT NULL COMMENT 'id Line chat',
  `state_id` int(255) NOT NULL DEFAULT '0',
  `province_id` int(255) NOT NULL,
  `city_id` int(255) NOT NULL,
  `kecamatan` varchar(255) NOT NULL,
  `kelurahan` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.mst_writer: ~12 rows (approximately)
DELETE FROM `mst_writer`;
/*!40000 ALTER TABLE `mst_writer` DISABLE KEYS */;
INSERT INTO `mst_writer` (`code`, `username`, `full_name`, `profile_picture`, `phone_1`, `phone_2`, `email`, `facebook`, `twitter`, `instagram`, `line`, `state_id`, `province_id`, `city_id`, `kecamatan`, `kelurahan`, `street`, `postal_code`, `modified_by`, `modified_date`, `is_active`) VALUES
	('ARIQY', '', 'Ariqy Raihan', '', '', '', '', '', '', 'ariqyraihan', '', 0, 6, 0, '', '', '', 0, 'admin', '2018-01-31 09:44:18', 1),
	('AULIA', '', 'K. Aulia R.', '', '', '', '', '', '', 'k.aulia.r', '', 0, 1, 0, '', '', '', 0, 'admin', '2018-01-31 09:46:47', 1),
	('AZN', '', 'Azhar Nurun Ala', '', '', '', '', '', '', '', '', 0, 9, 115, '', '', '', 0, 'admin', '2018-01-31 08:31:07', 1),
	('BOY', 'boy.chandra', 'Boy Chandra', ' ', '', '', ' ', ' ', ' ', ' ', ' ', 1, 1, 1, ' ', ' ', ' ', 1000, 'admin', '2018-01-27 11:32:10', 1),
	('BYPMN', '', 'Bayu Permana', '', '', '', '', '', '', 'bayupermana31_', '', 0, 1, 0, '', '', '', 0, 'admin', '2018-01-31 09:50:42', 1),
	('FALF', '', 'Falafu', '', '', '', '', '', '', '', '', 0, 1, 0, '', '', '', 0, 'admin', '2018-01-31 09:40:40', 1),
	('FRSB', '', 'Fiersa Besari', '', '', '', '', 'Fiersa Besari', '', 'fiersabesari', '', 0, 9, 23, '', '', '', 0, 'admin', '2018-01-31 09:38:20', 1),
	('HIMSA', '', 'Ahimsa Azaleav', '', '', '', '', '', '', 'ahimsa.azaleav', '', 0, 34, 0, '', '', '', 0, 'admin', '2018-01-31 09:43:04', 1),
	('IKROM', '', 'Ikrom Mustofa', '', '', '', '', '', '', 'ikrommustofa', '', 0, 1, 0, '', '', '', 0, 'admin', '2018-01-31 09:45:20', 1),
	('RDTYA', '', 'Raditya Dika', '', '', '', '', '', '', 'raditya_dika', '', 0, 1, 0, '', '', '', 0, 'admin', '2018-01-31 09:47:56', 1),
	('TSWT', '', 'Tia Setiawati', '', '', '', '', '', '', 'tiasetiawati2709', '', 0, 1, 0, '', '', '', 0, 'admin', '2018-01-31 09:49:01', 1),
	('WRNG', '', 'Wira Nagara', '', '', '', '', '', '', 'wira_', '', 0, 6, 0, '', '', '', 0, 'admin', '2018-01-31 09:39:39', 1);
/*!40000 ALTER TABLE `mst_writer` ENABLE KEYS */;

-- Dumping structure for table bukku.ms_bookwriter
CREATE TABLE IF NOT EXISTS `ms_bookwriter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_code` varchar(255) NOT NULL,
  `writer_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_code` (`book_code`),
  KEY `writer_code` (`writer_code`),
  CONSTRAINT `bookwriter_fk_book` FOREIGN KEY (`book_code`) REFERENCES `mst_book` (`code`) ON UPDATE CASCADE,
  CONSTRAINT `bookwriter_fk_writer` FOREIGN KEY (`writer_code`) REFERENCES `mst_writer` (`code`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.ms_bookwriter: ~5 rows (approximately)
DELETE FROM `ms_bookwriter`;
/*!40000 ALTER TABLE `ms_bookwriter` DISABLE KEYS */;
INSERT INTO `ms_bookwriter` (`id`, `book_code`, `writer_code`) VALUES
	(6, 'MT16008', 'FRSB'),
	(7, 'MT16009', 'FRSB'),
	(8, 'MT17021', 'FRSB'),
	(10, 'MT16007', 'FRSB'),
	(13, 'DST001', 'TSWT');
/*!40000 ALTER TABLE `ms_bookwriter` ENABLE KEYS */;

-- Dumping structure for table bukku.ms_menu_access
CREATE TABLE IF NOT EXISTS `ms_menu_access` (
  `role_code` varchar(50) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.ms_menu_access: ~0 rows (approximately)
DELETE FROM `ms_menu_access`;
/*!40000 ALTER TABLE `ms_menu_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_menu_access` ENABLE KEYS */;

-- Dumping structure for table bukku.ms_product
CREATE TABLE IF NOT EXISTS `ms_product` (
  `code` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` enum('book','bundle','') NOT NULL DEFAULT 'book' COMMENT '{"book","bundle"}',
  `category` varchar(50) NOT NULL DEFAULT 'Regular' COMMENT 'PO, POTTD, REG, FLASH, PROMO, PROBONUS',
  `purchase_price` decimal(10,0) NOT NULL COMMENT 'harga beli dari penerbit',
  `percentage_price` decimal(10,0) NOT NULL,
  `normal_price` decimal(10,0) NOT NULL COMMENT 'harga jual normal ke customer',
  `sell_price` decimal(10,0) NOT NULL,
  `image1` varchar(255) NOT NULL COMMENT 'path image',
  `image2` varchar(255) DEFAULT NULL COMMENT 'path image',
  `image3` varchar(255) DEFAULT NULL COMMENT 'path image',
  `image4` varchar(255) DEFAULT NULL COMMENT 'path image',
  `notes` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=published; 0=draft',
  `modified_by` varchar(255) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0-deleted; 1=active',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.ms_product: ~6 rows (approximately)
DELETE FROM `ms_product`;
/*!40000 ALTER TABLE `ms_product` DISABLE KEYS */;
INSERT INTO `ms_product` (`code`, `title`, `type`, `category`, `purchase_price`, `percentage_price`, `normal_price`, `sell_price`, `image1`, `image2`, `image3`, `image4`, `notes`, `status`, `modified_by`, `modified_date`, `is_active`) VALUES
	('BFIERSA', 'Bundle Fiersa Besari', 'bundle', 'Regular', 0, 0, 200000, 200000, '', '', '', NULL, '', 0, 'admin', '2018-02-03 11:45:29', 1),
	('DST001', 'Destin', 'book', 'Pre Order', 80000, 10, 92000, 82800, '', '', '', NULL, '', 0, 'admin', '2018-02-10 10:24:30', 1),
	('MT16007', 'Setelah Hujan Reda', 'book', '', 35000, 0, 45000, 45000, '', '', '', NULL, '', 1, 'admin', '2018-02-10 03:34:03', 1),
	('MT16008', 'Konspirasi Alam Semesta', 'book', 'Regular', 45000, 10, 60000, 54000, '', '', '', NULL, '', 0, 'admin', '2018-02-03 11:43:38', 1),
	('MT16009', 'Garis Waktu', 'book', 'Regular', 45000, 0, 60000, 60000, '', '', '', NULL, '', 0, 'admin', '2018-02-03 11:44:36', 1),
	('MT17021', 'Catatan Juang', 'book', 'Regular', 45000, 0, 55000, 55000, '', '', '', NULL, '', 0, 'admin', '2018-02-10 10:26:55', 1);
/*!40000 ALTER TABLE `ms_product` ENABLE KEYS */;

-- Dumping structure for table bukku.ms_promoproduct
CREATE TABLE IF NOT EXISTS `ms_promoproduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(50) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `customer_id` varchar(50) NOT NULL COMMENT 'value = All / customer_id (customer pilihan)',
  PRIMARY KEY (`id`),
  KEY `promo_code` (`promo_code`),
  CONSTRAINT `promo_fk_promo` FOREIGN KEY (`promo_code`) REFERENCES `mst_promo` (`promo_code`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.ms_promoproduct: ~2 rows (approximately)
DELETE FROM `ms_promoproduct`;
/*!40000 ALTER TABLE `ms_promoproduct` DISABLE KEYS */;
INSERT INTO `ms_promoproduct` (`id`, `promo_code`, `product_code`, `customer_id`) VALUES
	(1, 'MIDYEAR', '', ''),
	(2, 'ENDYEAR', '', '');
/*!40000 ALTER TABLE `ms_promoproduct` ENABLE KEYS */;

-- Dumping structure for table bukku.ms_wishlist
CREATE TABLE IF NOT EXISTS `ms_wishlist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `product_code` (`product_code`),
  CONSTRAINT `wishlist_fk_cust` FOREIGN KEY (`customer_id`) REFERENCES `mst_customer` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `wishlist_fk_product` FOREIGN KEY (`product_code`) REFERENCES `ms_product` (`code`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.ms_wishlist: ~0 rows (approximately)
DELETE FROM `ms_wishlist`;
/*!40000 ALTER TABLE `ms_wishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_wishlist` ENABLE KEYS */;

-- Dumping structure for table bukku.ongkir_city
CREATE TABLE IF NOT EXISTS `ongkir_city` (
  `id` int(11) NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bukku.ongkir_city: ~501 rows (approximately)
DELETE FROM `ongkir_city`;
/*!40000 ALTER TABLE `ongkir_city` DISABLE KEYS */;
INSERT INTO `ongkir_city` (`id`, `province_id`, `type`, `name`, `postal_code`) VALUES
	(1, 21, 'Kabupaten', 'Aceh Barat', '23681'),
	(2, 21, 'Kabupaten', 'Aceh Barat Daya', '23764'),
	(3, 21, 'Kabupaten', 'Aceh Besar', '23951'),
	(4, 21, 'Kabupaten', 'Aceh Jaya', '23654'),
	(5, 21, 'Kabupaten', 'Aceh Selatan', '23719'),
	(6, 21, 'Kabupaten', 'Aceh Singkil', '24785'),
	(7, 21, 'Kabupaten', 'Aceh Tamiang', '24476'),
	(8, 21, 'Kabupaten', 'Aceh Tengah', '24511'),
	(9, 21, 'Kabupaten', 'Aceh Tenggara', '24611'),
	(10, 21, 'Kabupaten', 'Aceh Timur', '24454'),
	(11, 21, 'Kabupaten', 'Aceh Utara', '24382'),
	(12, 32, 'Kabupaten', 'Agam', '26411'),
	(13, 23, 'Kabupaten', 'Alor', '85811'),
	(14, 19, 'Kota', 'Ambon', '97222'),
	(15, 34, 'Kabupaten', 'Asahan', '21214'),
	(16, 24, 'Kabupaten', 'Asmat', '99777'),
	(17, 1, 'Kabupaten', 'Badung', '80351'),
	(18, 13, 'Kabupaten', 'Balangan', '71611'),
	(19, 15, 'Kota', 'Balikpapan', '76111'),
	(20, 21, 'Kota', 'Banda Aceh', '23238'),
	(21, 18, 'Kota', 'Bandar Lampung', '35139'),
	(22, 9, 'Kabupaten', 'Bandung', '40311'),
	(23, 9, 'Kota', 'Bandung', '40111'),
	(24, 9, 'Kabupaten', 'Bandung Barat', '40721'),
	(25, 29, 'Kabupaten', 'Banggai', '94711'),
	(26, 29, 'Kabupaten', 'Banggai Kepulauan', '94881'),
	(27, 2, 'Kabupaten', 'Bangka', '33212'),
	(28, 2, 'Kabupaten', 'Bangka Barat', '33315'),
	(29, 2, 'Kabupaten', 'Bangka Selatan', '33719'),
	(30, 2, 'Kabupaten', 'Bangka Tengah', '33613'),
	(31, 11, 'Kabupaten', 'Bangkalan', '69118'),
	(32, 1, 'Kabupaten', 'Bangli', '80619'),
	(33, 13, 'Kabupaten', 'Banjar', '70619'),
	(34, 9, 'Kota', 'Banjar', '46311'),
	(35, 13, 'Kota', 'Banjarbaru', '70712'),
	(36, 13, 'Kota', 'Banjarmasin', '70117'),
	(37, 10, 'Kabupaten', 'Banjarnegara', '53419'),
	(38, 28, 'Kabupaten', 'Bantaeng', '92411'),
	(39, 5, 'Kabupaten', 'Bantul', '55715'),
	(40, 33, 'Kabupaten', 'Banyuasin', '30911'),
	(41, 10, 'Kabupaten', 'Banyumas', '53114'),
	(42, 11, 'Kabupaten', 'Banyuwangi', '68416'),
	(43, 13, 'Kabupaten', 'Barito Kuala', '70511'),
	(44, 14, 'Kabupaten', 'Barito Selatan', '73711'),
	(45, 14, 'Kabupaten', 'Barito Timur', '73671'),
	(46, 14, 'Kabupaten', 'Barito Utara', '73881'),
	(47, 28, 'Kabupaten', 'Barru', '90719'),
	(48, 17, 'Kota', 'Batam', '29413'),
	(49, 10, 'Kabupaten', 'Batang', '51211'),
	(50, 8, 'Kabupaten', 'Batang Hari', '36613'),
	(51, 11, 'Kota', 'Batu', '65311'),
	(52, 34, 'Kabupaten', 'Batu Bara', '21655'),
	(53, 30, 'Kota', 'Bau-Bau', '93719'),
	(54, 9, 'Kabupaten', 'Bekasi', '17837'),
	(55, 9, 'Kota', 'Bekasi', '17121'),
	(56, 2, 'Kabupaten', 'Belitung', '33419'),
	(57, 2, 'Kabupaten', 'Belitung Timur', '33519'),
	(58, 23, 'Kabupaten', 'Belu', '85711'),
	(59, 21, 'Kabupaten', 'Bener Meriah', '24581'),
	(60, 26, 'Kabupaten', 'Bengkalis', '28719'),
	(61, 12, 'Kabupaten', 'Bengkayang', '79213'),
	(62, 4, 'Kota', 'Bengkulu', '38229'),
	(63, 4, 'Kabupaten', 'Bengkulu Selatan', '38519'),
	(64, 4, 'Kabupaten', 'Bengkulu Tengah', '38319'),
	(65, 4, 'Kabupaten', 'Bengkulu Utara', '38619'),
	(66, 15, 'Kabupaten', 'Berau', '77311'),
	(67, 24, 'Kabupaten', 'Biak Numfor', '98119'),
	(68, 22, 'Kabupaten', 'Bima', '84171'),
	(69, 22, 'Kota', 'Bima', '84139'),
	(70, 34, 'Kota', 'Binjai', '20712'),
	(71, 17, 'Kabupaten', 'Bintan', '29135'),
	(72, 21, 'Kabupaten', 'Bireuen', '24219'),
	(73, 31, 'Kota', 'Bitung', '95512'),
	(74, 11, 'Kabupaten', 'Blitar', '66171'),
	(75, 11, 'Kota', 'Blitar', '66124'),
	(76, 10, 'Kabupaten', 'Blora', '58219'),
	(77, 7, 'Kabupaten', 'Boalemo', '96319'),
	(78, 9, 'Kabupaten', 'Bogor', '16911'),
	(79, 9, 'Kota', 'Bogor', '16119'),
	(80, 11, 'Kabupaten', 'Bojonegoro', '62119'),
	(81, 31, 'Kabupaten', 'Bolaang Mongondow (Bolmong)', '95755'),
	(82, 31, 'Kabupaten', 'Bolaang Mongondow Selatan', '95774'),
	(83, 31, 'Kabupaten', 'Bolaang Mongondow Timur', '95783'),
	(84, 31, 'Kabupaten', 'Bolaang Mongondow Utara', '95765'),
	(85, 30, 'Kabupaten', 'Bombana', '93771'),
	(86, 11, 'Kabupaten', 'Bondowoso', '68219'),
	(87, 28, 'Kabupaten', 'Bone', '92713'),
	(88, 7, 'Kabupaten', 'Bone Bolango', '96511'),
	(89, 15, 'Kota', 'Bontang', '75313'),
	(90, 24, 'Kabupaten', 'Boven Digoel', '99662'),
	(91, 10, 'Kabupaten', 'Boyolali', '57312'),
	(92, 10, 'Kabupaten', 'Brebes', '52212'),
	(93, 32, 'Kota', 'Bukittinggi', '26115'),
	(94, 1, 'Kabupaten', 'Buleleng', '81111'),
	(95, 28, 'Kabupaten', 'Bulukumba', '92511'),
	(96, 16, 'Kabupaten', 'Bulungan (Bulongan)', '77211'),
	(97, 8, 'Kabupaten', 'Bungo', '37216'),
	(98, 29, 'Kabupaten', 'Buol', '94564'),
	(99, 19, 'Kabupaten', 'Buru', '97371'),
	(100, 19, 'Kabupaten', 'Buru Selatan', '97351'),
	(101, 30, 'Kabupaten', 'Buton', '93754'),
	(102, 30, 'Kabupaten', 'Buton Utara', '93745'),
	(103, 9, 'Kabupaten', 'Ciamis', '46211'),
	(104, 9, 'Kabupaten', 'Cianjur', '43217'),
	(105, 10, 'Kabupaten', 'Cilacap', '53211'),
	(106, 3, 'Kota', 'Cilegon', '42417'),
	(107, 9, 'Kota', 'Cimahi', '40512'),
	(108, 9, 'Kabupaten', 'Cirebon', '45611'),
	(109, 9, 'Kota', 'Cirebon', '45116'),
	(110, 34, 'Kabupaten', 'Dairi', '22211'),
	(111, 24, 'Kabupaten', 'Deiyai (Deliyai)', '98784'),
	(112, 34, 'Kabupaten', 'Deli Serdang', '20511'),
	(113, 10, 'Kabupaten', 'Demak', '59519'),
	(114, 1, 'Kota', 'Denpasar', '80227'),
	(115, 9, 'Kota', 'Depok', '16416'),
	(116, 32, 'Kabupaten', 'Dharmasraya', '27612'),
	(117, 24, 'Kabupaten', 'Dogiyai', '98866'),
	(118, 22, 'Kabupaten', 'Dompu', '84217'),
	(119, 29, 'Kabupaten', 'Donggala', '94341'),
	(120, 26, 'Kota', 'Dumai', '28811'),
	(121, 33, 'Kabupaten', 'Empat Lawang', '31811'),
	(122, 23, 'Kabupaten', 'Ende', '86351'),
	(123, 28, 'Kabupaten', 'Enrekang', '91719'),
	(124, 25, 'Kabupaten', 'Fakfak', '98651'),
	(125, 23, 'Kabupaten', 'Flores Timur', '86213'),
	(126, 9, 'Kabupaten', 'Garut', '44126'),
	(127, 21, 'Kabupaten', 'Gayo Lues', '24653'),
	(128, 1, 'Kabupaten', 'Gianyar', '80519'),
	(129, 7, 'Kabupaten', 'Gorontalo', '96218'),
	(130, 7, 'Kota', 'Gorontalo', '96115'),
	(131, 7, 'Kabupaten', 'Gorontalo Utara', '96611'),
	(132, 28, 'Kabupaten', 'Gowa', '92111'),
	(133, 11, 'Kabupaten', 'Gresik', '61115'),
	(134, 10, 'Kabupaten', 'Grobogan', '58111'),
	(135, 5, 'Kabupaten', 'Gunung Kidul', '55812'),
	(136, 14, 'Kabupaten', 'Gunung Mas', '74511'),
	(137, 34, 'Kota', 'Gunungsitoli', '22813'),
	(138, 20, 'Kabupaten', 'Halmahera Barat', '97757'),
	(139, 20, 'Kabupaten', 'Halmahera Selatan', '97911'),
	(140, 20, 'Kabupaten', 'Halmahera Tengah', '97853'),
	(141, 20, 'Kabupaten', 'Halmahera Timur', '97862'),
	(142, 20, 'Kabupaten', 'Halmahera Utara', '97762'),
	(143, 13, 'Kabupaten', 'Hulu Sungai Selatan', '71212'),
	(144, 13, 'Kabupaten', 'Hulu Sungai Tengah', '71313'),
	(145, 13, 'Kabupaten', 'Hulu Sungai Utara', '71419'),
	(146, 34, 'Kabupaten', 'Humbang Hasundutan', '22457'),
	(147, 26, 'Kabupaten', 'Indragiri Hilir', '29212'),
	(148, 26, 'Kabupaten', 'Indragiri Hulu', '29319'),
	(149, 9, 'Kabupaten', 'Indramayu', '45214'),
	(150, 24, 'Kabupaten', 'Intan Jaya', '98771'),
	(151, 6, 'Kota', 'Jakarta Barat', '11220'),
	(152, 6, 'Kota', 'Jakarta Pusat', '10540'),
	(153, 6, 'Kota', 'Jakarta Selatan', '12230'),
	(154, 6, 'Kota', 'Jakarta Timur', '13330'),
	(155, 6, 'Kota', 'Jakarta Utara', '14140'),
	(156, 8, 'Kota', 'Jambi', '36111'),
	(157, 24, 'Kabupaten', 'Jayapura', '99352'),
	(158, 24, 'Kota', 'Jayapura', '99114'),
	(159, 24, 'Kabupaten', 'Jayawijaya', '99511'),
	(160, 11, 'Kabupaten', 'Jember', '68113'),
	(161, 1, 'Kabupaten', 'Jembrana', '82251'),
	(162, 28, 'Kabupaten', 'Jeneponto', '92319'),
	(163, 10, 'Kabupaten', 'Jepara', '59419'),
	(164, 11, 'Kabupaten', 'Jombang', '61415'),
	(165, 25, 'Kabupaten', 'Kaimana', '98671'),
	(166, 26, 'Kabupaten', 'Kampar', '28411'),
	(167, 14, 'Kabupaten', 'Kapuas', '73583'),
	(168, 12, 'Kabupaten', 'Kapuas Hulu', '78719'),
	(169, 10, 'Kabupaten', 'Karanganyar', '57718'),
	(170, 1, 'Kabupaten', 'Karangasem', '80819'),
	(171, 9, 'Kabupaten', 'Karawang', '41311'),
	(172, 17, 'Kabupaten', 'Karimun', '29611'),
	(173, 34, 'Kabupaten', 'Karo', '22119'),
	(174, 14, 'Kabupaten', 'Katingan', '74411'),
	(175, 4, 'Kabupaten', 'Kaur', '38911'),
	(176, 12, 'Kabupaten', 'Kayong Utara', '78852'),
	(177, 10, 'Kabupaten', 'Kebumen', '54319'),
	(178, 11, 'Kabupaten', 'Kediri', '64184'),
	(179, 11, 'Kota', 'Kediri', '64125'),
	(180, 24, 'Kabupaten', 'Keerom', '99461'),
	(181, 10, 'Kabupaten', 'Kendal', '51314'),
	(182, 30, 'Kota', 'Kendari', '93126'),
	(183, 4, 'Kabupaten', 'Kepahiang', '39319'),
	(184, 17, 'Kabupaten', 'Kepulauan Anambas', '29991'),
	(185, 19, 'Kabupaten', 'Kepulauan Aru', '97681'),
	(186, 32, 'Kabupaten', 'Kepulauan Mentawai', '25771'),
	(187, 26, 'Kabupaten', 'Kepulauan Meranti', '28791'),
	(188, 31, 'Kabupaten', 'Kepulauan Sangihe', '95819'),
	(189, 6, 'Kabupaten', 'Kepulauan Seribu', '14550'),
	(190, 31, 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '95862'),
	(191, 20, 'Kabupaten', 'Kepulauan Sula', '97995'),
	(192, 31, 'Kabupaten', 'Kepulauan Talaud', '95885'),
	(193, 24, 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', '98211'),
	(194, 8, 'Kabupaten', 'Kerinci', '37167'),
	(195, 12, 'Kabupaten', 'Ketapang', '78874'),
	(196, 10, 'Kabupaten', 'Klaten', '57411'),
	(197, 1, 'Kabupaten', 'Klungkung', '80719'),
	(198, 30, 'Kabupaten', 'Kolaka', '93511'),
	(199, 30, 'Kabupaten', 'Kolaka Utara', '93911'),
	(200, 30, 'Kabupaten', 'Konawe', '93411'),
	(201, 30, 'Kabupaten', 'Konawe Selatan', '93811'),
	(202, 30, 'Kabupaten', 'Konawe Utara', '93311'),
	(203, 13, 'Kabupaten', 'Kotabaru', '72119'),
	(204, 31, 'Kota', 'Kotamobagu', '95711'),
	(205, 14, 'Kabupaten', 'Kotawaringin Barat', '74119'),
	(206, 14, 'Kabupaten', 'Kotawaringin Timur', '74364'),
	(207, 26, 'Kabupaten', 'Kuantan Singingi', '29519'),
	(208, 12, 'Kabupaten', 'Kubu Raya', '78311'),
	(209, 10, 'Kabupaten', 'Kudus', '59311'),
	(210, 5, 'Kabupaten', 'Kulon Progo', '55611'),
	(211, 9, 'Kabupaten', 'Kuningan', '45511'),
	(212, 23, 'Kabupaten', 'Kupang', '85362'),
	(213, 23, 'Kota', 'Kupang', '85119'),
	(214, 15, 'Kabupaten', 'Kutai Barat', '75711'),
	(215, 15, 'Kabupaten', 'Kutai Kartanegara', '75511'),
	(216, 15, 'Kabupaten', 'Kutai Timur', '75611'),
	(217, 34, 'Kabupaten', 'Labuhan Batu', '21412'),
	(218, 34, 'Kabupaten', 'Labuhan Batu Selatan', '21511'),
	(219, 34, 'Kabupaten', 'Labuhan Batu Utara', '21711'),
	(220, 33, 'Kabupaten', 'Lahat', '31419'),
	(221, 14, 'Kabupaten', 'Lamandau', '74611'),
	(222, 11, 'Kabupaten', 'Lamongan', '64125'),
	(223, 18, 'Kabupaten', 'Lampung Barat', '34814'),
	(224, 18, 'Kabupaten', 'Lampung Selatan', '35511'),
	(225, 18, 'Kabupaten', 'Lampung Tengah', '34212'),
	(226, 18, 'Kabupaten', 'Lampung Timur', '34319'),
	(227, 18, 'Kabupaten', 'Lampung Utara', '34516'),
	(228, 12, 'Kabupaten', 'Landak', '78319'),
	(229, 34, 'Kabupaten', 'Langkat', '20811'),
	(230, 21, 'Kota', 'Langsa', '24412'),
	(231, 24, 'Kabupaten', 'Lanny Jaya', '99531'),
	(232, 3, 'Kabupaten', 'Lebak', '42319'),
	(233, 4, 'Kabupaten', 'Lebong', '39264'),
	(234, 23, 'Kabupaten', 'Lembata', '86611'),
	(235, 21, 'Kota', 'Lhokseumawe', '24352'),
	(236, 32, 'Kabupaten', 'Lima Puluh Koto/Kota', '26671'),
	(237, 17, 'Kabupaten', 'Lingga', '29811'),
	(238, 22, 'Kabupaten', 'Lombok Barat', '83311'),
	(239, 22, 'Kabupaten', 'Lombok Tengah', '83511'),
	(240, 22, 'Kabupaten', 'Lombok Timur', '83612'),
	(241, 22, 'Kabupaten', 'Lombok Utara', '83711'),
	(242, 33, 'Kota', 'Lubuk Linggau', '31614'),
	(243, 11, 'Kabupaten', 'Lumajang', '67319'),
	(244, 28, 'Kabupaten', 'Luwu', '91994'),
	(245, 28, 'Kabupaten', 'Luwu Timur', '92981'),
	(246, 28, 'Kabupaten', 'Luwu Utara', '92911'),
	(247, 11, 'Kabupaten', 'Madiun', '63153'),
	(248, 11, 'Kota', 'Madiun', '63122'),
	(249, 10, 'Kabupaten', 'Magelang', '56519'),
	(250, 10, 'Kota', 'Magelang', '56133'),
	(251, 11, 'Kabupaten', 'Magetan', '63314'),
	(252, 9, 'Kabupaten', 'Majalengka', '45412'),
	(253, 27, 'Kabupaten', 'Majene', '91411'),
	(254, 28, 'Kota', 'Makassar', '90111'),
	(255, 11, 'Kabupaten', 'Malang', '65163'),
	(256, 11, 'Kota', 'Malang', '65112'),
	(257, 16, 'Kabupaten', 'Malinau', '77511'),
	(258, 19, 'Kabupaten', 'Maluku Barat Daya', '97451'),
	(259, 19, 'Kabupaten', 'Maluku Tengah', '97513'),
	(260, 19, 'Kabupaten', 'Maluku Tenggara', '97651'),
	(261, 19, 'Kabupaten', 'Maluku Tenggara Barat', '97465'),
	(262, 27, 'Kabupaten', 'Mamasa', '91362'),
	(263, 24, 'Kabupaten', 'Mamberamo Raya', '99381'),
	(264, 24, 'Kabupaten', 'Mamberamo Tengah', '99553'),
	(265, 27, 'Kabupaten', 'Mamuju', '91519'),
	(266, 27, 'Kabupaten', 'Mamuju Utara', '91571'),
	(267, 31, 'Kota', 'Manado', '95247'),
	(268, 34, 'Kabupaten', 'Mandailing Natal', '22916'),
	(269, 23, 'Kabupaten', 'Manggarai', '86551'),
	(270, 23, 'Kabupaten', 'Manggarai Barat', '86711'),
	(271, 23, 'Kabupaten', 'Manggarai Timur', '86811'),
	(272, 25, 'Kabupaten', 'Manokwari', '98311'),
	(273, 25, 'Kabupaten', 'Manokwari Selatan', '98355'),
	(274, 24, 'Kabupaten', 'Mappi', '99853'),
	(275, 28, 'Kabupaten', 'Maros', '90511'),
	(276, 22, 'Kota', 'Mataram', '83131'),
	(277, 25, 'Kabupaten', 'Maybrat', '98051'),
	(278, 34, 'Kota', 'Medan', '20228'),
	(279, 12, 'Kabupaten', 'Melawi', '78619'),
	(280, 8, 'Kabupaten', 'Merangin', '37319'),
	(281, 24, 'Kabupaten', 'Merauke', '99613'),
	(282, 18, 'Kabupaten', 'Mesuji', '34911'),
	(283, 18, 'Kota', 'Metro', '34111'),
	(284, 24, 'Kabupaten', 'Mimika', '99962'),
	(285, 31, 'Kabupaten', 'Minahasa', '95614'),
	(286, 31, 'Kabupaten', 'Minahasa Selatan', '95914'),
	(287, 31, 'Kabupaten', 'Minahasa Tenggara', '95995'),
	(288, 31, 'Kabupaten', 'Minahasa Utara', '95316'),
	(289, 11, 'Kabupaten', 'Mojokerto', '61382'),
	(290, 11, 'Kota', 'Mojokerto', '61316'),
	(291, 29, 'Kabupaten', 'Morowali', '94911'),
	(292, 33, 'Kabupaten', 'Muara Enim', '31315'),
	(293, 8, 'Kabupaten', 'Muaro Jambi', '36311'),
	(294, 4, 'Kabupaten', 'Muko Muko', '38715'),
	(295, 30, 'Kabupaten', 'Muna', '93611'),
	(296, 14, 'Kabupaten', 'Murung Raya', '73911'),
	(297, 33, 'Kabupaten', 'Musi Banyuasin', '30719'),
	(298, 33, 'Kabupaten', 'Musi Rawas', '31661'),
	(299, 24, 'Kabupaten', 'Nabire', '98816'),
	(300, 21, 'Kabupaten', 'Nagan Raya', '23674'),
	(301, 23, 'Kabupaten', 'Nagekeo', '86911'),
	(302, 17, 'Kabupaten', 'Natuna', '29711'),
	(303, 24, 'Kabupaten', 'Nduga', '99541'),
	(304, 23, 'Kabupaten', 'Ngada', '86413'),
	(305, 11, 'Kabupaten', 'Nganjuk', '64414'),
	(306, 11, 'Kabupaten', 'Ngawi', '63219'),
	(307, 34, 'Kabupaten', 'Nias', '22876'),
	(308, 34, 'Kabupaten', 'Nias Barat', '22895'),
	(309, 34, 'Kabupaten', 'Nias Selatan', '22865'),
	(310, 34, 'Kabupaten', 'Nias Utara', '22856'),
	(311, 16, 'Kabupaten', 'Nunukan', '77421'),
	(312, 33, 'Kabupaten', 'Ogan Ilir', '30811'),
	(313, 33, 'Kabupaten', 'Ogan Komering Ilir', '30618'),
	(314, 33, 'Kabupaten', 'Ogan Komering Ulu', '32112'),
	(315, 33, 'Kabupaten', 'Ogan Komering Ulu Selatan', '32211'),
	(316, 33, 'Kabupaten', 'Ogan Komering Ulu Timur', '32312'),
	(317, 11, 'Kabupaten', 'Pacitan', '63512'),
	(318, 32, 'Kota', 'Padang', '25112'),
	(319, 34, 'Kabupaten', 'Padang Lawas', '22763'),
	(320, 34, 'Kabupaten', 'Padang Lawas Utara', '22753'),
	(321, 32, 'Kota', 'Padang Panjang', '27122'),
	(322, 32, 'Kabupaten', 'Padang Pariaman', '25583'),
	(323, 34, 'Kota', 'Padang Sidempuan', '22727'),
	(324, 33, 'Kota', 'Pagar Alam', '31512'),
	(325, 34, 'Kabupaten', 'Pakpak Bharat', '22272'),
	(326, 14, 'Kota', 'Palangka Raya', '73112'),
	(327, 33, 'Kota', 'Palembang', '31512'),
	(328, 28, 'Kota', 'Palopo', '91911'),
	(329, 29, 'Kota', 'Palu', '94111'),
	(330, 11, 'Kabupaten', 'Pamekasan', '69319'),
	(331, 3, 'Kabupaten', 'Pandeglang', '42212'),
	(332, 9, 'Kabupaten', 'Pangandaran', '46511'),
	(333, 28, 'Kabupaten', 'Pangkajene Kepulauan', '90611'),
	(334, 2, 'Kota', 'Pangkal Pinang', '33115'),
	(335, 24, 'Kabupaten', 'Paniai', '98765'),
	(336, 28, 'Kota', 'Parepare', '91123'),
	(337, 32, 'Kota', 'Pariaman', '25511'),
	(338, 29, 'Kabupaten', 'Parigi Moutong', '94411'),
	(339, 32, 'Kabupaten', 'Pasaman', '26318'),
	(340, 32, 'Kabupaten', 'Pasaman Barat', '26511'),
	(341, 15, 'Kabupaten', 'Paser', '76211'),
	(342, 11, 'Kabupaten', 'Pasuruan', '67153'),
	(343, 11, 'Kota', 'Pasuruan', '67118'),
	(344, 10, 'Kabupaten', 'Pati', '59114'),
	(345, 32, 'Kota', 'Payakumbuh', '26213'),
	(346, 25, 'Kabupaten', 'Pegunungan Arfak', '98354'),
	(347, 24, 'Kabupaten', 'Pegunungan Bintang', '99573'),
	(348, 10, 'Kabupaten', 'Pekalongan', '51161'),
	(349, 10, 'Kota', 'Pekalongan', '51122'),
	(350, 26, 'Kota', 'Pekanbaru', '28112'),
	(351, 26, 'Kabupaten', 'Pelalawan', '28311'),
	(352, 10, 'Kabupaten', 'Pemalang', '52319'),
	(353, 34, 'Kota', 'Pematang Siantar', '21126'),
	(354, 15, 'Kabupaten', 'Penajam Paser Utara', '76311'),
	(355, 18, 'Kabupaten', 'Pesawaran', '35312'),
	(356, 18, 'Kabupaten', 'Pesisir Barat', '35974'),
	(357, 32, 'Kabupaten', 'Pesisir Selatan', '25611'),
	(358, 21, 'Kabupaten', 'Pidie', '24116'),
	(359, 21, 'Kabupaten', 'Pidie Jaya', '24186'),
	(360, 28, 'Kabupaten', 'Pinrang', '91251'),
	(361, 7, 'Kabupaten', 'Pohuwato', '96419'),
	(362, 27, 'Kabupaten', 'Polewali Mandar', '91311'),
	(363, 11, 'Kabupaten', 'Ponorogo', '63411'),
	(364, 12, 'Kabupaten', 'Pontianak', '78971'),
	(365, 12, 'Kota', 'Pontianak', '78112'),
	(366, 29, 'Kabupaten', 'Poso', '94615'),
	(367, 33, 'Kota', 'Prabumulih', '31121'),
	(368, 18, 'Kabupaten', 'Pringsewu', '35719'),
	(369, 11, 'Kabupaten', 'Probolinggo', '67282'),
	(370, 11, 'Kota', 'Probolinggo', '67215'),
	(371, 14, 'Kabupaten', 'Pulang Pisau', '74811'),
	(372, 20, 'Kabupaten', 'Pulau Morotai', '97771'),
	(373, 24, 'Kabupaten', 'Puncak', '98981'),
	(374, 24, 'Kabupaten', 'Puncak Jaya', '98979'),
	(375, 10, 'Kabupaten', 'Purbalingga', '53312'),
	(376, 9, 'Kabupaten', 'Purwakarta', '41119'),
	(377, 10, 'Kabupaten', 'Purworejo', '54111'),
	(378, 25, 'Kabupaten', 'Raja Ampat', '98489'),
	(379, 4, 'Kabupaten', 'Rejang Lebong', '39112'),
	(380, 10, 'Kabupaten', 'Rembang', '59219'),
	(381, 26, 'Kabupaten', 'Rokan Hilir', '28992'),
	(382, 26, 'Kabupaten', 'Rokan Hulu', '28511'),
	(383, 23, 'Kabupaten', 'Rote Ndao', '85982'),
	(384, 21, 'Kota', 'Sabang', '23512'),
	(385, 23, 'Kabupaten', 'Sabu Raijua', '85391'),
	(386, 10, 'Kota', 'Salatiga', '50711'),
	(387, 15, 'Kota', 'Samarinda', '75133'),
	(388, 12, 'Kabupaten', 'Sambas', '79453'),
	(389, 34, 'Kabupaten', 'Samosir', '22392'),
	(390, 11, 'Kabupaten', 'Sampang', '69219'),
	(391, 12, 'Kabupaten', 'Sanggau', '78557'),
	(392, 24, 'Kabupaten', 'Sarmi', '99373'),
	(393, 8, 'Kabupaten', 'Sarolangun', '37419'),
	(394, 32, 'Kota', 'Sawah Lunto', '27416'),
	(395, 12, 'Kabupaten', 'Sekadau', '79583'),
	(396, 28, 'Kabupaten', 'Selayar (Kepulauan Selayar)', '92812'),
	(397, 4, 'Kabupaten', 'Seluma', '38811'),
	(398, 10, 'Kabupaten', 'Semarang', '50511'),
	(399, 10, 'Kota', 'Semarang', '50135'),
	(400, 19, 'Kabupaten', 'Seram Bagian Barat', '97561'),
	(401, 19, 'Kabupaten', 'Seram Bagian Timur', '97581'),
	(402, 3, 'Kabupaten', 'Serang', '42182'),
	(403, 3, 'Kota', 'Serang', '42111'),
	(404, 34, 'Kabupaten', 'Serdang Bedagai', '20915'),
	(405, 14, 'Kabupaten', 'Seruyan', '74211'),
	(406, 26, 'Kabupaten', 'Siak', '28623'),
	(407, 34, 'Kota', 'Sibolga', '22522'),
	(408, 28, 'Kabupaten', 'Sidenreng Rappang/Rapang', '91613'),
	(409, 11, 'Kabupaten', 'Sidoarjo', '61219'),
	(410, 29, 'Kabupaten', 'Sigi', '94364'),
	(411, 32, 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', '27511'),
	(412, 23, 'Kabupaten', 'Sikka', '86121'),
	(413, 34, 'Kabupaten', 'Simalungun', '21162'),
	(414, 21, 'Kabupaten', 'Simeulue', '23891'),
	(415, 12, 'Kota', 'Singkawang', '79117'),
	(416, 28, 'Kabupaten', 'Sinjai', '92615'),
	(417, 12, 'Kabupaten', 'Sintang', '78619'),
	(418, 11, 'Kabupaten', 'Situbondo', '68316'),
	(419, 5, 'Kabupaten', 'Sleman', '55513'),
	(420, 32, 'Kabupaten', 'Solok', '27365'),
	(421, 32, 'Kota', 'Solok', '27315'),
	(422, 32, 'Kabupaten', 'Solok Selatan', '27779'),
	(423, 28, 'Kabupaten', 'Soppeng', '90812'),
	(424, 25, 'Kabupaten', 'Sorong', '98431'),
	(425, 25, 'Kota', 'Sorong', '98411'),
	(426, 25, 'Kabupaten', 'Sorong Selatan', '98454'),
	(427, 10, 'Kabupaten', 'Sragen', '57211'),
	(428, 9, 'Kabupaten', 'Subang', '41215'),
	(429, 21, 'Kota', 'Subulussalam', '24882'),
	(430, 9, 'Kabupaten', 'Sukabumi', '43311'),
	(431, 9, 'Kota', 'Sukabumi', '43114'),
	(432, 14, 'Kabupaten', 'Sukamara', '74712'),
	(433, 10, 'Kabupaten', 'Sukoharjo', '57514'),
	(434, 23, 'Kabupaten', 'Sumba Barat', '87219'),
	(435, 23, 'Kabupaten', 'Sumba Barat Daya', '87453'),
	(436, 23, 'Kabupaten', 'Sumba Tengah', '87358'),
	(437, 23, 'Kabupaten', 'Sumba Timur', '87112'),
	(438, 22, 'Kabupaten', 'Sumbawa', '84315'),
	(439, 22, 'Kabupaten', 'Sumbawa Barat', '84419'),
	(440, 9, 'Kabupaten', 'Sumedang', '45326'),
	(441, 11, 'Kabupaten', 'Sumenep', '69413'),
	(442, 8, 'Kota', 'Sungaipenuh', '37113'),
	(443, 24, 'Kabupaten', 'Supiori', '98164'),
	(444, 11, 'Kota', 'Surabaya', '60119'),
	(445, 10, 'Kota', 'Surakarta (Solo)', '57113'),
	(446, 13, 'Kabupaten', 'Tabalong', '71513'),
	(447, 1, 'Kabupaten', 'Tabanan', '82119'),
	(448, 28, 'Kabupaten', 'Takalar', '92212'),
	(449, 25, 'Kabupaten', 'Tambrauw', '98475'),
	(450, 16, 'Kabupaten', 'Tana Tidung', '77611'),
	(451, 28, 'Kabupaten', 'Tana Toraja', '91819'),
	(452, 13, 'Kabupaten', 'Tanah Bumbu', '72211'),
	(453, 32, 'Kabupaten', 'Tanah Datar', '27211'),
	(454, 13, 'Kabupaten', 'Tanah Laut', '70811'),
	(455, 3, 'Kabupaten', 'Tangerang', '15914'),
	(456, 3, 'Kota', 'Tangerang', '15111'),
	(457, 3, 'Kota', 'Tangerang Selatan', '15332'),
	(458, 18, 'Kabupaten', 'Tanggamus', '35619'),
	(459, 34, 'Kota', 'Tanjung Balai', '21321'),
	(460, 8, 'Kabupaten', 'Tanjung Jabung Barat', '36513'),
	(461, 8, 'Kabupaten', 'Tanjung Jabung Timur', '36719'),
	(462, 17, 'Kota', 'Tanjung Pinang', '29111'),
	(463, 34, 'Kabupaten', 'Tapanuli Selatan', '22742'),
	(464, 34, 'Kabupaten', 'Tapanuli Tengah', '22611'),
	(465, 34, 'Kabupaten', 'Tapanuli Utara', '22414'),
	(466, 13, 'Kabupaten', 'Tapin', '71119'),
	(467, 16, 'Kota', 'Tarakan', '77114'),
	(468, 9, 'Kabupaten', 'Tasikmalaya', '46411'),
	(469, 9, 'Kota', 'Tasikmalaya', '46116'),
	(470, 34, 'Kota', 'Tebing Tinggi', '20632'),
	(471, 8, 'Kabupaten', 'Tebo', '37519'),
	(472, 10, 'Kabupaten', 'Tegal', '52419'),
	(473, 10, 'Kota', 'Tegal', '52114'),
	(474, 25, 'Kabupaten', 'Teluk Bintuni', '98551'),
	(475, 25, 'Kabupaten', 'Teluk Wondama', '98591'),
	(476, 10, 'Kabupaten', 'Temanggung', '56212'),
	(477, 20, 'Kota', 'Ternate', '97714'),
	(478, 20, 'Kota', 'Tidore Kepulauan', '97815'),
	(479, 23, 'Kabupaten', 'Timor Tengah Selatan', '85562'),
	(480, 23, 'Kabupaten', 'Timor Tengah Utara', '85612'),
	(481, 34, 'Kabupaten', 'Toba Samosir', '22316'),
	(482, 29, 'Kabupaten', 'Tojo Una-Una', '94683'),
	(483, 29, 'Kabupaten', 'Toli-Toli', '94542'),
	(484, 24, 'Kabupaten', 'Tolikara', '99411'),
	(485, 31, 'Kota', 'Tomohon', '95416'),
	(486, 28, 'Kabupaten', 'Toraja Utara', '91831'),
	(487, 11, 'Kabupaten', 'Trenggalek', '66312'),
	(488, 19, 'Kota', 'Tual', '97612'),
	(489, 11, 'Kabupaten', 'Tuban', '62319'),
	(490, 18, 'Kabupaten', 'Tulang Bawang', '34613'),
	(491, 18, 'Kabupaten', 'Tulang Bawang Barat', '34419'),
	(492, 11, 'Kabupaten', 'Tulungagung', '66212'),
	(493, 28, 'Kabupaten', 'Wajo', '90911'),
	(494, 30, 'Kabupaten', 'Wakatobi', '93791'),
	(495, 24, 'Kabupaten', 'Waropen', '98269'),
	(496, 18, 'Kabupaten', 'Way Kanan', '34711'),
	(497, 10, 'Kabupaten', 'Wonogiri', '57619'),
	(498, 10, 'Kabupaten', 'Wonosobo', '56311'),
	(499, 24, 'Kabupaten', 'Yahukimo', '99041'),
	(500, 24, 'Kabupaten', 'Yalimo', '99481'),
	(501, 5, 'Kota', 'Yogyakarta', '55222');
/*!40000 ALTER TABLE `ongkir_city` ENABLE KEYS */;

-- Dumping structure for table bukku.ongkir_cost
CREATE TABLE IF NOT EXISTS `ongkir_cost` (
  `origin_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL COMMENT 'berat dalam gram',
  `code` varchar(50) NOT NULL COMMENT 'kode kurir',
  `name` varchar(255) NOT NULL COMMENT 'nama kurir',
  `service` varchar(50) NOT NULL COMMENT 'service kurir = YES, OKE, REG',
  `value` decimal(10,0) NOT NULL COMMENT 'ongkos kirim',
  `est` int(11) NOT NULL COMMENT 'estimasi pengiriman'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.ongkir_cost: ~0 rows (approximately)
DELETE FROM `ongkir_cost`;
/*!40000 ALTER TABLE `ongkir_cost` DISABLE KEYS */;
/*!40000 ALTER TABLE `ongkir_cost` ENABLE KEYS */;

-- Dumping structure for table bukku.ongkir_province
CREATE TABLE IF NOT EXISTS `ongkir_province` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bukku.ongkir_province: ~34 rows (approximately)
DELETE FROM `ongkir_province`;
/*!40000 ALTER TABLE `ongkir_province` DISABLE KEYS */;
INSERT INTO `ongkir_province` (`id`, `state_id`, `name`) VALUES
	(1, 1, 'Bali'),
	(2, 1, 'Bangka Belitung'),
	(3, 1, 'Banten'),
	(4, 1, 'Bengkulu'),
	(5, 1, 'DI Yogyakarta'),
	(6, 1, 'DKI Jakarta'),
	(7, 1, 'Gorontalo'),
	(8, 1, 'Jambi'),
	(9, 1, 'Jawa Barat'),
	(10, 1, 'Jawa Tengah'),
	(11, 1, 'Jawa Timur'),
	(12, 1, 'Kalimantan Barat'),
	(13, 1, 'Kalimantan Selatan'),
	(14, 1, 'Kalimantan Tengah'),
	(15, 1, 'Kalimantan Timur'),
	(16, 1, 'Kalimantan Utara'),
	(17, 1, 'Kepulauan Riau'),
	(18, 1, 'Lampung'),
	(19, 1, 'Maluku'),
	(20, 1, 'Maluku Utara'),
	(21, 1, 'Nanggroe Aceh Darussalam (NAD)'),
	(22, 1, 'Nusa Tenggara Barat (NTB)'),
	(23, 1, 'Nusa Tenggara Timur (NTT)'),
	(24, 1, 'Papua'),
	(25, 1, 'Papua Barat'),
	(26, 1, 'Riau'),
	(27, 1, 'Sulawesi Barat'),
	(28, 1, 'Sulawesi Selatan'),
	(29, 1, 'Sulawesi Tengah'),
	(30, 1, 'Sulawesi Tenggara'),
	(31, 1, 'Sulawesi Utara'),
	(32, 1, 'Sumatera Barat'),
	(33, 1, 'Sumatera Selatan'),
	(34, 1, 'Sumatera Utara');
/*!40000 ALTER TABLE `ongkir_province` ENABLE KEYS */;

-- Dumping structure for table bukku.ongkir_state
CREATE TABLE IF NOT EXISTS `ongkir_state` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bukku.ongkir_state: ~3 rows (approximately)
DELETE FROM `ongkir_state`;
/*!40000 ALTER TABLE `ongkir_state` DISABLE KEYS */;
INSERT INTO `ongkir_state` (`id`, `name`) VALUES
	(1, 'Indonesia'),
	(2, 'Malaysia'),
	(3, 'Singapore');
/*!40000 ALTER TABLE `ongkir_state` ENABLE KEYS */;

-- Dumping structure for table bukku.tr_cart
CREATE TABLE IF NOT EXISTS `tr_cart` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id` (`customer_id`),
  KEY `product_code` (`product_code`),
  CONSTRAINT `cart_fk_customer` FOREIGN KEY (`customer_id`) REFERENCES `mst_customer` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `cart_fk_product` FOREIGN KEY (`product_code`) REFERENCES `ms_product` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.tr_cart: ~2 rows (approximately)
DELETE FROM `tr_cart`;
/*!40000 ALTER TABLE `tr_cart` DISABLE KEYS */;
INSERT INTO `tr_cart` (`ID`, `customer_id`, `product_code`) VALUES
	(3, 9, 'MT16007'),
	(4, 9, 'MT16008');
/*!40000 ALTER TABLE `tr_cart` ENABLE KEYS */;

-- Dumping structure for table bukku.tr_stock
CREATE TABLE IF NOT EXISTS `tr_stock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `book_code` varchar(50) NOT NULL,
  `stock_type` varchar(50) NOT NULL,
  `last_stock` int(11) NOT NULL COMMENT 'stok terakhir di gudang',
  `re_stock` int(11) NOT NULL COMMENT 'jumlah stok yang ditambahkan',
  `restock_date` datetime NOT NULL,
  `total_stock` int(11) NOT NULL COMMENT 'last stock + restock',
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.tr_stock: ~0 rows (approximately)
DELETE FROM `tr_stock`;
/*!40000 ALTER TABLE `tr_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `tr_stock` ENABLE KEYS */;

-- Dumping structure for table bukku.tr_transaction
CREATE TABLE IF NOT EXISTS `tr_transaction` (
  `order_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `order_date` datetime NOT NULL,
  `unique_code` int(11) NOT NULL COMMENT 'per hari max 1.000, jika melebihi reset 0',
  `customer_id` bigint(20) NOT NULL,
  `channel` enum('WA','LINE','WEB','') NOT NULL,
  `confirmation_date` datetime NOT NULL,
  `confirmation_bank` varchar(50) NOT NULL COMMENT 'nama bank',
  `confirmation_bank_owner` varchar(50) NOT NULL COMMENT 'nama pemilik rekening',
  `confirmation_image` varchar(255) NOT NULL COMMENT 'path url bukti pembayaran',
  `bank_bukku` varchar(20) NOT NULL COMMENT 'tujuan transfer bank bukku',
  `address_id` bigint(20) NOT NULL,
  `courier_date` datetime NOT NULL COMMENT 'tanggal pengiriman',
  `courier_name` varchar(50) NOT NULL COMMENT 'nama jasa kurir',
  `courier_type` varchar(50) NOT NULL COMMENT 'jenis service pengiriman',
  `courier_resi` varchar(255) NOT NULL COMMENT 'nomor resi',
  `courier_cost` bigint(20) unsigned NOT NULL COMMENT 'biaya pengiriman',
  `courier_weight` decimal(10,0) unsigned NOT NULL,
  `shopping_price` bigint(20) NOT NULL COMMENT 'sum(ProductTotalPrice) from tr_transaction_detail',
  `promo_code` varchar(50) NOT NULL,
  `reduction_price` bigint(20) NOT NULL DEFAULT '0' COMMENT 'potongan harga dari promo',
  `total_price` bigint(20) NOT NULL COMMENT 'total bayar = shopping price + ongkos - promo',
  `order_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 Batal, 1 Menunggu Pembayaran, 2 Telah konfirmasi, Siap Proses, 3 Dikirim segera input resi, 4 Selesai, 99 shopping cart',
  `is_refund` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'diisi jika order status 0, value = 0 belum refund, 1 sudah refund',
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `customer_id` (`customer_id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `transaction_fk_customer` FOREIGN KEY (`customer_id`) REFERENCES `mst_customer` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.tr_transaction: ~15 rows (approximately)
DELETE FROM `tr_transaction`;
/*!40000 ALTER TABLE `tr_transaction` DISABLE KEYS */;
INSERT INTO `tr_transaction` (`order_id`, `order_date`, `unique_code`, `customer_id`, `channel`, `confirmation_date`, `confirmation_bank`, `confirmation_bank_owner`, `confirmation_image`, `bank_bukku`, `address_id`, `courier_date`, `courier_name`, `courier_type`, `courier_resi`, `courier_cost`, `courier_weight`, `shopping_price`, `promo_code`, `reduction_price`, `total_price`, `order_status`, `is_refund`, `modified_by`, `modified_date`) VALUES
	(00000000002, '2018-02-04 14:11:44', 2, 9, 'WEB', '2018-02-04 14:16:41', '014', 'Regie Pahlewi', '', '014', 2, '2018-02-05 04:11:55', 'JNE', 'REG', '', 9000, 1, 45000, '', 0, 54000, 5, 0, '', '0000-00-00 00:00:00'),
	(00000000003, '2018-02-05 04:07:44', 3, 9, 'WEB', '2018-02-05 04:12:19', '014', 'Regie Pahlewi', '', '014', 2, '2018-02-05 04:12:01', 'JNE', 'REG', '', 9000, 1, 45000, '', 0, 54000, 5, 0, '', '0000-00-00 00:00:00'),
	(00000000004, '2018-02-05 10:57:21', 4, 9, 'WEB', '2018-02-05 11:06:23', '014', 'Regie Pahlewi', '', '014', 2, '2018-02-05 11:04:54', 'JNE', 'REG', '', 9000, 1, 90000, '', 0, 99004, 3, 0, '', '0000-00-00 00:00:00'),
	(00000000005, '2018-02-05 11:07:49', 5, 9, 'WEB', '0000-00-00 00:00:00', '', '', '', '', 2, '0000-00-00 00:00:00', '', '', '', 0, 0, 144000, '', 0, 0, 99, 0, '', '0000-00-00 00:00:00'),
	(00000000006, '2018-02-10 05:23:23', 6, 8, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '55000', '', 55000, 0, 92000, '', 0, 147000, 4, 0, 'admin', '2018-02-10 05:23:23'),
	(00000000007, '2018-02-10 05:26:18', 7, 8, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '55000', '', 55000, 0, 115000, '', 0, 170000, 1, 0, 'admin', '2018-02-10 05:26:18'),
	(00000000008, '2018-02-16 04:01:02', 180216, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 303216, 1, 0, 'admin', '2018-02-16 04:01:02'),
	(00000000009, '2018-02-16 04:15:19', 180216, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 303216, 1, 0, 'admin', '2018-02-16 04:15:19'),
	(00000000010, '2018-02-16 04:25:37', 0, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 123000, 0, 0, 'admin', '2018-02-16 04:25:37'),
	(00000000011, '2018-02-16 06:08:16', 0, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 123000, 1, 0, 'admin', '2018-02-16 06:08:16'),
	(00000000012, '2018-02-16 06:11:57', 0, 8, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '55000', '', 55000, 0, 92000, '', 0, 147000, 1, 0, 'admin', '2018-02-16 06:11:57'),
	(00000000013, '2018-02-16 06:16:40', 0, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 123000, 1, 0, 'admin', '2018-02-16 06:16:40'),
	(00000000014, '2018-02-17 01:35:26', 0, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 123000, 2, 0, 'admin', '2018-02-17 01:35:26'),
	(00000000015, '2018-02-17 01:37:03', 0, 10, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 123000, 3, 0, 'admin', '2018-02-17 01:37:03'),
	(00000000016, '2018-02-17 01:39:09', 12, 8, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '55000', '', 55000, 0, 92000, '', 0, 147000, 3, 0, 'admin', '2018-02-17 01:39:09'),
	(00000000017, '2018-02-17 02:02:57', 0, 10, 'LINE', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '31000', '', 31000, 0, 92000, '', 0, 123000, 3, 0, 'admin', '2018-02-17 02:02:57'),
	(00000000018, '2018-02-19 20:24:56', 0, 9, 'WA', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', 'jne', '', '', 9000, 0, 92000, '', 0, 101000, 3, 0, 'admin', '2018-02-19 20:24:56');
/*!40000 ALTER TABLE `tr_transaction` ENABLE KEYS */;

-- Dumping structure for table bukku.tr_transaction_detail
CREATE TABLE IF NOT EXISTS `tr_transaction_detail` (
  `order_id` int(11) unsigned zerofill NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_order_qty` int(11) NOT NULL,
  `product_price` bigint(20) NOT NULL,
  `product_total_price` bigint(20) NOT NULL COMMENT 'total = price * qty',
  KEY `order_id` (`order_id`),
  KEY `product_code` (`product_code`),
  CONSTRAINT `transactiondetail_fk_product` FOREIGN KEY (`product_code`) REFERENCES `ms_product` (`code`),
  CONSTRAINT `transactiondetail_fk_transaction` FOREIGN KEY (`order_id`) REFERENCES `tr_transaction` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.tr_transaction_detail: ~17 rows (approximately)
DELETE FROM `tr_transaction_detail`;
/*!40000 ALTER TABLE `tr_transaction_detail` DISABLE KEYS */;
INSERT INTO `tr_transaction_detail` (`order_id`, `product_code`, `product_order_qty`, `product_price`, `product_total_price`) VALUES
	(00000000002, 'MT16007', 1, 45000, 45000),
	(00000000003, 'MT16007', 1, 45000, 45000),
	(00000000004, 'MT16007', 2, 45000, 90000),
	(00000000005, 'MT16007', 2, 45000, 90000),
	(00000000005, 'MT16008', 1, 54000, 54000),
	(00000000006, 'DST001', 1, 92000, 92000),
	(00000000007, 'MT16009', 1, 60000, 60000),
	(00000000007, 'MT17021', 1, 55000, 55000),
	(00000000008, 'DST001', 1, 92000, 92000),
	(00000000009, 'DST001', 1, 92000, 92000),
	(00000000010, 'DST001', 1, 92000, 92000),
	(00000000011, 'DST001', 1, 92000, 92000),
	(00000000012, 'DST001', 1, 92000, 92000),
	(00000000013, 'DST001', 1, 92000, 92000),
	(00000000014, 'DST001', 1, 92000, 92000),
	(00000000015, 'DST001', 1, 92000, 92000),
	(00000000016, 'DST001', 1, 92000, 92000),
	(00000000017, 'DST001', 1, 92000, 92000),
	(00000000018, 'DST001', 1, 92000, 92000);
/*!40000 ALTER TABLE `tr_transaction_detail` ENABLE KEYS */;

-- Dumping structure for table bukku.tr_transaction_log
CREATE TABLE IF NOT EXISTS `tr_transaction_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status_id` int(11) NOT NULL,
  `notes` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.tr_transaction_log: ~11 rows (approximately)
DELETE FROM `tr_transaction_log`;
/*!40000 ALTER TABLE `tr_transaction_log` DISABLE KEYS */;
INSERT INTO `tr_transaction_log` (`ID`, `order_id`, `date`, `status_id`, `notes`) VALUES
	(1, 2, '2018-02-04 14:11:45', 99, 'Shipping'),
	(2, 2, '2018-02-04 14:13:11', 1, 'Payment'),
	(3, 2, '2018-02-04 14:16:41', 2, 'Confirmation'),
	(4, 3, '2018-02-05 04:07:44', 99, 'Shipping'),
	(5, 3, '2018-02-05 04:12:12', 1, 'Payment'),
	(6, 3, '2018-02-05 04:12:19', 2, 'Confirmation'),
	(7, 4, '2018-02-05 10:57:21', 99, 'Shipping'),
	(8, 4, '2018-02-05 11:02:16', 1, 'Payment'),
	(9, 4, '2018-02-05 11:04:10', 1, 'Payment'),
	(10, 4, '2018-02-05 11:06:23', 2, 'Confirmation'),
	(11, 5, '2018-02-05 11:07:49', 99, 'Shipping');
/*!40000 ALTER TABLE `tr_transaction_log` ENABLE KEYS */;

-- Dumping structure for table bukku.web_content
CREATE TABLE IF NOT EXISTS `web_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `content_name` varchar(50) NOT NULL,
  `content_type` varchar(50) NOT NULL,
  `content_body` varchar(255) NOT NULL,
  `content_image` varchar(255) NOT NULL,
  `redirect_url` varchar(255) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table bukku.web_content: ~16 rows (approximately)
DELETE FROM `web_content`;
/*!40000 ALTER TABLE `web_content` DISABLE KEYS */;
INSERT INTO `web_content` (`ID`, `content_name`, `content_type`, `content_body`, `content_image`, `redirect_url`, `modified_by`, `modified_date`, `is_active`) VALUES
	(1, 'Highlight Catatan Juang', 'HIGHLIGHT', '', 'https://bukku.co.id/source/uploads/2017/11/sliderbaru.jpg', 'https://bukku.co.id/kategori-produk/mediakita/fiersa-besari/', 'admin', '2018-01-16 00:00:00', 1),
	(2, 'Highlight Himsa', 'HIGHLIGHT', '', 'https://bukku.co.id/source/uploads/2017/12/sliderhimsah.jpg', 'https://bukku.co.id/produk/paket-bundel-ahimsa/', 'admin', '2018-01-16 00:00:00', 1),
	(3, 'Highlight AADC', 'HIGHLIGHT', '', 'https://bukku.co.id/source/uploads/2018/01/AAC2_180120_0014-1.jpg', 'https://bukku.co.id/produk/ayat-ayat-cinta-2-collectors-book-45-years-in-the-making/', 'admin', '2018-01-16 00:00:00', 1),
	(4, 'Writer of The Month', 'WOTM', '', 'https://bukku.id/public/images/writerofthemonth.jpg', 'https://bukku.co.id/produk/catatan-juang-fiersa-besari/', 'admiin', '0000-00-00 00:00:00', 1),
	(5, 'Book of The Month', 'BOTM', '', 'https://bukku.id/public/images/example-book3.jpg', 'https://bukku.co.id/produk/catatan-juang-fiersa-besari/', 'admin', '0000-00-00 00:00:00', 1),
	(6, 'Find Us Instagram', 'FIND', '', 'https://bukku.co.id/source/uploads/2017/08/003-instagram.png', 'https://www.instagram.com/bukku.co.id/', 'admin', '0000-00-00 00:00:00', 1),
	(7, 'Find Us Facebook', 'FIND', '', 'https://bukku.co.id/source/uploads/2017/08/002-facebook.png', 'https://www.facebook.com/bukku.co.id', 'admin', '0000-00-00 00:00:00', 1),
	(8, 'Find Us Youtube', 'FIND', '', 'https://bukku.co.id/source/uploads/2017/08/001-youtube.png', 'https://www.youtube.com/channel/UC2duq1sYm12Oupx2lFgciBA', 'admin', '0000-00-00 00:00:00', 1),
	(9, 'Find Us Twitter', 'FIND', '', 'https://bukku.co.id/source/uploads/2017/08/003-instagram.png', 'https://www.twitter.com/bukku.co.id/', 'admin', '0000-00-00 00:00:00', 1),
	(10, '', 'POPULER', '', 'https://bukku.id/public/images/example-book3.jpg', '', '', '0000-00-00 00:00:00', 1),
	(11, '', 'POPULER', '', 'https://bukku.id/public/images/example-book3.jpg', '', '', '0000-00-00 00:00:00', 1),
	(12, '', 'POPULER', '', 'https://bukku.id/public/images/example-book3.jpg', '', '', '0000-00-00 00:00:00', 1),
	(13, '', 'POPULER', '', 'https://bukku.id/public/images/example-book3.jpg', '', '', '0000-00-00 00:00:00', 1),
	(14, '', 'POPULER', '', 'https://bukku.id/public/images/example-book3.jpg', '', '', '0000-00-00 00:00:00', 1),
	(15, '', 'PROMO1', '', 'https://bukku.co.id/source/uploads/2018/01/AAC2_180120_0014-1.jpg', 'https://bukku.co.id/produk/ayat-ayat-cinta-2-collectors-book-45-years-in-the-making/', 'admin', '2018-01-16 00:00:00', 1),
	(16, '', 'PROMO2', '', 'https://bukku.co.id/source/uploads/2018/01/AAC2_180120_0014-1.jpg', 'https://bukku.co.id/produk/ayat-ayat-cinta-2-collectors-book-45-years-in-the-making/', 'admin', '2018-01-16 00:00:00', 1);
/*!40000 ALTER TABLE `web_content` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
